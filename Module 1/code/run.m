clear all
close all
clc

% % Parameters %%
inputs.predator.V_b  = 1.0;
% See inputs.predator.th_b below

inputs.impulse.v.a   = 100;
inputs.impulse.v.b   = 1;
inputs.impulse.v.tau = 6;
inputs.impulse.v.vo  = 7.45222/inputs.impulse.v.tau;

inputs.impulse.w.a   = 1000;
inputs.impulse.w.b   = 1;
inputs.impulse.w.tau = 1;
inputs.impulse.w.wo  = 6.8386/inputs.impulse.w.tau;

inputs.impulse.dist  = 5;

inputs.noise.phi = 0.2*pi;

inputs.start.thresh  = 1.5; % Prey response distance
inputs.stop.time     = max([inputs.impulse.v.tau, inputs.impulse.w.tau]);


% % Initial Conditions % %
% Prey
% Predator
x_a0  = 0.0;
y_a0  = 0.0;
th_a0 = 0*pi/180;

% Prey
d_0   = 3.0;
phi_0 = 0.3*pi;
x_b0  = d_0*cos(phi_0);
y_b0  = d_0*sin(phi_0);
inputs.predator.th_b = mod(phi_0 + pi + (0.1*pi),2*pi);
th_b  = inputs.predator.th_b;

X0 = [x_a0;...
      y_a0;...
      th_a0;...
      x_b0;...
      y_b0];
  
Ntrial = 21;

T  = 6;
dt = 0.01;
tspan  = 0:dt:T;
Nt = length(tspan);

for ntr = 1:Ntrial
    % % Integrate % %
    X = odeImpulse(@odefunc1,@odefunc2,@condStart,@condStop,@impulseFactor,tspan,X0,inputs);
    
    % % Results % %
    % Prey
    x(1:Nt,ntr)  = X(:,1);
    y(1:Nt,ntr)  = X(:,2);
    th(1:Nt,ntr) = X(:,3);
    % Predator
    xb(1:Nt,ntr) = X(:,4);
    yb(1:Nt,ntr) = X(:,5);

end

% % Animate % %
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 10 6 ];
f1.Color = 'w';


skipFrame = floor((1/dt)/30);

stamp = pseudoRandStamp(6);

vidWriter = VideoWriter(['films/evasion_' stamp '.mp4'],'MPEG-4');
vidWriter.FrameRate = 1/(dt*skipFrame);
open(vidWriter);

Nt = length(tspan);

headSize = 24;
tailWid  = 3;
tailLen  = 0.35;


for nt = 1:skipFrame:Nt
    figure(f1)
    
    cla
    box on
    hold on
    axis equal
    
    tt = tspan(nt);
    
    if abs(x(1,1)) < 0.001
        xcirc = inputs.start.thresh*cos(linspace(0,2*pi,101));
        ycirc = inputs.start.thresh*sin(linspace(0,2*pi,101));
    
        fill(xcirc,...
             ycirc,...
             [ 1 1 1 ]*0.9,...
             'LineStyle','none');
    end
    
    for ntr = 1:Ntrial
        
        plot(x(1:nt,ntr),...
             y(1:nt,ntr),...
             '-',...
             'LineWidth',1,...
             'Color',[ 0 0 0 ]);

        plot(x(nt,ntr),...
             y(nt,ntr),...
             '.',...
             'MarkerSize',headSize,...
             'Color',[ 0 0 0 ]);


        quiver(x(nt,ntr),...
               y(nt,ntr),...
              -tailLen*cos(th(nt,ntr)),...
              -tailLen*sin(th(nt,ntr)),...
               'Color',[ 1 1 1 ]*0,...
               'AutoScale','off',...
               'LineWidth',tailWid);
        
    end
    
    plot(xb(1:nt,1),...
         yb(1:nt,1),...
         '-',...
         'LineWidth',1,...
         'Color',[ 0.6 0 0 ]);

    plot(xb(nt,1),...
         yb(nt,1),...
         '.',...
         'MarkerSize',headSize,...
         'Color',[ 0.6 0 0 ]);

    quiver(xb(nt,1),...
           yb(nt,1),...
          -tailLen*cos(th_b),...
          -tailLen*sin(th_b),...
           'Color',[ 0.6 0 0 ],...
           'AutoScale','off',...
           'LineWidth',tailWid);
    
        
     
    axis([-4 6 -5 5])
    
    ax1 = gca;
    ax1.FontName = 'Times New Roman';
    ax1.FontSize = 16;
    ax1.XTick = -10:2:10;
    ax1.YTick = -10:2:10;

    xl = xlabel('$x$');
    xl.Interpreter = 'latex';
    yl = ylabel('$y$');
    yl.Interpreter = 'latex';
    
    writeVideo(vidWriter,getframe(gcf));
end


close(vidWriter)

fileID = fopen(['films/evasion_' stamp '_info.txt'],'w');

fprintf(fileID,['Event: \t\t' stamp '\n'...
                'Initial Conditions:\n'...
                ' Prey:\n'...
                '\tx: \t'  num2str(X0(1)) '\n'...
                '\ty: \t'  num2str(X0(2)) '\n'...
                '\tth: \t' num2str(X0(3)/pi) 'pi\n'...
                ' Predator:\n'...
                '\tx: \t'  num2str(X0(4)) '\n'...
                '\ty: \t'  num2str(X0(5)) '\n'...
                '\tth: \t' num2str(inputs.predator.th_b/pi) 'pi\n'...
                'Parameters:\n'...
                '\tResponse threshold: \t' num2str(inputs.start.thresh) '\n'...
                '\tResponse arclength: \t' num2str(inputs.impulse.dist) '\n'...
                '\tAngular pos noise:  \t' num2str(inputs.noise.phi) '\n'...
                ]);

fclose(fileID);




% === % === % === % === % AUX FUNCTIONS % === % === % === % === %


function V = velocity(t,tstar,inputs)

V = inputs.impulse.v.vo*...
    chi(inputs.impulse.v.a*(t-tstar)/inputs.impulse.v.tau).*...
    chi(inputs.impulse.v.b*(1-(t-tstar)/inputs.impulse.v.tau));

end

function Omg = angvel(t,tstar,inputs)

Omg = inputs.impulse.w.wo*...
      chi(inputs.impulse.w.a*(t-tstar)/inputs.impulse.w.tau).*...
      chi(inputs.impulse.w.b*(1-(t-tstar)/inputs.impulse.w.tau));

end

function dXdt = odefunc1(t,X,inputs)

% % States % %
% Predator
x_a  = X(1);
y_a  = X(2);
th_a = wrapTo2Pi(X(3));

% Prey
x_b  = X(4);
y_b  = X(5);

% % Parameters % %
V_b    = inputs.predator.V_b;
th_b   = wrapTo2Pi(inputs.predator.th_b);

% % Dynamics % %
% Predator
dx_a  = 0;
dy_a  = 0;
dth_a = 0;

% Prey
dx_b  = V_b.*cos(th_b);
dy_b  = V_b.*sin(th_b);

dXdt = [dx_a;...
        dy_a;...
        dth_a;...
        dx_b;...
        dy_b;];

end

function dXdt = odefunc2(t,X,tstar,impulse,inputs)

% % States % %
% Predator
x_a  = X(1);
y_a  = X(2);
th_a = wrapTo2Pi(X(3));

% Prey
x_b  = X(4);
y_b  = X(5);

% % Parameters % %
V_b    = inputs.predator.V_b;
th_b   = wrapTo2Pi(inputs.predator.th_b);

% % Reflex % %
V_a = velocity(t,tstar,inputs)*impulse.del_s;
Omg = angvel(t,tstar,inputs)*impulse.del_th;

% % Dynamics % %
% Predator
dx_a  = V_a.*cos(th_a);
dy_a  = V_a.*sin(th_a);
dth_a = Omg;

% Prey
dx_b  = V_b.*cos(th_b);
dy_b  = V_b.*sin(th_b);

dXdt = [dx_a;...
        dy_a;...
        dth_a;...
        dx_b;...
        dy_b;];

end

function out = condStart(t,Xn,Xn1,inputs)
% % States % %
% Predator
x_an  = Xn(1);
y_an  = Xn(2);

x_an1 = Xn1(1);
y_an1 = Xn1(2);

% Prey
x_bn  = Xn(4);
y_bn  = Xn(5);

x_bn1 = Xn1(4);
y_bn1 = Xn1(5);


% % Sensing % %
dn  = sqrt((x_bn-x_an).^2 + (y_bn-y_an).^2);
dn1 = sqrt((x_bn1-x_an1).^2 + (y_bn1-y_an1).^2);

% % Output % %
out = (dn > inputs.start.thresh && dn1 <= inputs.start.thresh );
end

function out = condStop(t,tstar,Xn,Xn1,inputs)
out = (t-tstar) > inputs.stop.time;
end

function impulse = impulseFactor(t,X,inputs)
% % States % %
% Predator
x_a  = X(1);
y_a  = X(2);
th_a = wrapTo2Pi(X(3));

% Prey
x_b  = X(4);
y_b  = X(5);

% % Sensing % %
d  = sqrt((x_b-x_a).^2 + (y_b-y_a).^2);
phi = wrapTo2Pi(atan2(y_b-y_a,x_b-x_a) - th_a)+inputs.noise.phi*randn;

delth = -wrapToPi(pi - phi);
% if delth < 0
%     impulse.del_th = -pi/2;
% elseif delth > 0
%     impulse.del_th = pi/2;
% else
%     impulse.del_th = 0;
% end
impulse.del_th = delth;
impulse.del_s  = inputs.impulse.dist;

fprintf(['Detected predator at t = ' num2str(t) '.\n'...
         '\tResponse Angle: \t' num2str(impulse.del_th/pi) 'pi\n'...
         '\tResponse Dist:  \t' num2str(impulse.del_s) '\n']);

end
