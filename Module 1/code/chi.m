function [ out ] = chi(t)

[Nj,Nk] = size(t);

out = zeros(Nj,Nk);

for n = 1:length(t)
    if t(n) <= 0
        out(n) = 0;
    else
        out(n) = exp(-1./t(n));
    end
end