function [ psrstamp ] = pseudoRandStamp(N)
if N > 19
    error('N must be <= 19');
else
	c = clock;
    psr = round(mod(c(6)*10^(N-1)*rand,10^(N)));
    psrstamp = sprintf(['%0' num2str(N) 'i'],psr);
end
