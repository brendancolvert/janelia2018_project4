function [ pdf ] = vonMises(x, fMu, fKappa)

pdf = 1./(2*pi*besseli(0,fKappa)).*...
      exp(fKappa*cos(x-fMu));