README

File Descriptions:

run.m - Main file to define parameters, auxiliary functions, and run simulation

odeImpulse.m - RK4 integrator for ODEs with a start and stop condition

chi.m - Component of bump function

pseudoRandStamp.m - Produces a pseudorandom string of numeric characters of a fixed length