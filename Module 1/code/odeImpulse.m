function [X,varargout] = odeImpulse(odefunc1,odefunc2,condStart,condStop,impulseFactor,tspan,X0,inputs)

Nx = length(X0);
Xn = X0;
X(1,1:Nx) = X0;
flag = false;

tstar = zeros(0,1);

for nt = 1:(length(tspan)-1)
    t = tspan(nt);
    dt = tspan(nt+1)-t;
    
    if ~flag
        dXdt = @(t,X)(odefunc1(t,X,inputs));
    else
        dXdt = @(t,X)(odefunc2(t,X,tstar,imp,inputs));
    end 
    
    k1 = dt*dXdt(t,Xn);
    k2 = dt*dXdt(t+dt/2,Xn + k1/2);
    k3 = dt*dXdt(t+dt/2,Xn + k2/2);
    k4 = dt*dXdt(t+dt  ,Xn + k3  );
    
    Xn1 = Xn + 1/6*(k1 + 2*k2 + 2*k3 + k4);
    
%     figure(99)
%     subplot(3,1,1)
%     hold on
%     plot(tspan(1:nt),X(1:nt,1),'-','Color','k')
%     plot(t,(Xn1(1)-Xn(1))/dt,'k.')
%     xlabel('t')
%     ylabel('dx/dt')
%     xlim([0 tspan(end)])
%     ylim(5*[-1 1])
%     
%     subplot(3,1,2)
%     hold on
%     plot(tspan(1:nt),X(1:nt,2),'-','Color','k')
%     plot(t,(Xn1(2)-Xn(2))/dt,'k.')
%     xlabel('t')
%     ylabel('dy/dt')
%     xlim([0 tspan(end)])
%     ylim(5*[-1 1])
%     
%     subplot(3,1,3)
%     hold on
%     plot(tspan(1:nt),X(1:nt,3)/pi,'-','Color','k')
%     plot(t,(Xn1(3)-Xn(3))/dt,'k.')
%     xlabel('t')
%     ylabel('d\theta/dt')
%     xlim([0 tspan(end)])
%     ylim(5*[-1 1])
    
    
    start = condStart(t,Xn,Xn1,inputs);
    if flag
        stop = condStop(t,tstar(end),Xn,Xn1,inputs);
    else
        stop = false;
    end
    if stop
        %flag = false;
    elseif start
        tstar(end+1) = t;
        imp = impulseFactor(t,Xn,inputs);
        flag = true;
    end
    
    X(nt+1,1:Nx) = Xn1;
    Xn = Xn1;
end

varargout{1} = tstar;
