function ptsT = local_to_global(rost,tail,pts)
% Transforms coordinates (pts, in n x 3) from global to local coordinate
% system, assuming the y-axis of larvae runs perpendicular to gravity

% Check dimensions of landmark coordinates
if size(rost,1)~=1 || size(rost,2)~=3 || ...
   size(tail,1)~=1 || size(tail,2)~=3
    error('inputs have incorrect dimensions')
end

% Retrieve local x axis to determine coordinate system
xaxis(1,1) = tail(1) - rost(1);
xaxis(1,2) = tail(2) - rost(2);
xaxis(1,3) = tail(3) - rost(3);

% Normalize to create a unit vector
xaxis = xaxis./norm(xaxis);

%Determine local y axis
%Short hand of cross product of inertial z axis and local x axis
yaxis = [-xaxis(2) xaxis(1) 0];

% Normalize to create a unit vector
yaxis = yaxis./norm(yaxis);

%Determine local z axis
zaxis = cross(xaxis,yaxis);

% Normalize to create a unit vector
zaxis = zaxis./norm(zaxis);

%Create rotation matrix (from inertial axes to local axes)
R = [xaxis' yaxis' zaxis'];

% If points are not meshgridded
if size(pts,2)==3
    % Rotate points
    ptsT = [R * pts']';
    
    % Translate global coordinates wrt rostrum
    ptsT(:,1) = ptsT(:,1) + rost(1);
    ptsT(:,2) = ptsT(:,2) + rost(2);
    ptsT(:,3) = ptsT(:,3) + rost(3);
    
else
    error('points need to be arranged in 3 columns')
    
end

% Visualize to test
if 0
    
    blength = norm([tail(1)-rost(1) tail(2)-rost(2) tail(3)-rost(3)]);    
    
    figure
    
    subplot(2,2,[1 3])
    plot3([tail(1) rost(1)],[tail(2) rost(2)],[tail(3) rost(3)],'b',...
          rost(1),rost(2),rost(3),'bo');
    hold on
    plot3(ptsT(:,1),ptsT(:,2),ptsT(:,3),'ro')
    xlabel('x'); ylabel('y'); zlabel('z')
    hold off
    grid on;axis equal
    view(3)
    title('global')
    
    subplot(2,2,2)
    plot([0 blength],[0 0],'b',0,0,'ob',pts(:,1),pts(:,2),'ro')
    xlabel('x');ylabel('y')
    grid on; axis equal
    title('local')
    
    subplot(2,2,4)
    plot([0 blength],[0 0],'b',0,0,'ob',pts(:,1),pts(:,3),'ro')
    xlabel('x');ylabel('z')
    grid on; axis equal
end