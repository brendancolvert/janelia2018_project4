clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);

U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
K   = V/U;
thp = data(:,4);
dth = data(:,5);
del = data(:,6);

data_in  = data(:,[2 4 1 3]);
data_out = data(:,5:6);

names_in  = {'d','phi','V','thp'};
names_out = {'dtheta','delta'};

noise_level = [0.01 0.02 0.05 0.10 0.20 0.50];

Nrep = 21;

state.phi = repmat(phi,Nrep,1);
state.thp = repmat(thp,Nrep,1);
state.K   = repmat(K,Nrep,1);
state.N   = length(state.phi);
N = state.N;
S = length(noise_level);

Delta = zeros(N,4,S);

clrs = lines;

f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 1 1 6.5 9 ];

for ns = 1:S
    fprintf('Noise level %6.3f\n',noise_level(ns));
    noise.phi   = noise_level(ns)*pi;
    noise.thp   = noise_level(ns)*pi;
    noise.K     = noise_level(ns)*1.0;
    noise.Delta = 0.05*pi;
    
    % Strategy 1
    Delta(1:N,1,ns) = strategy_1(state,noise);
    fprintf('\tStrategy 1:\t %6.3f pi�%6.3f pi\n',mean(Delta(1:N,1,ns))/pi,std(Delta(1:N,1,ns))/pi);
    
    
    subplot(S,4,(ns-1)*4+1)
    hold on
    box on
    plot(state.phi,Delta(:,1,ns),'k.','MarkerSize',2);
    
    xlim([ 0 2]*pi);
    ylim([-1 1]*pi);
    
    ax = gca;
    ax.FontName = 'Times New Roman';
    ax.FontSize = 8;
    ax.XTick = [-2:1:2]*pi;
    ax.YTick = [-2:1:2]*pi;
    if ns == S
        ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
        xlabel('$\phi$','Interpreter','latex');
    else
        ax.XTickLabels = [];
    end
    ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
    ylabel('$\Delta$','Interpreter','latex');
    ax.TickLabelInterpreter = 'latex';
    
    if ns == 1
        title('Strategy 1','FontName','Times New Roman','FontSize',8);
    end
    
    % Strategy 2
    Delta(1:N,2,ns) = strategy_2(state,noise);
    fprintf('\tStrategy 2:\t %6.3f pi�%6.3f pi\n',mean(Delta(1:N,2,ns))/pi,std(Delta(1:N,2,ns))/pi);
    
    subplot(S,4,(ns-1)*4+2)
    hold on
    box on
    plot(state.phi,Delta(:,2,ns),'k.','MarkerSize',2);
    
    xlim([ 0 2]*pi);
    ylim([-1 1]*pi);
    
    ax = gca;
    ax.FontName = 'Times New Roman';
    ax.FontSize = 8;
    ax.XTick = [-2:1:2]*pi;
    ax.YTick = [-2:1:2]*pi;
    if ns == S
        ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
        xlabel('$\phi$','Interpreter','latex');
    else
        ax.XTickLabels = [];
    end
    ax.YTickLabels = [];
    ax.TickLabelInterpreter = 'latex';
    
    if ns == 1
        title('Strategy 2','FontName','Times New Roman','FontSize',8);
    end
    
    % Strategy 3
    Delta(1:N,3,ns) = strategy_3(state,noise);
    fprintf('\tStrategy 3:\t %6.3f pi�%6.3f pi\n',mean(Delta(1:N,3,ns))/pi,std(Delta(1:N,3,ns))/pi);
    
    subplot(S,4,(ns-1)*4+3)
    hold on
    box on
    plot(state.thp,Delta(:,3,ns),'k.','MarkerSize',2);
    
    xlim([-1 1]*pi);
    ylim([-1 1]*pi);
    
    ax = gca;
    ax.FontName = 'Times New Roman';
    ax.FontSize = 8;
    ax.XTick = [-2:1:2]*pi;
    ax.YTick = [-2:1:2]*pi;
    if ns == S
        ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
        xlabel('$\theta_p$','Interpreter','latex');
    else
        ax.XTickLabels = [];
    end
    ax.YTickLabels = [];
    ax.TickLabelInterpreter = 'latex';
    
    if ns == 1
        title('Strategy 3','FontName','Times New Roman','FontSize',8);
    end
    
    % Strategy 4
    Delta(1:N,4,ns) = strategy_4(state,noise);
    fprintf('\tStrategy 4:\t %6.3f pi�%6.3f pi\n',mean(Delta(1:N,4,ns))/pi,std(Delta(1:N,4,ns))/pi);
    
    subplot(S,4,(ns-1)*4+4)
    hold on
    box on
    plot(state.thp,Delta(:,4,ns),'k.','MarkerSize',2);
    
    xlim([-1 1]*pi);
    ylim([-1 1]*pi);
    
    ax = gca;
    ax.FontName = 'Times New Roman';
    ax.FontSize = 8;
    ax.XTick = [-2:1:2]*pi;
    ax.YTick = [-2:1:2]*pi;
    if ns == S
        ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
        xlabel('$\theta_p$','Interpreter','latex');
    else
        ax.XTickLabels = [];
    end
    ax.YTickLabels = [];
    ax.TickLabelInterpreter = 'latex';
    
    if ns == 1
        title('Strategy 4','FontName','Times New Roman','FontSize',8);
    end
    
    pause(0.001)

end


print(f11,sprintf('responses_%6.3fpi.eps',noise.Delta/pi),'-depsc','-painters');









    
% == % == % == % == % == % FUNCTIONS % == % == % == % == % == % == %


% % Strategy 1 % % 
function Delta = strategy_1(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    if phi_hat < pi
        Delta(n) = wrapToPi(-pi/2+vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi( pi/2+vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 2 % % 
function Delta = strategy_2(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    Delta(n) = wrapToPi(phi_hat+pi+vmrand(0,1/noise.Delta^2));
end

end


% % Strategy 3 % % 
function Delta = strategy_3(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        Delta(n) = wrapToPi(thp_hat + pi/2 + vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi(thp_hat - pi/2 + vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 4 % % 
function Delta = strategy_4(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    K_hat   = max(1,state.K(n) + randn*noise.K);
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat + acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    else
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat - acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    end
end

end









