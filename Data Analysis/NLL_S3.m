function out = NLL_S3(eta,x,varargin)

[D,N] = size(x);

if D ~= 3
    error('Improper data matrix dimension');
end

out = 0;

if isempty(varargin)
    if length(eta) == 3 || length(eta) == 7
        out = sum(-log(pX_S3(x,eta)));
    end
end

global indx;

if isreal(out) && ~isinf(out)
    
    if length(eta) == 3
        fprintf('%i - %f: \t(%f,\t%f,\t%f)\n',indx,out,eta(1)/pi,eta(2)/pi,eta(3)/pi);
    elseif length(eta) == 7
        fprintf('%i - %f: \t(%f,\t%f,\t%f,\t%f,\t%f,\t%f,\t%f)\n',indx,out,eta(1)/pi,eta(2)/pi,eta(3)/pi,eta(4)/pi,eta(5)/pi,eta(6)/pi,eta(7)/pi);
    end
    
    
    
    figure(99)
    plot(indx,out,'k.');
    hold on
    indx = indx + 1;
    ylim([5.2 5.5])
    
end





end

