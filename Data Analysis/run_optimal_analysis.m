clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

sin = lam < 0;
dex = ~sin;

dfh(sin) = wrapToPi(dlh(sin)-wrapToPi(lam(sin)+phi(sin)+pi+chi(sin)));
dfh(dex) = wrapToPi(dlh(dex)-wrapToPi(lam(dex)+phi(dex)+pi-chi(dex)));

dfd(sin) = wrapToPi(dld(sin)-wrapToPi(lam(sin)+phi(sin)+pi+chi(sin)));
dfd(dex) = wrapToPi(dld(dex)-wrapToPi(lam(dex)+phi(dex)+pi-chi(dex)));

Nbin = 16;

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [1 1 14 8];
f1.Color = 'k';
subplot(1,2,1)
histogram(dfh,linspace(-pi,pi,Nbin+1),...
    'FaceColor',[0 0 0],...
    'EdgeColor',[1 1 1],...
    'LineWidth',2,...
    'Normalization','pdf')
xlim([-pi pi])
ylim([0 0.4])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-1:0.5:1);
ax.YTick = 0:0.1:0.4;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$\Delta_h-\Delta_{\rm optimal}$','Interpreter','latex')
ylabel('$p$','Interpreter','latex')


subplot(1,2,2)
histogram(dfd,linspace(-pi,pi,Nbin+1),...
    'FaceColor',[0 0 0],...
    'EdgeColor',[1 1 1],...
    'LineWidth',2,...
    'Normalization','pdf')
xlim([-pi pi])
ylim([0 0.4])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-1:0.5:1);
ax.YTick = 0:0.1:0.4;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$\Delta_d-\Delta_{\rm optimal}$','Interpreter','latex')
ylabel('$p$','Interpreter','latex','Color','w')



print(f1,'epsilon.eps','-depsc','-painters')


















