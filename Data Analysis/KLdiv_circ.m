function kldiv = KLdiv_circ(X,Y,k,circFlag)

[D,N] = size(X);
[~,M] = size(Y);

kldiv = log(M/(N-1));

[rhoX,rhoY] = nn_dist_circ(X,Y,k,circFlag);

for n = 1:N
    kldiv = kldiv + D/N*log(rhoY(n)/rhoX(n));
end


end
