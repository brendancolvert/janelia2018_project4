function y = boxfunc(x,eps)
[Nx,Ny] = size(x);
ii = (x >= -eps/2 & x <= eps/2);
y = zeros(Nx,Ny);
y(ii) = 1/eps;
