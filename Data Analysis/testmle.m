clear all
close all
clc

load(['responseData_' num2str([1 2 3]) '.mat']);

d   = data(:,1);

rng default % for reproducibility
x = ncx2rnd(8,3,1000,1);
[phat,pci] = mle(d,'distribution','gamma')

dt = linspace(0,0.06,101)
pt = gampdf(dt,phat(1),phat(2))


figure(1)
hold on
histogram(d,20,'Normalization','pdf')
plot(dt,pt,'LineWidth',2)