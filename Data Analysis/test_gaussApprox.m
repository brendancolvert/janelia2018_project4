clear all
close all
clc

sigma = logspace(-2,0,101)*pi;
mu  = 0;

fun = @(x,mu,sig)( VM(x,mu,sig).*log(VM(x,mu,sig)./GS(x,mu,sig)));

for ns = 1:length(sigma)
    d_KL(ns) = integral(@(x)fun(x,mu,sigma(ns)),-pi,pi);
    
end

loglog(sigma/pi,d_KL)
xlim([min(sigma) max(sigma)]/pi);

function p = VM(x,mu,sig)

p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);

end

function p = GS(x,mu,sig)

p = 1/sqrt(2*pi*sig^2)*exp(-(x-mu).^2/(2*sig^2));

end