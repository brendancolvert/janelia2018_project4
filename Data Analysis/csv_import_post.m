function [frames,pt1,pt2] = csv_import_post(file_path)
% Imports coordinate data from DLTViewer's CSV files
% Each individual is assumed to be described by a pair of coordinates in
% only a single frame

% Read CSV data. The rows correspond to the frame number, the columns are
% the coordinates
M = csvread(file_path,1,0);

% Initialze indicies
j = 1; i = 1;

% Store all point pairs
while i < (size(M,2)+1)
    
    % Indicies for values
    idx = ~isnan(M(:,i));
    
    % Check for only only value in column
    if sum(idx)>1
        error('More than one value in the column')
        
    % Store, if a single numerical value
    elseif sum(idx)==1
        
        % Store frame number
        frames(j,1) = find(idx);

        % Store coordinate data
        coord       = M(find(idx),i:(i+5));
        pt1(j,:)    = M(find(idx),i:(i+2));
        pt2(j,:)    = M(find(idx),(i+3):(i+5));
    
        % Advance indicies
        j = j + 1;       
        i = i + 6;
        
        % Clear for next iteration
        clear coord     
    else
        i = i + 1;
    end  
end