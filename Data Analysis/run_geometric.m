clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load(['responseData_R_' num2str(Vnum) '.mat']);
load rbcmap

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

%%


dlh_neg = dlh <  0;
dlh_pos = dlh >= 0;

dld_neg = dld <  0;
dld_pos = dld >= 0;

figure(1)
subplot(1,3,1)
plot(dlh(dlh_neg & dld_neg),dld(dlh_neg & dld_neg),'k.')
axis equal
axis(pi*[-1 1 -1 1])
xlabel('$\Delta_h$','Interpreter','latex')
ylabel('$\Delta_d$','Interpreter','latex')

subplot(1,3,2)
plot(dlh(dlh_pos & dld_pos),dld(dlh_pos & dld_pos),'k.')
axis equal
axis(pi*[-1 1 -1 1])
xlabel('$\Delta_h$','Interpreter','latex')
ylabel('$\Delta_d$','Interpreter','latex')

subplot(1,3,3)
plot(dlh,dld,'k.')
axis equal
axis(pi*[-1 1 -1 1])
xlabel('$\Delta_h$','Interpreter','latex')
ylabel('$\Delta_d$','Interpreter','latex')