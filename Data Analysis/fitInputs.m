clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

U = 0.075; % m/s


d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

%% Phi

func = @VonMises;
eta0 = [1.0*pi;...
        0.1*pi];
    
A = [-1  0;...
      0 -1;...
      0  1];
  
b = [0;...
     0;...
     2*pi];


x = phi';
 
options = optimoptions('fmincon','Display','notify-detailed');
etaopt = fmincon(@(eta)NLL(eta,x,func),eta0,A,b,[],[],[],[],[],options)

%% Lambda

func = @PiecewiseUniform;
eta0 = [0.5];
    
A = [-1;...
      1];
  
b = [0;...
     1];


x = lam';
 
options = optimoptions('fmincon','Display','notify-detailed');
etaopt = fmincon(@(eta)NLL(eta,x,func),eta0,A,b,[],[],[],[],[],options)

%% Chi

func = @Exponential;
eta0 = [1];
    
A = [-1];
  
b = [0];


x = chi';
 
options = optimoptions('fmincon','Display','notify-detailed');
etaopt = fmincon(@(eta)NLL(eta,x,func),eta0,A,b,[],[],[],[],[],options)




function out = NLL(eta,x,func)

out = sum(-log(func(x,eta)));

end



function p = VonMises(x,eta)

mu  = eta(1);
sig = eta(2);

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end

function p = PiecewiseUniform(x,eta)

[Nx,Ny] = size(x);

q = eta(1);

p = zeros(Nx,Ny);

p( ((x > -pi) & (x < -pi/2)) | ((x > pi/2) & (x < pi)) ) = (1-q)/pi;
p( ((x > -pi/2) & (x < pi/2)) )                          = q/pi;

end

function p = Exponential(x,eta)

alp = eta(1);

p = alp*exp(-alp*x);

end