function I = grid_MI(x,y,Nx,Ny)

[~,N] = size(x);

cx          = histcounts(x,Nx);
cy          = histcounts(y,Ny);
[cxy,Xe,Ye] = histcounts2(x,y,[Nx Ny]);

px  = cx/N;
py  = cy/N;
pxy = cxy/N;

for j = 1:Nx
    for k = 1:Ny
        dx = Xe(j+1)-Xe(j);
        dy = Ye(k+1)-Ye(k);
        if pxy(j,k) == 0
            ii(j,k) = 0;
        else
            ii(j,k) = pxy(j,k)*log2(pxy(j,k)/(px(j)*py(k)))*(dx*dy);
        end
    end
end

I = sum(ii(:));

