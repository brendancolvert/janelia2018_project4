function out = NLL_S1(eta,x,varargin)

[D,N] = size(x);

if D ~= 2
    error('Improper data matrix dimension');
end

out = 0;

if isempty(varargin)
    if length(eta) == 2 || length(eta) == 4
        out = 1/N*sum(-log(pX_S1(x,eta)));
    else
        error('Invalid parameter vector length');
    end
end

global indx;

if isreal(out) && ~isinf(out)
    
    if length(eta) == 2
        fprintf('%i - %f: \t(%f,\t%f,\t%f)\n',indx,out,eta(1)/pi,eta(2)/pi);
    elseif length(eta) == 4
        fprintf('%i - %f: \t(%f,\t%f,\t%f,\t%f,\t%f,\t%f,\t%f)\n',indx,out,eta(1)/pi,eta(2)/pi,eta(3)/pi,eta(4)/pi);
    end
    
    
    
    figure(99)
    plot(indx,out,'k.');
    hold on
    indx = indx + 1;
    %ylim([5.2 5.5])
    
end

end

