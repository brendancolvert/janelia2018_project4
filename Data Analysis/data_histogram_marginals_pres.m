clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);

Nbin = 11;
Nplot = 101;
Nx = length(phi);

f11 = figure(11);
f11.Units = 'pixels';
f11.Position = [ 20 20 1000 400 ];


% % d % % 
subplot(1,5,1)
h = histogram(d,Nbin,...
    'Normalization','count',...
    'FaceColor',[1 1 1]*1.0,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 2]*0.03);
ylim([0 160]);
xlabel('$d$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*0.03;
ax.XTickLabels = {'$0$','$0.03$','$0.06$'};
ax.YTick = 0:40:320;


% % phi % %
subplot(1,5,2)
h = histogram(phi,Nbin,...
    'Normalization','count',...
    'FaceColor',[1 1 1]*1.0,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 2]*pi);
ylim([0 160]);
xlabel('$\phi$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = 0:40:320;








% % tht % %
subplot(1,5,3)
histogram(tht,Nbin,...
    'Normalization','count',...
    'FaceColor',[1 1 1]*1.0,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);
ylim([0 160]);
xlabel('$\theta$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:40:320;






% % Delta d % %
subplot(1,5,4)
histogram(dld,Nbin,...
    'Normalization','count',...
    'FaceColor',[1 1 1]*1.0,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);
ylim([0 160]);
xlabel('$\Delta_d$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:40:320;





% % Delta h % %
subplot(1,5,5)
histogram(dlh,Nbin,...
    'Normalization','count',...
    'FaceColor',[1 1 1]*1.0,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);
ylim([0 160]);
xlabel('$\Delta_h$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:40:320;





print(f11,'histograms_marginal_whites.eps','-depsc','-painters');





function nll = NLL_Gamma(p,x)

alpha = p(1);
beta  = p(2);

nll = 0;

for n = 1:length(x)
    nll = nll - log(beta^alpha*x(n)^(alpha-1)*exp(-beta*x(n)));
end

nll = nll/length(x);

end

function nll = NLL_VonMises(p,x)

mu     = p(1);
kappa  = p(2);

nll = 0;

for n = 1:length(x)
    nll = nll - log(1/(2*pi*besseli(0,kappa))*exp(kappa*cos(x(n)-mu)));
end

nll = nll/length(x);

end

function nll = NLL_LogNormal(p,x)

mu    = p(1);
sigma = p(2);

nll = 0;

for n = 1:length(x)
    nll = nll - log(1/x(n)*1/(sqrt(2*pi)*sigma)*exp(-(log(x(n)-mu)^2/(2*sigma^2))));
end

nll = nll/length(x);

end



















