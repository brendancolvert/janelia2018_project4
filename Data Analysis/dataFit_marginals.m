clear all
close all
clc

Vnum = [1 2 3];


load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%% Load Data
V   = data(:,3)*0.01;
d   = data(:,1);
U   = data(:,9);
tht = data(:,4);
phi = data(:,2);
lam = wrapToPi(tht-phi+pi);
chi = real(acos(U./V));
dlh = data(:,5);
dld = data(:,6);

% phi
[ph,pc] = mle(phi,'pdf',@vmpdf,'start',[pi,10/pi])

% lam
[ph,pc] = mle(lam,'pdf',@pwupdf,'start',[0,0.5])

% U
[ph,pc] = mle(U,'distribution','inversegaussian')

% V
[ph,pc] = mle(V,'distribution','inversegaussian')

%% 