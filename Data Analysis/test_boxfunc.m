clear all
close all
clc

eps = 0.5*pi;
x = linspace(-1,1,801)*pi;
y_BX = boxfunc(x,eps);
y_VM = VM(x,0,0.5^2);

plot(x,y_BX,x,y_VM);

%integral(@(x)boxfunc(x,eps),-1,1)


function out = VM(x,mu,sig2)

[Nx,Ny] = size(x);

if isinf(sig2)
    out = ones(Nx,Ny)*1/(2*pi);
else
    out = 1/(2*pi*besseli(0,1/sig2))*exp(cos(x-mu)/sig2);
end

end
