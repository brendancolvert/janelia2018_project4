clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);

U = 0.075; % m/s

[Nprey,~] = size(data);

%data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle'];

t0 = 0;
A = -1;
b = 0;

for nprey = 1:Nprey
    fprintf('%i\n',nprey);
    d   = data(nprey,1); % m
    phi = wrapTo2Pi(data(nprey,2));
    V   = data(nprey,3); % m/s
    thp = data(nprey,4);
    K   = V/U;
    
    options = optimoptions('fmincon','Display','off');
    
    % Actual Heading
    dth = data(nprey,5);
    delth(nprey,5) = dth;
    [tmin(nprey,5),Dmin(nprey,5)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,5) = Dmin(nprey,5)/d;
    
    % Actual Displacement
    dth = data(nprey,6);
    delth(nprey,6) = dth;
    [tmin(nprey,6),Dmin(nprey,6)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,6) = Dmin(nprey,6)/d;
    
    % Strategy 1
    if phi < pi
        dth = pi/2;
    else
        dth = -pi/2;
    end
    delth(nprey,1) = dth;
    [tmin(nprey,1),Dmin(nprey,1)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,1) = Dmin(nprey,1)/d;
    
    % Strategy 2
    dth = wrapToPi(phi+pi);
    delth(nprey,2) = dth;
    [tmin(nprey,2),Dmin(nprey,2)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,2) = Dmin(nprey,2)/d;
    
    % Strategy 3
    if wrapTo2Pi(thp-phi) > pi
        dth = thp-pi/2;
    else
        dth = thp+pi/2;
    end
    delth(nprey,3) = dth;
    [tmin(nprey,3),Dmin(nprey,3)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,3) = Dmin(nprey,3)/d;
    
    % Strategy 4
    if wrapTo2Pi(thp-phi) > pi
        dth = thp-acos(1/K);
    else
        dth = thp+acos(1/K);
    end
    delth(nprey,4) = dth;
    [tmin(nprey,4),Dmin(nprey,4)] = fmincon(@(t)distance(t,U,V,dth,thp,d,phi),t0,A,b,[],[],[],[],[],options);
    
    dnorm(nprey,4) = Dmin(nprey,4)/d;
    
end

%% Run Plots



for n_strategy = 1:6
    
%     figure(1)
%     hold on
%     subplot(1,6,n_strategy)
%     histogram(Dmin(:,n_strategy),21);
    
    figure(2)
    hold on
    subplot(1,6,n_strategy)
    histogram(dnorm(:,n_strategy),21,'Normalization','probability',...
        'FaceColor',[1 1 1]*0.7,...
        'EdgeColor',[0 0 0]);
    ax = gca;
    ax.FontName = 'Times New Roman';
    ax.FontSize = 8;
    ax.XTick = 0:0.5:1;
    ax.YTick = 0:0.1:1;
    xlabel('$D_{\rm min}/d$','Interpreter','latex')
    xlim([0 1])
    if n_strategy <= 4
        title(['Strategy ' num2str(n_strategy)])
    elseif n_strategy == 5
        title('$\Delta \theta$','Interpreter','latex')
    elseif n_strategy == 6
        title('$\delta$','Interpreter','latex')
    end
   
    
%     figure(4)
%     hold on
%     subplot(1,6,n_strategy)
%     polarhistogram(delth(:,n_strategy),21);
%     rlim([0 20])
%     
%     figure(5)
%     hold on
%     subplot(1,6,n_strategy)
%     histogram(delth(:,n_strategy),21);
end


f2 = figure(2);
f2.Units = 'inches';
f2.Position = [3 1 8 1.5];



print(f2,'dnorm_dist.eps','-depsc','-painters')









function D = distance(t,U,V,dtheta,thetap,d,phi)

x  = U*t*cos(dtheta);
y  = U*t*sin(dtheta);
xp = V*t*cos(thetap)+d*cos(phi);
yp = V*t*sin(thetap)+d*sin(phi);

D = sqrt((x-xp).^2+(y-yp).^2);

end