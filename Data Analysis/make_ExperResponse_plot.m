clear all
close all
clc

dataset = [];

for vnum = 1:3
    load(['responseData_' num2str(vnum) '.mat']);
    dataset = [dataset;data];
end

dataset = [dataset(1:436,:);...
           dataset(438:end,:)];

%data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle'];
d   = dataset(:,1);
phi = wrapTo2Pi(dataset(:,2));
V   = dataset(:,3);
thp = dataset(:,4);
dth = dataset(:,5);
del = dataset(:,6);

Nprey = length(d);




% Plot Experimental Response
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [3 1 2 2];

Nbin = 31;
polarhistogram(dth,Nbin,...
    'FaceColor',[ 0 0 0 ],...
    'FaceAlpha',0.5,...
    'EdgeColor',[ 0 0 0 ],...
    'Normalization','pdf')


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.5];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

polarplot(linspace(0,2*pi,101),0.5*ones(101,1),'k-','LineWidth',0.75)

print(f1,'experResponse.eps','-depsc','-painters')


%Plot Strategy 1

for nprey = 1:Nprey
    if phi(nprey) < pi
        dth_1(nprey) = pi/2;
    else
        dth_1(nprey) = -pi/2;
    end
end

f2 = figure(2);
f2.Units = 'inches';
f2.Position = [3 1 2 2];

Nbin = 31;
polarhistogram(dth_1,Nbin,...
    'FaceColor',[ 0 0 0 ],...
    'FaceAlpha',0.5,...
    'EdgeColor',[ 0 0 0 ],...
    'Normalization','pdf')


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.5];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

polarplot(linspace(0,2*pi,101),0.5*ones(101,1),'k-','LineWidth',0.75)

print(f2,'response_1.eps','-depsc','-painters')


%Plot Strategy 2
dth_2 = phi-pi;

f3 = figure(3);
f3.Units = 'inches';
f3.Position = [3 1 2 2];

Nbin = 31;
polarhistogram(dth_2,Nbin,...
    'FaceColor',[ 0 0 0 ],...
    'FaceAlpha',0.5,...
    'EdgeColor',[ 0 0 0 ],...
    'Normalization','pdf')


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.5];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

polarplot(linspace(0,2*pi,101),0.5*ones(101,1),'k-','LineWidth',0.75)

print(f3,'response_2.eps','-depsc','-painters')


%Plot Strategy 3
for nprey = 1:Nprey
    if wrapTo2Pi(thp(nprey)-phi(nprey)) > pi
        dth_3(nprey) = thp(nprey)-pi/2;
    else
        dth_3(nprey) = thp(nprey)+pi/2;
    end
end
%dth_3 = (wrapTo2Pi(phi-thp)>pi).*(thp + pi/2)...
%            + (wrapTo2Pi(phi-thp)<pi).*(thp - pi/2);

f4 = figure(4);
f4.Units = 'inches';
f4.Position = [3 1 2 2];

Nbin = 31;
polarhistogram(dth_3,Nbin,...
    'FaceColor',[ 0 0 0 ],...
    'FaceAlpha',0.5,...
    'EdgeColor',[ 0 0 0 ],...
    'Normalization','pdf')


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.5];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

polarplot(linspace(0,2*pi,101),0.5*ones(101,1),'k-','LineWidth',0.75)

print(f4,'response_3.jpg','-djpeg','-r300')
print(f4,'response_3.eps','-depsc','-painters')

%Plot Strategy 5
K = 11/7.5;
for nprey = 1:Nprey
    if wrapTo2Pi(thp(nprey)-phi(nprey)) > pi
        dth_4(nprey) = thp(nprey)-acos(1/K);
    else
        dth_4(nprey) = thp(nprey)+acos(1/K);
    end
end
%dth_3 = wrapToPi((wrapTo2Pi(thp-phi)>pi).*(thp + acos(1/K))...
%            + (wrapTo2Pi(thp-phi)<pi).*(thp - acos(1/K)));

f5 = figure(5);
f5.Units = 'inches';
f5.Position = [3 1 2 2];

Nbin = 31;
polarhistogram(dth_4,Nbin,...
    'FaceColor',[ 0 0 0 ],...
    'FaceAlpha',0.5,...
    'EdgeColor',[ 0 0 0 ],...
    'Normalization','pdf')


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.5];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

polarplot(linspace(0,2*pi,101),0.5*ones(101,1),'k-','LineWidth',0.75)

print(f5,'response_4.jpg','-djpeg','-r300')
print(f5,'response_4.eps','-depsc','-painters')





