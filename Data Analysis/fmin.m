clear all
close all
clc

[x,y] = meshgrid(linspace(-1,1,201));

z = -exp(-(x.^2+y.^2));

surf(x,y,z)
shading flat
colormap gray