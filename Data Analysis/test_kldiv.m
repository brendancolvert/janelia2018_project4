clear all
close all
clc

nu = 2;
sg = 1;
       
Sig_p = sg^2*[nu^2 0;...
                 0 1];
     
ths = linspace(0,90,101)*pi/180;

rng(1)

N = 2000;
M = 2000;

X = mvnrnd([0;0],Sig_p,N)';

ks = [1 10 100 500];
for tind = 1:length(ths)
    fprintf('tind = %i/%i\n',tind,length(ths))
    th = ths(tind);

    Sig_q = sg^2*[ nu^2*cos(th)^2+sin(th)^2 0.5*(1-nu^2)*sin(2*th);...
                   0.5*(1-nu^2)*sin(2*th)   nu^2*sin(th)^2+cos(th)^2];

    D = 2;

    %KLD(tind) = 0.5*(log(det(Sig_q)/det(Sig_p)) + trace(inv(Sig_q)*Sig_p)-D);
    KLD(tind) = (nu^2-1)^2/(2*nu^2)*sin(th)^2;
    Y = mvnrnd([0;0],Sig_q,M)';
    
    for kind = 1:length(ks)
        k = ks(kind);
        kld(tind,kind) = max([KLdiv(X,Y,k),0]);
    end
end


%% Plots

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 3 3 ];

semilogy(ths,KLD,...
       'k-',...
       'LineWidth',2)
hold on
for kind = 1:length(ks)
    plot(ths,kld(:,kind),'-')
end
xlim([0 0.5]*pi)
ylim(10.^[-1.2 0.3])


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (0:0.1:0.5)*pi;
ax.YTick = 10.^(-1.2:0.3:0.3);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.1\pi$','$0.2\pi$',...
                    '$0.3\pi$','$0.4\pi$','$0.5\pi$'};
ax.YTickLabels = {'$10^{-1.2}$','$10^{-0.9}$','$10^{-0.6}$',...
                    '$10^{-0.3}$','$10^{0}$','$10^{0.3}$'};

xlabel('$\sigma_q/\sigma_p$',...
       'Interpreter','latex');
ylabel('$\mathcal{D}(p \| q )$',...
       'Interpreter','latex');
   
lns = lines;
kshft = [0.98 0.88 0.80 0.72];
for kind = 1:length(ks)
    text(0.49*pi,kld(end,kind)*kshft(kind),['$k=' num2str(ks(kind)) '$'],...
        'HorizontalAlign','right',...
        'FontName','Times New Roman',...
        'FontSize',8,...
        'Interpreter','latex',...
        'Color',lns(kind,:));
end

text(0.49*pi,10^(0.12),'Analytical',...
    'FontName','Times New Roman',...
    'FontSize',8,...
    'Interpreter','latex',...
    'HorizontalAlign','right',...
    'Color',[ 0 0 0 ]);


print(sprintf('kldiv_mvnrnd_test_%i_%i.eps',N,M),'-depsc','-painters')

% 
% 
% 
% 






















