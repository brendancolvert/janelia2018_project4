clear all
close all
clc

Nx = 21;
phi = linspace(0,2,Nx)*pi;
tht = linspace(-1,1,Nx)*pi;
del = linspace(-1,1,Nx)*pi;

[phi,tht,del] = meshgrid(phi,tht,del);

lam = wrapToPi(tht-phi+pi);

sig_S   = 0.05*pi;
sig_W   = 0.01*pi;
sig_R   = 0.05*pi;

mu_phi  = pi;
sig_phi = 0.4*pi;
mu_lam  = 0;
sig_lam = 0.4*pi;

tic

p = pX_S3([phi(:)';...
           lam(:)';...
           del(:)'],...
          [sig_S;...
           sig_W;...
           sig_R]);
       
toc

p = reshape(p,Nx,Nx,Nx);



%% Plot

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 2 2 ];
       
pt = patch(isosurface(phi,tht,del,p,0.005));
isonormals(phi,tht,del,p,pt)
pt.FaceColor = 'k';
pt.EdgeColor = 'none';
daspect([1 1 1])
view(3); 
axis tight
camlight 
lighting gouraud

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (-2:1:2)*pi;
ax1.YTick = (-2:1:2)*pi;
ax1.ZTick = (-2:1:2)*pi;
ax1.TickLabelInterpreter = 'latex';
ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax1.ZTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\theta$','Interpreter','latex');
zlabel('$\delta$','Interpreter','latex');

% print(f1,'testDist_S3.eps','-depsc');