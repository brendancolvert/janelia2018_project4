clear all
close all
clc

kappas = [0 0.1 1 10 100];

J = 101;
j = 1:J;

phi = linspace(0,2*pi,201);

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 2 2 4 3 ];
hold on
box on

for kind = 1:length(kappas)
    kappa = kappas(kind);
    
    for pind = 1:length(phi)
        p(kind,pind) = 1/2 + 1/(pi*besseli(0,kappa))*sum(besseli(j,kappa)./j.*(sin(j*(phi(pind)+pi))-sin(j*phi(pind))));
    end
    
    plot(phi,p(kind,:),'LineWidth',1)
    
end

lg = legend(['$\kappa = ' num2str(kappas(1)) '$'],...
            ['$' num2str(kappas(2)) '$'],...
            ['$' num2str(kappas(3)) '$'],...
            ['$' num2str(kappas(4)) '$'],...
            ['$' num2str(kappas(5)) '$']...
            );
lg.Interpreter = 'latex';
lg.Location = 'northwest';

xlim([0 2]*pi);
ylim([0 1]);

xlabel('$\phi$','Interpreter','latex');
ylabel('$p(\phi)$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (0:2)*pi;
ax.YTick = (0:0.5:1);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};

print(f1,'decisionFunction.eps','-depsc');