function play_play

subplot(1,2,2);

% Vary relative speed of predator
Kvals = [0.8 1.2 2 3];

% Set initial x-position
p.x0 = 0.5;

% Normalized initial y-position
p.y0 = 1;

% Initial spatial gradient in y wrt x
p.dydx0 = p.y0/p.x0;

% Span for simulation
p.x_span = [p.x0 4];

% Loop thru initial positions
for i = 1:length(Kvals)
    
    % Current 'K'
    p.K = Kvals(i);
    
    % Numerical simulation of ODE
    [t,Y] = solver(p,options);
    
    % Plot results
    plot(t,Y(:,1)) 
    hold on   
end

ylim([0 3]); xlim([0 max(p.x_span)]); axis square

% Plot text
xlabel('X');ylabel('Y') ; title('CASE 2: Fig 4')

% Clear variables
clear p i t Y X0vals