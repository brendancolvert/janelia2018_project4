function [indiv,frames,pt1,pt2] = csv_import(file_path)
% Imports coordinate data from DLTViewer's CSV files
% Each individual is assumed to be described by a pair of coordinates in
% only a single frame

% Read CSV data. The rows correspond to the frame number, the columns are
% the coordinates
M = csvread(file_path,1,0);

% Initialze index
j = 1;

% Store all point pairs
for i = 1:size(M,1)
    
    % If there are values in row . . .
    if max(isnan(M(i,:)))
        
        % Indicies for values
        idx = ~isnan(M(i,:));
        
        % Column numbers for values
        col_num = find(idx);
        
        % Step thru each larva in row
        for k = 1:(sum(idx)/6)
            
            % Store individual number
            indiv(j,1)  = (col_num((k-1).*6+1)-1)/6+1;
            
            % Store frame number
            frames(j,1) = i;
            
            % Store coordinate data
            coord       = M(i,col_num(((k-1)*6+1):k*6));
            pt1(j,:)    = coord(1:3);
            pt2(j,:)    = coord(4:6);
            
            % Advance index
            j = j + 1;
            
            % Clear for next iteration
            clear coord 
        end      
    end
end