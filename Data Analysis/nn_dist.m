function [rho_p,rho_q] = nn_dist(X,Y,k)
%NN_DIST Computes k-nearest-neigbor (KNN) distances from elements in X to
%elements in X and elements in Y

Nx = size(X,2);
Ny = size(Y,2);
if k > Nx || k > Ny
    error('Invalid choice of k. Must be less than total number of samples');
end

rho_p = zeros(1,Nx);
rho_q = zeros(1,Nx);
for nx = 1:Nx
    if nx == 1
        rho_p(nx) = kth_dist(X(:,nx),X(:,2:end),k);
    elseif nx == Nx
        rho_p(nx) = kth_dist(X(:,nx),X(:,1:(end-1)),k);
    else
        rho_p(nx) = kth_dist(X(:,nx),X(:,[1:(nx-1) (nx+1):end]),k);
    end
    rho_q(nx) = kth_dist(X(:,nx),Y,k);
end


end

function rhok = kth_dist(X,Y,k)
%%KTH_DIST Computes kth smallest distances from X to elements in Y

rhos = vecnorm(X-Y);
rhom = mink(rhos,k);
rhok = rhom(k);


end

function rho = sorted_dist(X,Y)
%%SORTED_DIST Computes sorted distances from X to elements in Y

rho = sort(vecnorm(X-Y));

end