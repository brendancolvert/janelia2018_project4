clear all
close all
clc

load rbcmap;

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end


Nprey = length(com_before);

for nprey = 1:Nprey
    prey0            = (head_before(nprey,1:2)-com_before(nprey,1:2))';
    p0(1:2,nprey)    = prey0/norm(prey0);
    th               = atan2(p0(2,nprey),p0(1,nprey));
    
    R = [  cos(th)  sin(th);...
          -sin(th)  cos(th) ];
    
    pred             = R*(-com_before(nprey,1:2))';
    d(1,nprey)       = norm(pred);
    phi(1,nprey)     = atan2(pred(2),pred(1));
    thp(1,nprey)     = -atan2(p0(2,nprey),p0(1,nprey));
end

airfoil = ([21.056338028169016, 28.183098591549296;
46.647887323943664, 34.66197183098592;
76.12676056338029, 39.845070422535215;
99.12676056338029, 43.732394366197184;
130.8732394366197, 47.943661971830984;
157.11267605633802, 51.83098591549296;
183.0281690140845, 52.80281690140845;
213.47887323943664, 54.42253521126761;
238.42253521126761, 53.45070422535211;
264.3380281690141, 50.859154929577464;
285.71830985915494, 46.971830985915496;
306.77464788732397, 43.40845070422535;
323.29577464788736, 35.309859154929576;
328.4788732394366, 28.183098591549296;
323.61971830985914, 20.732394366197184;
308.3943661971831, 13.28169014084507;
286.0422535211268, 8.098591549295774;
263.0422535211268, 4.859154929577465;
238.09859154929578, 3.23943661971831;
213.47887323943664, 3.23943661971831;
181.08450704225353, 4.211267605633803;
153.5492957746479, 7.450704225352113;
126.98591549295774, 10.04225352112676;
98.15492957746478, 14.253521126760564;
74.1830985915493, 17.816901408450704;
47.29577464788733, 21.380281690140844;
21.056338028169016, 28.183098591549296])'*0.000013;

airfoil(1,:) = airfoil(1,:)-0.0035;
airfoil(2,:) = airfoil(2,:)-0.00040;


f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 2 2 6.5 8 ];

hold on
box on

plength = 0.0020;

for nprey = 1:Nprey
    plot(d(nprey)*cos(phi(nprey))+plength*[0 cos(thp(nprey))],...
         d(nprey)*sin(phi(nprey))+plength*[0 sin(thp(nprey))],...
         '-',...
         'Color',[1 1 1]*0.4,...
         'LineWidth',1);
     
    plot(d(nprey)*cos(phi(nprey)),...
         d(nprey)*sin(phi(nprey)),...
         '.',...
         'Color',[1 1 1]*0.4,...
         'MarkerSize',7);
    
    
end

preyScale = 0.003;
af0 = airfoil;
    
    
fill(af0(1,:),af0(2,:),...
    [46 49 146]/255,...
    'FaceAlpha',1,...
    'LineStyle','none');









axis equal
axis(0.01*[[-5 5] [-5 5]*8/6.5])

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (-8:2:8)*0.01;
ax1.YTick = (-8:2:8)*0.01;

xlabel('$d \cos \phi$','Interpreter','latex');
ylabel('$d \sin \phi$','Interpreter','latex');


print(f1,'dataplot_preyFrame2d.eps','-depsc','-painters');