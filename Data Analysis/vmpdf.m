function p = vmpdf(x,mu,kappa)
%VMPDF Von Mises probability distribution function
%   x:      points to evaluate pdf
%   mu:     mean
%   kappa:  concentration parameter

if kappa < 0
    warning('Negative value for kappa');
end

p = 1/(2*pi*besseli(0,kappa))*exp(kappa*cos(x-mu));

end

