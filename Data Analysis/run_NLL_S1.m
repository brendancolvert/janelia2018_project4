clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data.D;
phi = data.Phi;
V   = data.V*0.01;
K   = V/U;
thp = wrapToPi(data.Lambda+phi+pi);
dlh = data.Delta;
dld = atan2(data.Y,data.X);

Ns = length(phi);

x = [phi;...
     dlh];
 
marg = true;
 
if marg

    eta0 = [0.1*pi;...
            0.1*pi;...
            1.0*pi;...
            0.1*pi];

    A = [-1  0  0  0;...
          0 -1  0  0;...
          0  0 -1  0;...
          0  0  1  0;...
          0  0  0 -1];


    b = [0;...
         0;...
         0;...
         2*pi;...
         0];


else


    eta0 = [0.1*pi;...
            0.1*pi];
    A = [-1  0;...
          0 -1];

    b = [0;...
         0];

end

global indx;
indx = 1;

options = optimoptions('fmincon','Display','notify-detailed');
[etaopt,nllopt] = fmincon(@(eta)NLL_S1(eta,x),eta0,A,b,[],[],[],[],[],options)

%% AIC
Np = length(etaopt);
AICc = 2*Ns*nllopt +2*Np + (2*Np^2+2*Np)/(Np-Ns-1)

pause


%% Plot
Nx = 101;
phim = linspace(0,2,Nx)*pi;
delm = linspace(-1,1,Nx)*pi;

[delm,phim] = meshgrid(delm,phim);

sig_S   = etaopt(1);
sig_R   = etaopt(2);


if length(etaopt) == 2
    mu_phi  = pi;
    sig_phi = inf;
elseif length(etaopt) == 4
    mu_phi  = etaopt(3);
    sig_phi = etaopt(4);
end

p = pX_S1([phim(:)';...
           delm(:)'],...
          [sig_S;...
           sig_R;...
           mu_phi;...
           sig_phi]);
p = reshape(p,Nx,Nx);


f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 2 2 ];
       
numc = 40;
contourf(phim,delm,p,numc,'LineStyle','none')
colormap(flipud(gray))

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (-2:1:2)*pi;
ax1.YTick = (-2:1:2)*pi;
ax1.TickLabelInterpreter = 'latex';
ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');

print(f1,'testDist_S1.eps','-depsc');


f2 = figure(2);
f2.Units = 'inches';
f2.Position = [ 3 1 2 2 ];
       
plot(phi,dld,'k.','MarkerSize',3)

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (-2:1:2)*pi;
ax1.YTick = (-2:1:2)*pi;
ax1.TickLabelInterpreter = 'latex';
ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');
axis(pi*[0 2 -1 1])

print(f2,'testDist_S1_data.eps','-depsc');