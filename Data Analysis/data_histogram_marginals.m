clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);

Nbin = 16;
Npl = 1001; 
Nx = length(phi);

f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 2 2 9 3 ];


% % d % % 
subplot(1,6,1)
h = histogram(d,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 0.06]);
ylim([0 40]);
xlabel('$d$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*0.03;
ax.XTickLabels = {'$0$','$0.03$','$0.06$'};
ax.YTick = 0:10:40;
%breakyaxis(ax,[0.45 0.95])

% % phi % %
subplot(1,6,2)
h = histogram(phi,linspace(0,2*pi,Nbin+1),...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

hold on


phipl = linspace(0,2*pi,Npl);
dpl   = VonMises(phipl,[3.192;1.2344]);
plot(phipl,dpl,'-','Color',[0.7 0 0],'LineWidth',1)

xlim([0 2]*pi);
ylim([0 0.40]);
xlabel('$\phi$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = 0:0.10:0.40;
%breakyaxis(ax,[0.45 0.95])







% % lambda % %
subplot(1,6,3)
histogram(lam,linspace(-pi,pi,Nbin+1),...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);
hold on

lampl = linspace(-pi,pi,Npl);
dpl   = PiecewiseUniform(lampl,0.90558);
plot(lampl,dpl,'-','Color',[0.7 0 0],'LineWidth',1)

xlim([-1 1]*pi);
ylim([0 0.4]);
xlabel('$\lambda$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:0.1:0.4
%breakyaxis(ax,[0.45 0.95])



% % Chi % %
subplot(1,6,4)
histogram(real(acos(1./K)),linspace(0,pi/2,Nbin+1),...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

hold on

chipl = linspace(0,pi/2,Npl);
dpl   = Exponential(chipl,2.5166);
plot(chipl,dpl,'-','Color',[0.7 0 0],'LineWidth',1)

xlim([0 0.5]*pi);
ylim([0 6]);
xlabel('$\chi$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:0.25:0.5)*pi;
ax.XTickLabels = {'$0$','$0.25\pi$','$0.5\pi$'};
ax.YTick = 0:1.5:6;
%breakyaxis(ax,[0.45 0.95])





% % Delta d % %
subplot(1,6,5)
histogram(dld,linspace(-pi,pi,Nbin+1),...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);
ylim([0 0.4]);
xlabel('$\delta_d$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:0.1:0.4;
%breakyaxis(ax,[0.45 0.95])




% % Delta h % %
subplot(1,6,6)
histogram(dlh,linspace(-pi,pi,Nbin+1),...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);
ylim([0 0.4]);
xlabel('$\delta_h$','Interpreter','latex');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = 0:0.1:0.4;
%breakyaxis(ax,[0.45 0.95])




%print(f11,'histograms_marginals.eps','-depsc','-painters');





function out = NLL(eta,x,func)

out = sum(-log(func(x,eta)));

end



function p = VonMises(x,eta)

mu  = eta(1);
sig = eta(2);

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end

function p = PiecewiseUniform(x,eta)

[Nx,Ny] = size(x);

q = eta(1);

p = zeros(Nx,Ny);

p( ((x > -pi) & (x < -pi/2)) | ((x > pi/2) & (x < pi)) ) = (1-q)/pi;
p( ((x >= -pi/2) & (x <= pi/2)) )                          = q/pi;

end

function p = Exponential(x,eta)

alp = eta(1);

p = alp*exp(-alp*x);

end


