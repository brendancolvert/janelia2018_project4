function p = pwupdf(x,mu,q)
%PWUPDF Piecewise uniform probability distribution function
%   x:      points to evaluate pdf
%   mu:     mean
%   q:      probability parameter

eps = 10^(-6);

q = min([max([eps q]) 1-eps]);


if q < 0 | q > 1
    warning('Improper value for q');
end

p = q/pi*(cos(x-mu) >= 0) + (1-q)/pi*(cos(x-mu) < 0);


end

