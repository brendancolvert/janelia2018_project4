clear all
close all
clc

ph  = @(phi,mu,kp,k)( phi + atan2(sin(mu-phi),k/kp+cos(mu-phi)) );
dph  = @(phi,mu,kp,k)( k*(k+kp*(cos(mu-phi)))/(k^2+kp^2+2*k*kp*cos(mu-phi)) );


mu = pi;
kp = 0.6481;

kk = [0.8 0.9 1 1.1 1.2];

phi = linspace(0,2*pi,201);

figure(1)
hold on
for kind = 1:length(kk)
    plot(phi,ph(phi,mu,kp,kk(kind)),'-');
end

legend('\kappa = 0.8',...
       '\kappa = 0.9',...
       '\kappa = 1.0',...
       '\kappa = 1.1',...
       '\kappa = 1.2');
   
   
figure(2)
hold on
for kind = 1:length(kk)
    plot(phi,ph(phi,mu,kp,kk(kind)),'-');
end

legend('\kappa = 0.8',...
       '\kappa = 0.9',...
       '\kappa = 1.0',...
       '\kappa = 1.1',...
       '\kappa = 1.2');