clear all
close all
clc

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end

% Heading of each prey
th_before = atan2(head_before(:,2)-com_before(:,2),head_before(:,1)-com_before(:,1));
th_after  = atan2(head_after(:,2)-com_after(:,2),head_after(:,1)-com_after(:,1));

Nprey = length(th_before);

% Plot
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [3 1 5 4];
hold on
axis equal
box on

scale = 100;
for nprey = 1:Nprey
    
    %length = norm(head_before-tail_before);
    length = 0.005;
    
    plot([head_before(nprey,1) head_after(nprey,1)]*scale,...
         [head_before(nprey,2) head_after(nprey,2)]*scale,...
         '-',...
         'Color',[1 1 1]*0.5);
    
    plot((head_after(nprey,1)+[0 -length*cos(th_after(nprey))])*scale,...
         (head_after(nprey,2)+[0 -length*sin(th_after(nprey))])*scale,...
         '-',...
         'Color',[0.8 0 0]);
    plot(head_after(nprey,1)*scale,...
         head_after(nprey,2)*scale,...
         '.','MarkerSize',6,...
         'Color',[0.8 0 0]);
    
    
    
end

for nprey = 1:Nprey
    
    %length = norm(head_before-tail_before);
    length = 0.005;
    
    
    plot((head_before(nprey,1)+[0 -length*cos(th_before(nprey))])*scale,...
         (head_before(nprey,2)+[0 -length*sin(th_before(nprey))])*scale,...
         'k-');
    plot(head_before(nprey,1)*scale,...
         head_before(nprey,2)*scale,'k.','MarkerSize',6);
    
    
end



axis([-2,6,-5,5])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.XTick = -4:2:6;
ax.YTick = -4:2:6;


xl = xlabel('$x$ [cm]');
xl.Interpreter = 'latex';
yl = ylabel('$y$ [cm]');
yl.Interpreter = 'latex';

print(f1,'rawdata.eps','-depsc','-painters')
