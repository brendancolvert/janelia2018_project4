clear all
close all
clc

load rbcmap;

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end


Nprey = length(com_before);

for nprey = 1:Nprey
    dx               = (com_after(nprey,1:2)-com_before(nprey,1:2))';
    prey0            = (head_before(nprey,1:2)-com_before(nprey,1:2))';
    prey             = (head_after(nprey,1:2)-com_after(nprey,1:2))';
    p0(1:2,nprey)    = prey0/norm(prey0);
    p(1:2,nprey)     = prey/norm(prey);
    th0              = atan2(p0(2,nprey),p0(1,nprey));
    th               = atan2(p(2,nprey),p(1,nprey));
    
    R0 = [  cos(th0)  sin(th0);...
          -sin(th0)  cos(th0) ];
    
    pred             = R0*(-com_before(nprey,1:2))';
    d(1,nprey)       = norm(pred);
    phi(1,nprey)     = wrapTo2Pi(atan2(pred(2),pred(1)));
    thp(1,nprey)     = -atan2(p0(2,nprey),p0(1,nprey));
    dth(1,nprey)     = wrapToPi(th-th0);
    delta(1,nprey)   = wrapToPi(atan2(dx(2),dx(1))-th0);
end

Nbin = 31;

f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 2 2 3 3 ];

plot(phi,wrapTo2Pi(phi-thp),'k.',...
    'MarkerSize',4.5);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (0:1:2)*pi;
ax.YTick = (0:1:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$0$','$\pi$','$2\pi$'};


xlim([0 2]*pi);
ylim([0 2]*pi);

xlabel('$\phi$','Interpreter','latex');
ylabel('$\phi-\theta_p$','Interpreter','latex');








print(f11,'phi_thp_relationship.eps','-depsc','-painters');



