clear all
close all
clc

sigma = [ 0.1 0.2 0.5 ]*pi;

N = 401;
x = linspace(-1,1,N)*pi;

f1 = figure(1);

for ns = 1:length(sigma)
    px{ns} = 1/(2*pi*besseli(0,1/sigma(ns)^2))*exp(cos(x)/sigma(ns)^2);
    plot(x,px{ns},'k-','LineWidth',1);
    hold on
    box on
end

xlim([-1 1]*pi)
ylim([0 1.5])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = (0:0.5:1.5);

xlabel('$x$','Interpreter','latex');
ylabel('$p(x)$','Interpreter','latex');




print(f1,['plotVM.eps'],...
                          '-depsc','-painters');
                      
                      
                      
                      
