clear all
close all
clc

load rbcmap;

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end


Nprey = length(com_before);

for nprey = 1:Nprey
    
    % Compute prey polarization vector before and after escape
    prey0 = (head_before(nprey,:)-com_before(nprey,:))';
    prey1 = (head_after(nprey,:)-com_after(nprey,:))';
    polr0    = prey0/norm(prey0);
    polr1    = prey1/norm(prey1);
    
    [az,el,r] = cart2sph(polr0(1),polr0(2),polr0(3));
    
    Raz = [ cos(az)  sin(az) 0;...
           -sin(az)  cos(az) 0;...
            0        0       1];
        
    Rel = [ cos(el)  0       sin(el);...
            0        1       0;...
           -sin(el)  0       cos(el)];
       
    R = Rel*Raz;

    p0(1:3,nprey) = R*polr0;
    p1(1:3,nprey) = R*polr1;
    
    p01 = cross(p0,p1)
    
    if p01(3) < 0
        dth(1,nprey)  =  acos(dot(p0(1:3,nprey),p1(1:3,nprey))/(norm(p0(1:3,nprey))*norm(p0(1:3,nprey))));
    else
        dth(1,nprey)  = -acos(dot(p0(1:3,nprey),p1(1:3,nprey))/(norm(p0(1:3,nprey))*norm(p0(1:3,nprey))));
    end
    
    
    pp(1:3,nprey) = R*[1;0;0];
    
    predator = R*(-com_before(nprey,:))';
    
    rp(1:3,nprey) = predator;
    
    [phi(1,nprey),...
     psi(1,nprey),...
     d(1,nprey)] = cart2sph(predator(1),predator(2),predator(3));
    
    
    
    % Center all coordinates at prey COM before escape
    predator(1:3,nprey)     = (-com_before(nprey,:))';
    displacement(1:3,nprey) = (com_after(nprey,:)-com_before(nprey,:))';
    
    
    
    % Compute body length
    bodylength(1,nprey)     = norm(head_before(nprey,:)-tail_before(nprey,:));
    
   
end

