clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap



%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);

delmod = zeros(length(tht),1);
for tind = 1:length(tht)
    chi = real(acos(1/K(tind)));
    if wrapTo2Pi(tht(tind)-phi(tind)) < pi
        delmod(tind) = wrapToPi(tht(tind)+chi);
    else
        delmod(tind) = wrapToPi(tht(tind)-chi);
    end
end

Nbin = 21;

f11 = figure(11);
f11.Units = 'pixels';
f11.Position = [ 100 100 500 250 ];

subplot(1,2,1)
hold on
box on
plot(tht,dld,...
    'k.',...
    'MarkerSize',5);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);
% plot(tht,delmod,...
%     '.',...
%     'MarkerSize',5,...
%     'Color',[0.7,0,0]);


er = rmsErrCirc(dld,delmod)

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\theta$','Interpreter','latex');
ylabel('$\Delta_d$','Interpreter','latex');

subplot(1,2,2)
hold on
box on
plot(tht,dlh,...
    'k.',...
    'MarkerSize',5);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);
% plot(tht,delmod,...
%     '.',...
%     'MarkerSize',5,...
%     'Color',[0.7,0,0]);

er = rmsErrCirc(dlh,delmod)


xlim([0 2]*pi);
ylim([-1 1]*pi);

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\theta$','Interpreter','latex');
ylabel('$\Delta_h$','Interpreter','latex');


print(f11,'dataComp_pres_04.eps','-depsc','-painters');



function RMS = rmsErrCirc(x,xmod)

Nx = length(x);

err = 0;

for xind = 1:Nx
    ero = (x(xind)-xmod(xind))^2;
    erp = (x(xind)+2*pi-xmod(xind))^2;
    erm = (x(xind)-2*pi-xmod(xind))^2;
    err = err + min([ero erp erm]);
end

RMS = 1/Nx*err;
end




