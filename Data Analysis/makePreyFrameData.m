clear all
close all
clc

load rbcmap;

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end


Nprey = length(com_before);

for nprey = 1:Nprey
    
    % Compute prey polarization vector before and after escape
    prey0 = (head_before(nprey,:)-com_before(nprey,:))';
    prey1 = (head_after(nprey,:)-com_after(nprey,:))';
    polr0    = prey0/norm(prey0);
    polr1    = prey1/norm(prey1);
    
    [az,el,r] = cart2sph(polr0(1),polr0(2),polr0(3));
    
    Raz = [ cos(az)  sin(az) 0;...
           -sin(az)  cos(az) 0;...
            0        0       1];
        
    Rel = [ cos(el)  0       sin(el);...
            0        1       0;...
           -sin(el)  0       cos(el)];
       
    R = Rel*Raz;

    p0(1:3,nprey) = R*polr0;
    p1(1:3,nprey) = R*polr1;
    
    p01 = cross(p0,p1)
    
    if p01(3) < 0
        dth(1,nprey)  =  acos(dot(p0(1:3,nprey),p1(1:3,nprey))/(norm(p0(1:3,nprey))*norm(p0(1:3,nprey))));
    else
        dth(1,nprey)  = -acos(dot(p0(1:3,nprey),p1(1:3,nprey))/(norm(p0(1:3,nprey))*norm(p0(1:3,nprey))));
    end
    
    
    pp(1:3,nprey) = R*[1;0;0];
    
    predator = R*(-com_before(nprey,:))';
    
    rp(1:3,nprey) = predator;
    
    [phi(1,nprey),...
     psi(1,nprey),...
     d(1,nprey)] = cart2sph(predator(1),predator(2),predator(3));
    
    
    
    % Center all coordinates at prey COM before escape
    predator(1:3,nprey)     = (-com_before(nprey,:))';
    displacement(1:3,nprey) = (com_after(nprey,:)-com_before(nprey,:))';
    
    
    
    % Compute body length
    bodylength(1,nprey)     = norm(head_before(nprey,:)-tail_before(nprey,:));
    
   
end

% figure(1)
% quiver3(zeros(1,Nprey),zeros(1,Nprey),zeros(1,Nprey),...
%         p0(1,:),p0(2,:),p0(3,:),...
%         'k-',...
%         'AutoScale','off');
% axis equal
% 
% figure(2)
% quiver3(zeros(1,Nprey),zeros(1,Nprey),zeros(1,Nprey),...
%         p1(1,:),p1(2,:),p1(3,:),...
%         'k-',...
%         'AutoScale','off');
% axis equal


f11 = figure(11);
f11.Units = 'inches';
f11.Position = [2 2 4 3];
hold on
box on

% Predators
predScale = 0.003;
quiver3(rp(1,:),rp(2,:),rp(3,:),...
        pp(1,:)*predScale,pp(2,:)*predScale,pp(3,:)*predScale,...
        '-',...
        'LineWidth',1,...
        'Color',[0.7 0 0],...
        'AutoScale','off');
% Prey
quiver3(0,0,0,mean(bodylength),0,0,...
    'k-',...
    'LineWidth',4,...
    'AutoScale','off');

    
axis equal
axis(0.06*[-1 1 -1 1 -1 1])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
ax.XTick = 0.03*(-2:1:2);
ax.YTick = 0.03*(-2:1:2);

xlabel('$x$','Interpreter','latex');
ylabel('$y$','Interpreter','latex');






f3 = figure(3);
f3.Units = 'inches';
f3.Position = [2 2 12 6];

subplot(1,2,1)
hold on
box on

% Predators
predScale = 0.005;
quiver3(rp(1,:),rp(2,:),rp(3,:),...
        pp(1,:)*predScale,pp(2,:)*predScale,pp(3,:)*predScale,...
        '-',...
        'LineWidth',2,...
        'Color',[0.7 0 0],...
        'AutoScale','off');
% Prey
quiver3(0,0,0,mean(bodylength),0,0,...
    'k-',...
    'LineWidth',4,...
    'AutoScale','off');

    
axis equal
axis(0.06*[-1 1 -1 1 -1 1])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
ax.XTick = 0.03*(-2:1:2);
ax.YTick = 0.03*(-2:1:2);
ax.ZTick = 0.03*(-2:1:2);

xlabel('$x$','Interpreter','latex');
ylabel('$y$','Interpreter','latex');
zlabel('$z$','Interpreter','latex');

view([0 90])

subplot(1,2,2)
hold on
box on

% Predators
predScale = 0.005;
quiver3(rp(1,:),rp(2,:),rp(3,:),...
        pp(1,:)*predScale,pp(2,:)*predScale,pp(3,:)*predScale,...
        '-',...
        'LineWidth',2,...
        'Color',[0.7 0 0],...
        'AutoScale','off');
% Prey
quiver3(0,0,0,mean(bodylength),0,0,...
    'k-',...
    'LineWidth',4,...
    'AutoScale','off');

    
axis equal
axis(0.06*[-1 1 -1 1 -1 1])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
ax.XTick = 0.03*(-2:1:2);
ax.YTick = 0.03*(-2:1:2);
ax.ZTick = 0.03*(-2:1:2);

xlabel('$x$','Interpreter','latex');
ylabel('$y$','Interpreter','latex');
zlabel('$z$','Interpreter','latex');

view([0 0])

print(f3,'rawdata_preyframe.eps','-depsc')



f4 = figure(4);
f4.Units = 'inches';
f4.Position = [2 2 8 6];


%Phi
subplot(2,2,1)
polarhistogram(phi,40,...
    'EdgeColor',[1 1 1]*0,...
    'FaceColor',[1 1 1]*0.7,...
    'Normalization','pdf');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'$0$','$\frac{\pi}{4}$','$\frac{\pi}{2}$','$\frac{3\pi}{4}$',...
    '$\pi$','$\frac{5\pi}{4}$','$\frac{3\pi}{2}$','$\frac{7\pi}{4}$'};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 0.4];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

title('$\phi$','Interpreter','latex');

%Psi
subplot(2,2,2)
polarhistogram(pi-psi,40,...
    'EdgeColor',[1 1 1]*0,...
    'FaceColor',[1 1 1]*0.7,...
    'Normalization','pdf');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 14;
ax.RAxisLocation = 180;
ax.Box = 'on';
ax.ThetaTick = 0:45:359;
ax.ThetaTickLabels = {'','','$\frac{\pi}{2}$','$\frac{\pi}{4}$',...
    '$0$','$-\frac{\pi}{4}$','$-\frac{\pi}{2}$',''};
ax.TickLabelInterpreter = 'latex';
ax.RLim = [0 1.4];
ax.RTick = [];
ax.RTickLabels = [];
ax.NextPlot = 'add';

title('$\psi$','Interpreter','latex');


%XY
subplot(2,2,3)
hold on
[cts,Xedges,Yedges] = histcounts2(rp(1,:),rp(2,:),20);
Xctr = 0.5*(Xedges(2:end)+Xedges(1:end-1));
Yctr = 0.5*(Yedges(2:end)+Yedges(1:end-1));
contourf(Yctr,Xctr,cts',...
    40,...
    'LineStyle','none')
colormap(cmap(end/2:end,:))

quiver(0,0,0.01,0,...
    'k-',...
    'LineWidth',2,...
    'AutoScale','off');

axis equal
axis(0.06*[-1 1 -1 1])

ax = gca;
ax.FontName = 'Times New Roman';
ax.XTick = 0.06*(-2:1:2);
ax.YTick = 0.06*(-2:1:2);
ax.FontName = 'Times New Roman';
ax.FontSize = 14;

xlabel('$x$','Interpreter','latex')
ylabel('$y$','Interpreter','latex')

%XZ
subplot(2,2,4)
hold on
[cts,Xedges,Zedges] = histcounts2(rp(1,:),rp(3,:),20);
Xctr = 0.5*(Xedges(2:end)+Xedges(1:end-1));
Zctr = 0.5*(Zedges(2:end)+Zedges(1:end-1));
contourf(Xctr,Zctr,cts,...
    40,...
    'LineStyle','none')
colormap(cmap(end/2:end,:))

quiver(0,0,0.01,0,...
    'k-',...
    'LineWidth',2,...
    'AutoScale','off');

axis equal
axis(0.06*[-1 1 -1 1])

ax = gca;
ax.FontName = 'Times New Roman';
ax.XTick = 0.06*(-2:1:2);
ax.YTick = 0.06*(-2:1:2);
ax.FontName = 'Times New Roman';
ax.FontSize = 14;

xlabel('$x$','Interpreter','latex')
ylabel('$z$','Interpreter','latex')

print(f4,'distributions_preyframe.eps','-depsc')

f5 = figure(5);
polarhistogram(dth,20);

