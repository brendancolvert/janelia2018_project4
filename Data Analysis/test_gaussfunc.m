clear all
close all
clc

sig = 0.01275*pi;
x = linspace(-1,1,1001)*pi;
y_GS = gaussfunc(x,sig);
y_VM = VM(x,0,0.04^2);

plot(x,y_GS,x,y_VM);

%integral(@(x)boxfunc(x,eps),-1,1)


function out = VM(x,mu,sig2)

[Nx,Ny] = size(x);

if isinf(sig2)
    out = ones(Nx,Ny)*1/(2*pi);
else
    out = 1/(2*pi*besseli(0,1/sig2))*exp(cos(x-mu)/sig2);
end

end
