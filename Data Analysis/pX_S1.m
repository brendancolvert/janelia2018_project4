function p = pX_S1(x,eta,varargin)

sig_S = eta(1);
sig_R = eta(2);

[D,N] = size(x);

if D~=2
    error('Invalid data size');
end

p = zeros(1,N);

if isempty(varargin)
    if length(eta) == 2
        mu_phi  = pi;
        sig_phi = inf;
    elseif length(eta) == 3
        mu_phi  = pi;
        sig_phi = eta(3);
    elseif length(eta) == 4
        mu_phi  = eta(3);
        sig_phi = eta(4);
    end
    for n = 1:N
        phi = x(1,n);
        del = x(2,n);
        p_phi = VonMises(phi,mu_phi,sig_phi);
        p(n)  = p_phi*p_dp(phi,del,sig_S,sig_R);
    end
end

end

function p = VonMises(x,mu,sig)

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end


function p = p_dp(phi,del,sig_S,sig_R)

p = 1/(2*pi*besseli(0,1/sig_R^2))*exp(cos(del+pi/2)/sig_R^2)*xi(phi,sig_S)+...
    1/(2*pi*besseli(0,1/sig_R^2))*exp(cos(del-pi/2)/sig_R^2)*(1-xi(phi,sig_S));
end

function out = xi(phi,sig_S)

out = integral(@(phih)integrand(phih,phi,sig_S),0,pi);

end

function out = integrand(phih,phi,sig_S)

out = 1/(2*pi*besseli(0,1/sig_S^2))*exp(cos(phih-phi)/sig_S^2);

end

