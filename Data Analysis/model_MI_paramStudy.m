clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
K   = V/U;
thp = data(:,4);
dth = data(:,5);
del = data(:,6);

Nsig = 15;
Nrep = 15;
Ngrid = 11;

sig_phi = logspace(log10(0.01),log10(1.0),Nsig)*pi;
sig_Del = logspace(log10(0.01),log10(1.0),Nsig)*pi;
Ns_phi  = length(sig_phi);
Ns_Del  = length(sig_Del);



state.phi = repmat(phi,Nrep,1);
state.thp = repmat(thp,Nrep,1);
state.K   = repmat(K,Nrep,1);
state.N   = length(state.phi);
N = state.N;

Delta_1 = zeros(N,Ns_phi,Ns_Del);
Delta_2 = zeros(N,Ns_phi,Ns_Del);
% Delta_3 = zeros(N,Ns_phi,Ns_Del);
% Delta_4 = zeros(N,Ns_phi,Ns_Del);

for ns_phi = 1:Ns_phi
    fprintf(' %i -',ns_phi);
    for ns_Del = 1:Ns_Del
        fprintf(' %i',ns_Del);
        noise.phi   = sig_phi(ns_phi);
        noise.thp   = sig_phi(ns_phi);
        noise.Delta = sig_Del(ns_Del);
        Delta_1(1:N,ns_phi,ns_Del) = strategy_1(state,noise);
        Delta_2(1:N,ns_phi,ns_Del) = strategy_2(state,noise);
        I_1(ns_phi,ns_Del) = grid_MI(state.phi',squeeze(Delta_1(1:N,ns_phi,ns_Del))',Ngrid,Ngrid);
        I_2(ns_phi,ns_Del) = grid_MI(state.phi',squeeze(Delta_2(1:N,ns_phi,ns_Del))',Ngrid,Ngrid);
    end
    fprintf('\n');
end


%% Plot

cnum = 40;

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 12 2.8 ];

subplot(1,4,1)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),I_1,cnum,'LineStyle','none')
contour(log10(sig_phi),log10(sig_Del),I_1,[1 1]*0.118,'k-')
contour(log10(sig_phi),log10(sig_Del),I_1,[1 1]*0.161,'k--')
colormap(cmap(end/2:end,:));
%colorbar
xlabel('$\sigma_\phi$','Interpreter','latex');
ylabel('$\sigma_\Delta$','Interpreter','latex');
axis([-1.5 .5 -1.5 .5])
title('Strategy 1');


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1.5:0.5:0.5);
ax.YTick = (-1.5:0.5:0.5);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};


subplot(1,4,2)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),I_2,cnum,'LineStyle','none')
contour(log10(sig_phi),log10(sig_Del),I_2,[1 1]*0.118,'k-')
contour(log10(sig_phi),log10(sig_Del),I_2,[1 1]*0.161,'k--')
colormap(cmap(end/2:end,:));
xlabel('$\sigma_\phi$','Interpreter','latex');
ylabel('$\sigma_\Delta$','Interpreter','latex');
axis([-1.5 .5 -1.5 .5])
title('Strategy 2');

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1.5:0.5:0.5);
ax.YTick = (-1.5:0.5:0.5);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};

lg = legend('$I$','$I = 0.118$','$I = 0.161$');
lg.Interpreter = 'latex';

print(f1,'paramstudy.eps','-depsc')


    
% == % == % == % == % == % FUNCTIONS % == % == % == % == % == % == %


% % Strategy 1 % % 
function Delta = strategy_1(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    if phi_hat < pi
        Delta(n) = wrapToPi(-pi/2+vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi( pi/2+vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 2 % % 
function Delta = strategy_2(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    Delta(n) = wrapToPi(phi_hat+pi+vmrand(0,1/noise.Delta^2));
end

end


% % Strategy 3 % % 
function Delta = strategy_3(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        Delta(n) = wrapToPi(thp_hat + pi/2 + vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi(thp_hat - pi/2 + vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 4 % % 
function Delta = strategy_4(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    K_hat   = max(1,state.K(n) + randn*noise.K);
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat + acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    else
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat - acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    end
end

end









