clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap



%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);

delmod = zeros(length(tht),1);
for tind = 1:length(tht)
    chi = real(acos(1/K(tind)));
    if wrapTo2Pi(tht(tind)-phi(tind)) < pi
        delmod(tind) = wrapToPi(tht(tind)+chi);
    else
        delmod(tind) = wrapToPi(tht(tind)-chi);
    end
end

Nbin = 21;

f11 = figure(11);
<<<<<<< HEAD
f11.Units = 'pixels';
f11.Position = [ 100 100 500 250 ];
=======
f11.Units = 'inches';
f11.Position = [ 1 1 4.2 2.1 ];
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b

subplot(1,2,1)
hold on
box on
plot(tht,dld,...
    'k.',...
<<<<<<< HEAD
    'MarkerSize',5);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);
% plot(tht,delmod,...
%     '.',...
%     'MarkerSize',5,...
%     'Color',[0.7,0,0]);


er = rmsErrCirc(dld,delmod)
=======
    'MarkerSize',3);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);


er = rmsErrCirc(dld,delmod);

fhwid = 0.25*pi;
fhhgt = 0.07*pi;
fctrx = 0.0*pi;
fctry = 0.88*pi;


% fill(fctrx + fhwid*[-1 1 1 -1 -1],...
%      fctry + fhhgt*[-1 -1 1 1 -1],...
%      [1 1 1]*1,...
%      'EdgeColor',[ 0 0 0 ]);
% text(fctrx,fctry,...
%     ['$\epsilon^2 = ' sprintf('%6.3f',er) '$'],...
%     'HorizontalAlignment','center',...
%     'Interpreter','latex',...
%     'FontSize',8);
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
<<<<<<< HEAD
ax.FontSize = 16;
=======
ax.FontSize = 8;
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\theta$','Interpreter','latex');
<<<<<<< HEAD
ylabel('$\Delta_d$','Interpreter','latex');
=======
ylabel('$\delta_d$','Interpreter','latex');


>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b

subplot(1,2,2)
hold on
box on
plot(tht,dlh,...
    'k.',...
<<<<<<< HEAD
    'MarkerSize',5);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);
% plot(tht,delmod,...
%     '.',...
%     'MarkerSize',5,...
%     'Color',[0.7,0,0]);

er = rmsErrCirc(dlh,delmod)


xlim([0 2]*pi);
ylim([-1 1]*pi);
=======
    'MarkerSize',3);
plot(pi*[-1 1],pi*[-1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);

er = rmsErrCirc(dlh,delmod);

% fill(fctrx + fhwid*[-1 1 1 -1 -1],...
%      fctry + fhhgt*[-1 -1 1 1 -1],...
%      [1 1 1]*1,...
%      'EdgeColor',[ 0 0 0 ]);
% text(fctrx,fctry,...
%     ['$\epsilon^2 = ' sprintf('%6.3f',er) '$'],...
%     'HorizontalAlignment','center',...
%     'Interpreter','latex',...
%     'FontSize',8);

>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
<<<<<<< HEAD
ax.FontSize = 16;
=======
ax.FontSize = 8;
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\theta$','Interpreter','latex');
<<<<<<< HEAD
ylabel('$\Delta_h$','Interpreter','latex');


print(f11,'dataComp_pres_04.eps','-depsc','-painters');
=======
ylabel('$\delta_h$','Interpreter','latex');


print(f11,'dataComp_04.eps','-depsc','-painters');
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b



function RMS = rmsErrCirc(x,xmod)

Nx = length(x);

err = 0;

for xind = 1:Nx
    ero = (x(xind)-xmod(xind))^2;
    erp = (x(xind)+2*pi-xmod(xind))^2;
    erm = (x(xind)-2*pi-xmod(xind))^2;
    err = err + min([ero erp erm]);
end

RMS = 1/Nx*err;
end




