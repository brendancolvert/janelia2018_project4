function pred_facing = threeDPredTraj(traj_pts)
% Unit vector for the predator direction defined as opposite direction
% of motion of passive prey

[xData, yData, zData] = prepareSurfaceData( traj_pts(:,1), traj_pts(:,2), traj_pts(:,3) );

ft = fittype( 'poly11' );
opts = fitoptions( ft );
opts.Lower = [-Inf -Inf -Inf];
opts.Robust = 'Bisquare';
opts.Upper = [Inf Inf Inf];

[fitresult, gof] = fit( [xData, yData], zData, ft, opts );

pred_facing = [0 0 0];

X = [traj_pts(1,1) traj_pts(end,1)];
Y = [traj_pts(1,2) traj_pts(end,2)];

Z = fitresult.p00 + fitresult.p10*X + fitresult.p01*Y;

pred_facing = [(X(2) -X(1)) (Y(2) -Y(1)) (Z(2) -Z(1))];

pred_facing = -pred_facing/norm(pred_facing);

end