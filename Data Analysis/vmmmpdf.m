function p = vmmmpdf(x,mu1,mu2,kappa1,kappa2,q)
%VMPDF Von Mises Mixture Model probability distribution function
%   x:      points to evaluate pdf
%   mu1:    mean1
%   mu2:    mean2
%   kappa1: concentration parameter 1
%   kappa2: concentration parameter 1
%   q:      mixture parameter

eps = 10^(-6);

q = min([max([eps q]) 1-eps]);

p = q/(2*pi*besseli(0,kappa1))*exp(kappa1*cos(x-mu1))+...
    (1-q)/(2*pi*besseli(0,kappa2))*exp(kappa2*cos(x-mu2));

end

