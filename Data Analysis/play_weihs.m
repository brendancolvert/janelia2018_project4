function play_weihs
% Run simulations from Weihs & Webb pred-prey modeling paper



%% General parameters

% Solver options
options    = odeset('RelTol',1e-7);

dpath = '/Users/mmchenry/Documents/Matlab code/robopredator with vision';


%% Avoidance, case I
% This determines the angle that minimizes the time require to move outside
% of a predator's visual sensory field. The solution is given by Eqn. 7

% Parameter values ("Case 1")  -----------------

% Initial predator position (m)
p.P0 = [0 0];

% Initial position of prey (m)
p.E0 = [10e-2 2e-2];

% Speed of predator (m/s)
p.U = 10e-2;

% Speed of prey (m/s)
p.V = 12e-2;

% Range of visual field of predator
p.gamma_p = 30/180*pi;

% Initial guess on min angle
alfa0 = 45/180*pi;

% Find alfa value that minimizes time to move out of sensory field
alfa_min = fminsearch(@time_to_exit,alfa0);

% Report minimum
disp(' '); disp('CASE I');
disp(['Angle to minimize time in view = ' num2str(alfa_min.*180/pi) ' deg'])

    function T = time_to_exit(alfa)
    % Calculates the time to exit the visual field of a predator, as a
    % function of the escape angle
    
        B = tan(p.gamma_p);
        
        % Eqn. 5
        T = (B.*p.E0(1) - p.E0(2)) ./ ...
            ( p.V.*(sin(alfa)-B.*cos(alfa)) + B.*p.U);    
    end

clear p alfa0 alfa_min


%% Avoidance, case II: Fig. 3
% The Case II strategy is where the prey attempts to maximize its distance
% from the predator by moving away from its instantaneous heading.
% Fig. 3 plots the trajectories predicted by the prey at different starting 
% positions for K = 1.2.  

figure;
subplot(1,2,1);

% Variable starting normlized x-position
X0vals = [.1 .2 .5 1.2];

% Normalized initial y-position
p.y0 = 1;

% Ratio of predator to prey speed
p.K = 1.2;

% Loop thru initial positions
for i = 1:length(X0vals)
    
    % Set current position
    p.x0 = X0vals(i);
    
    % Initial spatial gradient in y wrt x
    p.dydx0 = p.y0/p.x0;
    
    % span for simulation
    p.x_span = [p.x0 3.2];
    
    % Numerical simulation
    [t,Y] = solver(p,options);
    
    % Plot results
    plot(t,Y(:,1)) 
    hold on   
end

ylim([0 3]); xlim([0 4]);axis square

% Plot text
xlabel('X');ylabel('Y') ; title('CASE 2: Fig 3')

% Clear variables
clear p i t Y X0vals


%% Avoidance, case II: Fig. 4
% Same strategy as Fig. 3, but now plotting trajectories from the same
% position, but for varying K.

subplot(1,2,2);

% Vary relative speed of predator
Kvals = [0.8 1.2 2 3];

% Set initial x-position
p.x0 = 0.5;

% Normalized initial y-position
p.y0 = 1;

% Initial spatial gradient in y wrt x
p.dydx0 = p.y0/p.x0;

% Span for simulation
p.x_span = [p.x0 4];

% Loop thru initial positions
for i = 1:length(Kvals)
    
    % Current 'K'
    p.K = Kvals(i);
    
    % Numerical simulation of ODE
    [t,Y] = solver(p,options);
    
    % Plot results
    plot(t,Y(:,1)) 
    hold on   
end

ylim([0 3]); xlim([0 max(p.x_span)]); axis square

% Plot text
xlabel('X');ylabel('Y') ; title('CASE 2: Fig 4')

% Clear variables
clear p i t Y X0vals


%% Evasion
% Determines the angle that maximizes the distance that a predator can
% approach the prey for varying K.

% Parameter values  -----------------
Kvals1 = 10.^linspace(-2,0,1000);
Kvals2 = 10.^linspace(0,2,1000);

% Initial guess on min angle
alfa0 = 10/180*pi;

% Loop thru Kvals
for i = 1:length(Kvals1)

    p.K = Kvals1(i);
    
    % Find alfa value 
    alfa_min1(i,1) = fminsearch(@find_Dmin,alfa0).*180/pi;

end

% Loop thru Kvals
for i = 1:length(Kvals2)

    p.K = Kvals2(i);
    
    % Find alfa value 
    alfa_min2(i,1) = fminsearch(@find_Dmin,alfa0).*180/pi;

end

    function tau = find_Dmin(alfa)
        % Calculates the inverse of the minimum distance between predator 
        % and prey as a function of the escape angle
        
        % Time at which mimum distance is achived (Eqn. 33)
        %t_min = (p.X0./p.V).*(p.K-cos(alfa))./...
        %    (1-2.*p.K.*cos(alfa)+p.K.^2);
        
        % Min distance over time
        D_min = sin(alfa).^2 ./ (p.K.^2-2.*p.K.*cos(alfa)+1);
        
        % Find inverse (to use fminsearch)
        tau = 1./D_min;
    end

pp1 = spline(Kvals1,alfa_min1);
pp2 = spline(Kvals2,alfa_min2);


% Plot results
figure;
semilogx(Kvals1,alfa_min1,'k-',Kvals2,alfa_min2,'k-',...
         Kvals1,fnval(pp1,Kvals1),'r--',Kvals2,fnval(pp2,Kvals2),'r--')
title('Evasion (Fig. 5)')
xlabel('K')
ylabel('Optimal angle (deg)')

clear p alfa0

save([dpath filesep 'Weihs spline'],'pp1','pp2')





%% Functions

function [t,Y] = solver(p,options)

% Numerically solve the equation
[t,Y] = ode45(@gov_eqn,p.x_span,[p.y0; p.dydx0],options);

    function dY = gov_eqn(x,Y)
        % ODE of the dynamics of the system
        
        % Body position along y-axis
        y  = Y(1);
        
        % Spatial gradient in y wrt x
        dydx  = Y(2);
    
        % Define output: 
        dY(1,1) = dydx;
        
        % Define output: 
        dY(2,1) = -(p.K .* sqrt(1 + dydx.^2))./x;       
    end
end



end %play_weihs