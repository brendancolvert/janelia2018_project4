clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap



%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);

delmod = zeros(length(phi),1);
for pind = 1:length(phi)
    if phi(pind) < pi
        delmod(pind) = -pi/2;
    else
        delmod(pind) = pi/2;
    end
end

Nbin = 21;

f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 1 1 4.2 2.1 ];

subplot(1,2,1)
hold on
box on
plot(phi,dld,...
    'k.',...
    'MarkerSize',3);
plot(pi*[0 1],-pi/2*[1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);
plot(pi*[1 2],pi/2*[1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);


er = rmsErrCirc(dld,delmod);

fhwid = 0.25*pi;
fhhgt = 0.07*pi;
fctrx = 1.0*pi;
fctry = 0.88*pi;


% fill(fctrx + fhwid*[-1 1 1 -1 -1],...
%      fctry + fhhgt*[-1 -1 1 1 -1],...
%      [1 1 1]*1,...
%      'EdgeColor',[ 0 0 0 ]);
% text(fctrx,fctry,...
%     ['$\epsilon^2 = ' sprintf('%6.3f',er) '$'],...
%     'HorizontalAlignment','center',...
%     'Interpreter','latex',...
%     'FontSize',8);

xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
<<<<<<< HEAD
ax.FontSize = 16;
=======
ax.FontSize = 8;
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
<<<<<<< HEAD
ylabel('$\Delta_d$','Interpreter','latex');
=======
ylabel('$\delta_d$','Interpreter','latex');


>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b

subplot(1,2,2)
hold on
box on
plot(phi,dlh,...
    'k.',...
<<<<<<< HEAD
    'MarkerSize',5);
plot(pi*[0 1],-pi/2*[1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);
plot(pi*[1 2],pi/2*[1 1],...
    '-',...
    'LineWidth',3,...
    'Color',[0.7,0,0]);

er = rmsErrCirc(dlh,delmod)
=======
    'MarkerSize',3);
plot(pi*[0 1],-pi/2*[1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);
plot(pi*[1 2],pi/2*[1 1],...
    '-',...
    'LineWidth',2,...
    'Color',[0,0,0]);

er = rmsErrCirc(dlh,delmod);

% fill(fctrx + fhwid*[-1 1 1 -1 -1],...
%      fctry + fhhgt*[-1 -1 1 1 -1],...
%      [1 1 1]*1,...
%      'EdgeColor',[ 0 0 0 ]);
% text(fctrx,fctry,...
%     ['$\epsilon^2 = ' sprintf('%6.3f',er) '$'],...
%     'HorizontalAlignment','center',...
%     'Interpreter','latex',...
%     'FontSize',8);
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b


xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
<<<<<<< HEAD
ax.FontSize = 16;
=======
ax.FontSize = 8;
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
<<<<<<< HEAD
ylabel('$\Delta_h$','Interpreter','latex');


print(f11,'dataComp_pres_01.eps','-depsc','-painters');
=======
ylabel('$\delta_h$','Interpreter','latex');


print(f11,'dataComp_01.eps','-depsc','-painters');
>>>>>>> 0d9da3b9fb3c0bd16348ff94161154e698b9625b



function RMS = rmsErrCirc(x,xmod)

Nx = length(x);

err = 0;

for xind = 1:Nx
    ero = (x(xind)-xmod(xind))^2;
    erp = (x(xind)+2*pi-xmod(xind))^2;
    erm = (x(xind)-2*pi-xmod(xind))^2;
    err = err + min([ero erp erm]);
end

RMS = 1/Nx*err;
end




