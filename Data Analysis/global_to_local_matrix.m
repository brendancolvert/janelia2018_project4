function [xT,yT,zT] = global_to_local_matrix(rost,tail,xpts,ypts,zpts)
% Transforms coordinates (pts, in n x 3) from global to local coordinate
% system, assuming the y-axis of larvae runs perpendicular to gravity

% Check dimensions
if size(rost,1)~=1 || size(rost,2)~=3 || ...
   size(tail,1)~=1 || size(tail,2)~=3 
    error('inputs have incorrect dimensions')
end

% Retrieve local x axis to determine coordinate system
xaxis(1,1) = tail(1) - rost(1);
xaxis(1,2) = tail(2) - rost(2);
xaxis(1,3) = tail(3) - rost(3);

% Normalize to create a unit vector
xaxis = xaxis./norm(xaxis);

%Determine local y axis
%Short hand of cross product of inertial z axis and local x axis
yaxis = [-xaxis(2) xaxis(1) 0];

% Normalize to create a unit vector
yaxis = yaxis./norm(yaxis);

%Determine local z axis
zaxis = cross(xaxis,yaxis);

% Normalize to create a unit vector
zaxis = zaxis./norm(zaxis);

%Create rotation matrix (from inertial axes to local axes)
R = [xaxis' yaxis' zaxis'];

% Translate global coordinates wrt rostrum
xptsT = xpts - rost(1);
yptsT = ypts - rost(2);
zptsT = zpts - rost(3);

% Rotate points
%ptsT = [inv(R) * ptsT']';

% Transformation
for i = 1:size(xptsT,2)
    
    for j = 1:size(xptsT,3)
        ptsT = [xptsT(:,i,j) yptsT(:,i,j) zptsT(:,i,j)];
        
        % Rotate points
        ptsT = [inv(R) * ptsT']';
        
        % Store
        xT(:,i,j) = ptsT(:,1);
        yT(:,i,j) = ptsT(:,2);
        zT(:,i,j) = ptsT(:,3);    
    end    
end