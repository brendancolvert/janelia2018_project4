clear all
close all
clc

load Ldata;
spds = [2 11 20];

Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

%for vnum = Vnum
com_before = vertcat(L(Vnum).com,com_before);
com_after = vertcat(L(Vnum).com2,com_after);

head_before = vertcat(L(Vnum).head,head_before);
head_after  = vertcat(L(Vnum).head2,head_after);

tail_before = vertcat(L(Vnum).tail,tail_before);
%end

Nprey = size(com_before,1);

for n = 1:Nprey
    dist3d(1,n) = norm(com_before(n,:));
end

% Heading of each prey
th = atan2(head_before(:,2)-com_before(:,2),head_before(:,1)-com_before(:,1));


dth_left = zeros(0,1);
dth_rght = zeros(0,1);
dsp_left = zeros(0,1);
dsp_rght = zeros(0,1);
phi_dthr = [];
phi_dthl = [];
phi_dspr = [];
phi_dspl = [];

% Center around initial COM
for nprey = 1:Nprey
    
    R = [  cos(th(nprey))  sin(th(nprey));...
          -sin(th(nprey))  cos(th(nprey))];
      
    p0(1:2,nprey)    = [cos(th(nprey));sin(th(nprey))];
    ppred(1:2,nprey) = R*[1;0];
    
    com0(1:2,nprey)  = [0;0];
    pred0(1:2,nprey) = R*(-com_before(nprey,1:2))';
    head0(1:2,nprey) = R*(head_before(nprey,1:2)-com_before(nprey,1:2))';
    tail0(1:2,nprey) = R*(tail_before(nprey,1:2)-com_before(nprey,1:2))';
    com1(1:2,nprey)  = R*(com_after(nprey,1:2)-com_before(nprey,1:2))';
    head1(1:2,nprey) = R*(head_after(nprey,1:2)-com_before(nprey,1:2))';
    p1(1:2,nprey)    = nrmz(head1(1:2,nprey)-com1(1:2,nprey));
    
    bodylength(1,nprey) = norm(head_before(nprey,1:2)-tail_before(nprey,1:2));
    d(1,nprey)          = norm(pred0(1:2,nprey));
    phi(1,nprey)        = wrapToPi(atan2(pred0(2,nprey),pred0(1,nprey)));
    thp(1,nprey)        = wrapToPi(atan2(ppred(2,nprey),ppred(1,nprey)));
    dispangle(1,nprey)  = wrapToPi(atan2(com1(2,nprey),com1(1,nprey)));
    dtheta(1,nprey)     = wrapToPi(atan2(p1(2,nprey),p1(1,nprey)));
    dispdist(1,nprey)   = norm(com1(:,nprey));
   
    
    if (phi(nprey) < 0 )
        phi_dthr = [phi_dthr phi(nprey)];
        dth_rght = [dth_rght, dtheta(1,nprey)];
    else
        phi_dthl = [phi_dthl phi(nprey)];
        dth_left = [dth_left, dtheta(1,nprey)];
    end
    
    if (phi(nprey) < 0 )
        phi_dspr = [phi_dspr phi(nprey)];
        dsp_rght = [dsp_rght, dispangle(1,nprey)];
    else
        phi_dspl = [phi_dspl phi(nprey)];
        dsp_left = [dsp_left, dispangle(1,nprey)];
    end
    
    
    
    scale = 0.001;
    plotsize = 0.05;
    
%     figure(1);
%     hold on
%     axis equal
%     axis(plotsize*[-1 1 -1 1])
%     plot([head0(1,nprey) com0(1,nprey)],...
%          [head0(2,nprey) com0(2,nprey)],...
%          'k-',...
%          'LineWidth',1);
%     plot(head0(1,nprey),...
%          head0(2,nprey),...
%          'k.',...
%          'MarkerSize',12);
%     quiver(d(nprey)*cos(phi(nprey)),d(nprey)*sin(phi(nprey)),...
%            scale*cos(thp(nprey)),-scale*sin(thp(nprey)),'r-');
%     
%     
%     figure(2);
%     hold on
%     axis equal
%     axis(plotsize*[-1 1 -1 1])
%     plot([head1(1,nprey) com1(1,nprey)],...
%          [head1(2,nprey) com1(2,nprey)],...
%      'k-',...
%      'LineWidth',1);
%     plot(head1(1,nprey),...
%          head1(2,nprey),...
%      'k.',...
%      'MarkerSize',12);
%     quiver(d(nprey)*cos(phi(nprey)),d(nprey)*sin(phi(nprey)),...
%            scale*cos(thp(nprey)),scale*sin(thp(nprey)),'r-');
    
end

figure(101)
histogram(dispdist,20)

% figure(1)
% subplot(1,2,1)
% polarhistogram(dth_left,20);
% subplot(1,2,2)
% polarhistogram(dth_rght,20);
% 
% figure(2)
% subplot(1,2,1)
% polarhistogram(dsp_left,20);
% subplot(1,2,2)
% polarhistogram(dsp_rght,20);
% 
% figure(3)
% subplot(2,1,1)
% histogram(dist3d,20)
% subplot(2,1,2)
% histogram(d,20)


data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle' dispdist'];
save(['responseData_new_' num2str(Vnum) '.mat'],'data');


pause
% figure(4)
% subplot(1,2,1)
% plot(phi_dthl, dth_left, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dthr, dth_rght, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('right')  
% 
% figure(5)
% subplot(1,2,1)
% plot(phi_dspl, dsp_left, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dspr, dsp_rght, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('right')


% % % % CORRELATION ANALYSIS % % % %

% Phi to Delta Theta
phi_wrap = wrapTo2Pi(phi)/pi;
dtheta_wrap = wrapTo2Pi(dtheta+pi)/pi-1;
indices = ~isnan(dtheta_wrap);
[R, p] = corr(phi_wrap(indices)', dtheta_wrap(indices)');
figure(6)
plot(wrapTo2Pi(phi)/pi,wrapTo2Pi(dtheta+pi)/pi-1,'k.')
ylabel('\Delta \theta')
xlabel('\phi')

Vpred = 1;
L = 1;

% Displacement to Flow Velocity Field
u = Vpred*L./(d.^2).*cos(2*phi-thp);
v = Vpred*L./(d.^2).*sin(2*phi-thp);
arg_uv = atan2(v,u);

delta = wrapTo2Pi(dispangle);
arg_uv_wrap = wrapTo2Pi(arg_uv+pi)/pi-1;
indices = ~isnan(delta);
[R, p] = corr(dispangle(indices)', arg_uv_wrap(indices)')
figure(7)
plot(wrapTo2Pi(phi-thp)/pi,wrapTo2Pi(dispangle)/pi,'k.')
xlabel('\delta')
ylabel('\angle u,v')
zlabel('\theta_p')

% Correlation between phi and thetap
phibound   = linspace(-pi,pi,101);
thpbound_l = phibound + pi/2;
thpbound_u = phibound + 3*pi/2;

for nphi = 1:length(phi)
    if thp(nphi) > phi(nphi)
        thp_unwrap(nphi) = thp(nphi);
    else
        thp_unwrap(nphi) = thp(nphi)+2*pi;
    end
end

figure(8)
hold on
plot(phi/pi,thp_unwrap/pi,'k.')
plot(phibound/pi,thpbound_l/pi,'r--')
plot(phibound/pi,thpbound_u/pi,'r--')
xlabel('\phi/\pi')
ylabel('(\theta_p-\theta)/\pi')

figure(9)
hold on
plot3(phi/pi,thp/pi,delta/pi,'k.')
xlabel('\phi/\pi')
ylabel('\theta_p/\pi')
zlabel('\delta/\pi')
axis(1.5*[-1 1 -1 1]);

figure(10)
hold on
plot3(1/sqrt(5)*(2*phi-thp)/pi,1/sqrt(5)*(phi+2*thp)/pi,delta/pi,'k.')
xlabel('(2*\phi-theta_p)/\pi')
ylabel('(-\phi+2*theta_p)/\pi')
zlabel('\delta/\pi')
%ylabel('\delta/\pi')
xlim(1.5*[-1 1]);
ylim(1.5*[-1 1]);
zlim([-0.5 2.5]);
view([0 0]) 

figure(11)
hold on
plot3(1/sqrt(10)*(3*phi-thp)/pi,1/sqrt(10)*(phi+3*thp)/pi,delta/pi,'k.')
xlabel('(3*\phi-theta_p)/\pi')
ylabel('(-\phi+3*theta_p)/\pi')
zlabel('\delta/\pi')
%ylabel('\delta/\pi')
xlim(1.5*[-1 1]);
ylim(1.5*[-1 1]);
zlim([-0.5 2.5]);
view([0 0]) 



% f1 = figure(1);
% f1.Units = 'inches';
% f1.Position = [1 1 6 6];
% hold on
% axis equal
% box on
% 
% for nprey = 1:Nprey
%     plot(com_before(nprey,1),com_before(nprey,2),'k.','MarkerSize',4);
%     plot(head_before(nprey,1),head_before(nprey,2),'k.','MarkerSize',8);
%     plot([head_before(nprey,1) com_before(nprey,1)],[head_before(nprey,2) com_before(nprey,2)],...
%         'k-');
%     
%     plot([com_before(nprey,1) com_after(nprey,1)],...
%          [com_before(nprey,2) com_after(nprey,2)],...
%          '-',...
%          'Color',[ 1 1 1 ]*0.5);
%     
%     plot(com_after(nprey,1),com_after(nprey,2),'r.','MarkerSize',4);
%     plot(head_after(nprey,1),head_after(nprey,2),'r.','MarkerSize',8);
%     plot([head_after(nprey,1) com_after(nprey,1)],[head_after(nprey,2) com_after(nprey,2)],...
%         'r-');
% end
% 
% ax = gca;
% ax.FontName = 'Times New Roman';
% xl = xlabel('$x$');
% xl.Interpreter = 'latex';
% xl.FontName = 'Times New Roman';
% yl = ylabel('$y$');
% yl.Interpreter = 'latex';
% yl.FontName = 'Times New Roman';
% 
% axis(0.01*[-2 6 -5 5])
% 
% print(f1,'2d_slow.eps','-depsc','-painters');

% figure(2)
% hold on
% axis equal
% 
% for nprey = 1:Nprey
%     plot(com_before(nprey,1),com_before(nprey,3),'k.','MarkerSize',6);
%     plot(head_before(nprey,1),head_before(nprey,3),'k.','MarkerSize',9);
%     plot([head_before(nprey,1) tail_before(nprey,1)],[head_before(nprey,3) tail_before(nprey,3)],...
%         'k-');
%     
%     plot(com_after(nprey,1),com_after(nprey,3),'r.','MarkerSize',6);
%     plot(head_after(nprey,1),head_after(nprey,3),'r.','MarkerSize',9);
%     plot([head_after(nprey,1) com_after(nprey,1)],[head_after(nprey,3) com_after(nprey,3)],...
%         'r-');
% end
% 
% figure(3)
% hold on
% axis equal
% 
% for nprey = 1:Nprey
%     plot3(com_before(nprey,1),...
%           com_before(nprey,2),...
%           com_before(nprey,3),...
%           'k.','MarkerSize',10);
%     plot3(head_before(nprey,1),...
%           head_before(nprey,2),...
%           head_before(nprey,3),...
%           'k.','MarkerSize',20);
%     plot3([head_before(nprey,1) tail_before(nprey,1)],...
%           [head_before(nprey,2) tail_before(nprey,2)],...
%           [head_before(nprey,3) tail_before(nprey,3)],...
%           'k-',...
%           'LineWidth',2);
%     
%     plot3(com_after(nprey,1),...
%           com_after(nprey,2),...
%           com_after(nprey,3),...
%           'r.','MarkerSize',10);
%     plot3(head_after(nprey,1),...
%           head_after(nprey,2),...
%           head_after(nprey,3),...
%           'r.','MarkerSize',20);
%     plot3([head_after(nprey,1) com_after(nprey,1)],...
%           [head_after(nprey,2) com_after(nprey,2)],...
%           [head_after(nprey,3) com_after(nprey,3)],...
%           'r-',...
%           'LineWidth',2); 
% end
