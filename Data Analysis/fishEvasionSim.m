%% Simulations

L = 1; %Length scale of the predator.
U = 1; %Speed of prey


%Simulating noisy observations for a number of trials, when the true
%predator coordinates are given
%True predator coordinates
nTrials = 10000;

d_true = repmat([1],nTrials,1);
phi_true = (repmat([0.25*pi],nTrials,1));
thetaP_true = (repmat([pi/4+pi/2],nTrials,1)+phi_true);
scale =1;
vP_true = repmat([2*scale],nTrials,1);

K = vP_true/U;
%
% Visual observations
s_vis = observe_vis([d_true,phi_true,thetaP_true,vP_true],[0.1,pi/10,pi/10,sqrt(scale)*0.1]);
s_vis = s_vis(K>1,:);

%Bayesian MAP estimate for flat prior & gaussian noise
% is distributed identically to the observation.
d_est = s_vis(:,1);
phi_est = wrapTo2Pi(s_vis(:,2));
thetaP_est = wrapTo2Pi(s_vis(:,3));
vP_est = s_vis(:,4);
estimates = [d_est,phi_est,thetaP_est,vP_est];

%Decision rule according to strategy


% Plotting
%Plotting the resultant estimates of phi
figure(1);

subplot(2,4,1)
histogram(phi_est,50)
title('$p(\hat{\phi}|\phi)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[],'xlim',[0, 2*pi],'XTick',[0 pi 2*pi],'XTickLabel',rad2deg([0 pi 2*pi]))
ylabel('Probability')
subplot(2,4,2)
histogram(d_est)
title('$p(\hat{d}|d)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[])
subplot(2,4,3)
histogram(thetaP_est,50)
set(gca,'FontSize',18,'YTick',[],'xlim',[0, 2*pi],'XTick',[0 pi 2*pi],'XTickLabel',rad2deg([0 pi 2*pi]))
title('$p(\hat{\theta_p}|\theta_p)$','Interpreter','Latex')
subplot(2,4,4)
histogram(vP_est)
title('$p(\hat{vP}|vP)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[])


%Decision rule
for i = 1:4
    delta_theta = escape(estimates,i,U);
    subplot(2,4,4+i)
    polarhistogram(delta_theta,50,'Normalization','probability')
    %rlim([0,1])
    set(gca,'RTick',[]);
    title(['Strategy ',num2str(i)])
    set(gca,'FontSize',18)
    set(gca,'ThetaTick',[0 90 180 270])
end

%%
% Flow observations
s_flow = observe_flow([d_true,phi_true,thetaP_true,vP_true],sqrt(scale)*[0.1 0.1 0.1 0.1],L);
s_flow = s_flow(K>1,:);

%Bayesian MAP estimate for flat prior & gaussian noise
% is distributed identically to the observation.
u_est = s_flow(:,1);
v_est = s_flow(:,2);
uy_est = s_flow(:,3);
vy_est = s_flow(:,4);

d_est = 2*sqrt((u_est.^2+v_est.^2)./(uy_est.^2+vy_est.^2));
phi_est = wrapTo2Pi(atan2((v_est.*vy_est+u_est.*uy_est),(-u_est.*vy_est+v_est.*uy_est)));
thetaP_est = wrapTo2Pi(3*atan2(-v_est,u_est) - 2*atan2(-uy_est,-vy_est));
vP_est = 4*sqrt((u_est.^2+v_est.^2).^3)./((vy_est.^2+uy_est.^2));

estimates = [d_est,phi_est,thetaP_est,vP_est];

%Convert flow estimates into predator coordinates
%Decision rule according to strategy


% Plotting
%Plotting the resultant estimates of phi
figure(2);

subplot(2,4,1)
histogram(phi_est,50)
title('$p(\hat{\phi}|\phi)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[],'xlim',[0, 2*pi],'XTick',[0 pi 2*pi],'XTickLabel',rad2deg([0 pi 2*pi]))
ylabel('Probability')
subplot(2,4,2)
histogram(d_est)
title('$p(\hat{d}|d)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[])
subplot(2,4,3)
histogram(thetaP_est,50)
set(gca,'FontSize',18,'YTick',[],'xlim',[0, 2*pi],'XTick',[0 pi 2*pi],'XTickLabel',rad2deg([ 0 pi 2*pi]))
title('$p(\hat{\theta_p}|\theta_p)$','Interpreter','Latex')
subplot(2,4,4)
histogram(vP_est)
title('$p(\hat{vP}|vP)$','Interpreter','Latex')
set(gca,'FontSize',18,'YTick',[])

%Decision rule
for i = 1:4
    delta_theta = escape(estimates,i,U);
    subplot(2,4,4+i)
    polarhistogram(delta_theta,50,'Normalization','probability')
    %rlim([0,1])
    set(gca,'RTick',[]);
    title(['Strategy ',num2str(i)])
    set(gca,'FontSize',18)
    set(gca,'ThetaTick',[0 90 180 270])
end





%% Simulating noisy observations for many different predator locations
load('pooled_data.mat')
phi_data = wrapTo2Pi(data(:,2));
thetaP_data = wrapTo2Pi(data(:,4));
vP_data = (data(:,3));
d_data = (data(:,1));
U = 2;

b_data = [d_data, phi_data, thetaP_data, vP_data];
dtheta_data = (data(:,5));

%Params
nreps = 10;
nbins = 25;
strategy = 2;

% kPhi = 0.7;
% kPhi_prior = 0.35;
% muPhi_prior = 0.8*pi;

kPhi = 1;
%Max entropy prior only respecting bounds
kPhi_prior = 0;
muPhi_prior = 0;
% Max entropy prior matching mean and variance & respecting bounds.
paramsPhiMLL = fmincon(@(params)vonMisesNLL(params,phi_data),[0,1],[],[],[],[],[0,0],[2*pi,inf]);
kPhi_prior = paramsPhiMLL(2);
muPhi_prior = paramsPhiMLL(1);

kThetaP = 1;
kThetaP_prior = 0.7;
muThetaP_prior = 0.8*pi;

sigmaV = 0.5;
sigmaV_prior = 1;
muV_prior = 10;

sigmaD = 0.01;
sigmaD_prior = inf;
muD_prior = 0;

params = [sigmaD, sigmaD_prior, muD_prior, kPhi,kPhi_prior,muPhi_prior,kThetaP, kThetaP_prior, muThetaP_prior, sigmaV, sigmaV_prior, muV_prior];

[bHat_sim, b_sim] = generate_data(b_data, params, nreps);
dtheta_sim{1} = escape(bHat_sim,1,U);
dtheta_sim{2} = escape(bHat_sim,2,U);
dtheta_sim{3} = escape(bHat_sim,3,U);
dtheta_sim{4} = escape(bHat_sim,4,U);

figure(1)
subplot(3,2,1)
hist3(repmat([phi_data,dtheta_data],nreps,1),[nbins,nbins]); view([0,90]); xlim([0,2*pi]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');
title('Actual escape headings')
xlabel('\Phi_{true}')
ylabel('\Delta\theta_{data}')
c = colorbar;
set(gca,'FontSize',18)
subplot(3,2,2)
hist3([b_sim(:,2),dtheta_sim{strategy}],[nbins,nbins]); view([0,90]); xlim([0,2*pi]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');

title('Model simulation')
xlabel('\Phi_{true}')
ylabel('\Delta\theta_{simulated}')
set(gca,'FontSize',18)
c = colorbar;
set(get(c,'label'),'string','Probability');
subplot(3,2,3)
hist3(repmat([thetaP_data,dtheta_data],nreps,1),[nbins,nbins]); view([0,90]); xlim([0,2*pi]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');

xlabel('\Theta_{P_{true}}')
ylabel('\Delta\theta_{data}')
set(gca,'FontSize',18)
c = colorbar;
subplot(3,2,4)
hist3([b_sim(:,3),dtheta_sim{strategy}],[nbins,nbins]); view([0,90]); xlim([0,2*pi]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');
xlabel('\Theta_{P_{true}}')
ylabel('\Delta\theta_{simulated}')
set(gca,'FontSize',18)
c = colorbar;
set(get(c,'label'),'string','Probability');
subplot(3,2,5)
hist3(repmat([vP_data,dtheta_data],nreps,1),[25,25]); view([0,90]); xlim([2,20]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');
xlabel('V_{P_{true}}')
ylabel('\Delta\theta_{data}')
set(gca,'FontSize',18)
c = colorbar;
subplot(3,2,6)
hist3([b_sim(:,4),dtheta_sim{strategy}],[25,25]); view([0,90]); xlim([2,20]); ylim([-pi,pi])
surfHandle = get(gca, 'child');
set(surfHandle,'FaceColor','interp', 'CdataMode', 'auto');xlabel('V_{P_{true}}')
ylabel('\Delta\theta_{simulated}')
set(gca,'FontSize',18)
c = colorbar;
set(get(c,'label'),'string','Probability');
%%
% [bHat_sim,b_sim] = generate_data([1, pi/2, pi/3, 3], [0.01,inf,0,0.01,0.01,0,0.7,0.01,0,0.01,inf,0],1);
% escape(bHat_sim,4,1)

%% Visual noise model
function s_vis = observe_vis(b, noise)
% b = [d,phi,thetaP, vP];
% vis = [d, phi, thetaP, vP];
% s = [s_d, s_phi, s_thetaP, s_vP];

%True predator coordinates
d = b(:,1);
phi = b(:,2);
thetaP = b(:,3);
vP = b(:,4);

%Noisy visual measurements
s_vis(:,1) = normrnd(d, noise(1)); %normal  predator distance d
s_vis(:,2) = vmrand(phi,(1/noise(2))^2); %vonMises predator angular location phi
s_vis(:,3) = vmrand(thetaP,(1/noise(3))^2); %vonMises predator heading thetaP
s_vis(:,4) = normrnd(vP, noise(4)); %normal predator velocity v
end

%% Flow noise model
function s_flow = observe_flow(b, noise, L)
% b = [d,phi,thetaP, vP];
% flow = [u, v, uy, vy];
% s = [s_u, s_v, s_uy, s_vy];

%True predator coordinates
d = b(:,1);
phi = b(:,2);
thetaP = b(:,3);
vP = b(:,4);

%Converting to true flow values
u = (vP*(L^2)).*(cos(-2*phi+thetaP))./(d.^2); %u
v = (-vP*(L^2)).*(sin(-2*phi+thetaP))./(d.^2); %v
uy = (-2*vP*(L^2)).*(sin(-3*phi+thetaP))./(d.^3); %uy
vy = (-2*vP*(L^2)).*(cos(-3*phi+thetaP))./(d.^3); %vy

%Noisy flow measurements
% s = [u,v,uy, vy];
s_flow(:,1) = normrnd(u, noise(1)); %gaussian predator velocity x component
s_flow(:,2) = normrnd(v, noise(2)); %gaussian predator velocity y compoent
s_flow(:,3) = normrnd(uy, noise(3)); %gaussian self velocity gradient in y- uy
s_flow(:,4) = normrnd(vy, noise(4)); %gaussian predator velocity gradient in y- vy
end

%% Decision rule/strategy


function delta_theta = escape(estimates, strategy, U)
d_est = estimates(:,1);
phi_est = estimates(:,2);
thetaP_est = estimates(:,3);
vP_est = estimates(:,4);

switch strategy
    
    case 1
        delta_theta = (phi_est<pi)*(-pi/2 + 0.01) ...
            + (phi_est>pi)*(pi/2+ 0.01)...
            + (phi_est==pi).*(rand(size(phi_est))<0.5);
    case 2
        delta_theta = phi_est -pi;
    case 3
        %         delta_theta = (phi_est>pi).*((thetaP_est<-pi/2).*(thetaP_est+3*pi/2)...
        %             + (thetaP_est>=-pi/2 & thetaP_est<pi/2).*(thetaP_est+pi/2)...
        %             + (thetaP_est>=pi/2).*(thetaP_est-pi/2))...
        %             + (phi_est<pi).*((thetaP_est<-pi/2).*(thetaP_est+pi/2)...
        %             + (thetaP_est>=-pi/2 & thetaP_est<pi/2).*(thetaP_est-pi/2)...
        %             + (thetaP_est>=pi/2).*(thetaP_est-3*pi/2));
        %
        %         delta_theta = (phi_est<pi).*((wrapToPi(phi_est-thetaP_est)<pi).*(thetaP_est + pi/2)...
        %             + (wrapToPi(phi_est-thetaP_est)>pi).*(thetaP_est - pi/2))...
        %             + (phi_est>pi).*((wrapToPi(phi_est-thetaP_est)>pi).*(thetaP_est + pi/2)...
        %             + (wrapToPi(phi_est-thetaP_est)<pi).*(thetaP_est - pi/2));
        
        delta_theta = wrapToPi((wrapTo2Pi(phi_est-thetaP_est)>pi).*(thetaP_est + pi/2)...
            + (wrapTo2Pi(phi_est-thetaP_est)<pi).*(thetaP_est - pi/2));
        
    case 4
        K = vP_est/U;
        %                 delta_theta = (phi_est<pi).*(thetaP_est - acos(1./K))...
        %                     + (phi_est>pi).*(thetaP_est + acos(1./K));
        %                 delta_theta(K<1) = NaN;
        
        %         delta_theta = (phi_est<pi).*((wrapToPi(phi_est-thetaP_est)<pi).*(thetaP_est + acos(1./K))...
        %             + (wrapToPi(phi_est-thetaP_est)>pi).*(thetaP_est - acos(1./K)))...
        %             + (phi_est>pi).*((wrapToPi(phi_est-thetaP_est)>pi).*(thetaP_est + acos(1./K))...
        %             + (wrapToPi(phi_est-thetaP_est)<pi).*(thetaP_est - acos(1./K)));
        %         delta_theta(K<1) = NaN;
        %
        
        delta_theta = wrapToPi((wrapTo2Pi(phi_est-thetaP_est)>pi).*(thetaP_est + acos(1./K))...
            + (wrapTo2Pi(phi_est-thetaP_est)<pi).*(thetaP_est - acos(1./K)));
        
end
end


%% Function to sample from estimate distributions 
function [bHat,b] = generate_data(b_true, params, nreps)
sigmaD = params(1);
sigmaD_prior = params(2);
muD_prior = params(3);
kPhi = params(4);
kPhi_prior = params(5);
muPhi_prior = params(6);
kThetaP = params(7);
kThetaP_prior = params(8);
muThetaP_prior = params(9);
sigmaV = params(10);
sigmaV_prior = params(11);
muV_prior = params(12);

d = b_true(:,1);
phi = b_true(:,2);
thetaP = b_true(:,3);
vP = b_true(:,4);

d_true = repmat(d,nreps,1);
d_obs = normrnd(d_true,sigmaD);
dHat_sim = (d_obs/(sigmaD^2) + muD_prior/(sigmaD_prior^2))/(1/sigmaD^2 + 1/sigmaD_prior^2);

phi_true = repmat(phi,nreps,1);
phi_obs = vmrand(phi_true,kPhi);
phiHat_sim = wrapTo2Pi(phi_obs + atan2(sin(muPhi_prior-phi_obs),kPhi/kPhi_prior + cos(muPhi_prior-phi_obs)));

thetaP_true = repmat(thetaP,nreps,1);
thetaP_obs = vmrand(thetaP_true,kThetaP);
thetaPHat_sim = wrapTo2Pi(thetaP_obs + atan2(sin(muThetaP_prior-thetaP_obs),kThetaP/kThetaP_prior + cos(muThetaP_prior-thetaP_obs)));

vP_true = repmat(vP,nreps,1);
vP_obs = normrnd(vP_true,sigmaV);
vPHat_sim = (vP_obs/(sigmaV^2) + muV_prior/(sigmaV_prior^2))/(1/sigmaV^2 + 1/sigmaV_prior^2);

b(:,1) = d_true;
b(:,2) = phi_true;
b(:,3) = thetaP_true;
b(:,4) = vP_true;

bHat(:,1) = dHat_sim;
bHat(:,2) = phiHat_sim;
bHat(:,3) = thetaPHat_sim;
bHat(:,4) = vPHat_sim;
end

%% NLL for vonmises data
function nll = vonMisesNLL(params,obs)
mu = params(1);
kappa = params(2);
nll = sum(log(2*pi*besseli(0,kappa)) - kappa*cos(obs-mu));
end

%% NLL for von mises MAP estimate 
function nll = MAPvonMisesNLL(kappa,obs,true,prior)
mu_prior = prior(1);
kappa_prior = prior(2);

nll = sum(log(2*pi*besseli(0,kappa)) - kappa*cos(true-mu));
end