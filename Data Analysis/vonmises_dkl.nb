(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3100,        108]
NotebookOptionsPosition[      2422,         83]
NotebookOutlinePosition[      2775,         99]
CellTagsIndexPosition[      2732,         96]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"\[Kappa]", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"x", "-", 
      SubscriptBox["\[Mu]", "p"]}], "]"}], 
    RowBox[{"Exp", "[", 
     RowBox[{"\[Kappa]", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"x", "-", 
        SubscriptBox["\[Mu]", "p"]}], "]"}]}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"-", "\[Pi]"}], ",", "\[Pi]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7420347746311197`*^9, 
  3.7420348540360947`*^9}},ExpressionUUID->"375b5f52-b631-4979-8e1f-\
945e0dec0b97"],

Cell[BoxData[
 RowBox[{"2", " ", "\[Pi]", " ", "\[Kappa]", " ", 
  RowBox[{"BesselI", "[", 
   RowBox[{"1", ",", "\[Kappa]"}], "]"}]}]], "Output",
 CellChangeTimes->{{3.742034830592538*^9, 
  3.742034859751883*^9}},ExpressionUUID->"fe955fec-dbc9-4c5f-bb28-\
bb5482f777fa"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"\[Kappa]", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"x", "-", 
      SubscriptBox["\[Mu]", "q"]}], "]"}], 
    RowBox[{"Exp", "[", 
     RowBox[{"\[Kappa]", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"x", "-", 
        SubscriptBox["\[Mu]", "p"]}], "]"}]}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"-", "\[Pi]"}], ",", "\[Pi]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{
  3.74203502974459*^9},ExpressionUUID->"72eea234-d0c8-4f42-a4e0-e5aabcd1fc21"],

Cell[BoxData[
 RowBox[{"2", " ", "\[Pi]", " ", "\[Kappa]", " ", 
  RowBox[{"BesselI", "[", 
   RowBox[{"1", ",", "\[Kappa]"}], "]"}], " ", 
  RowBox[{"Cos", "[", 
   RowBox[{
    SubscriptBox["\[Mu]", "p"], "-", 
    SubscriptBox["\[Mu]", "q"]}], "]"}]}]], "Output",
 CellChangeTimes->{
  3.7420350375891953`*^9},ExpressionUUID->"4d3e873e-06cc-420b-8032-\
0f31c7806d65"]
}, Open  ]]
},
WindowSize->{808, 834},
WindowMargins->{{4, Automatic}, {Automatic, 4}},
FrontEndVersion->"11.1 for Mac OS X x86 (32-bit, 64-bit Kernel) (April 27, \
2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 586, 17, 35, "Input", "ExpressionUUID" -> \
"375b5f52-b631-4979-8e1f-945e0dec0b97"],
Cell[1169, 41, 272, 6, 32, "Output", "ExpressionUUID" -> \
"fe955fec-dbc9-4c5f-bb28-bb5482f777fa"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1478, 52, 555, 16, 35, "Input", "ExpressionUUID" -> \
"72eea234-d0c8-4f42-a4e0-e5aabcd1fc21"],
Cell[2036, 70, 370, 10, 69, "Output", "ExpressionUUID" -> \
"4d3e873e-06cc-420b-8032-0f31c7806d65"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

