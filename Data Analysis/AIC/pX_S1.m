function p = pX_S1(x,eta,varargin)

sig_S = eta(1);
sig_R = eta(2);


[D,N] = size(x);

if D~=5
    error('Invalid data size');
end

p = zeros(1,N);

if isempty(varargin)
    if length(eta) == 3
        p_phi   = 1/(2*pi);
        p_lam   = 1/(2*pi);
        p_u     = 1/(0.20);
        p_v     = 1/(0.20);
    elseif length(eta) == 10
        mu_phi  = eta(3);
        sig_phi = eta(4);
        q_lam   = eta(5);
        mu_u    = eta(6);
        sig_u   = eta(7);
        mu_v    = eta(8);
        sig_v   = eta(9);
    end
    for n = 1:N
        phi = x(1,n);
        lam = x(2,n);
        u   = x(3,n);
        v   = x(4,n);
        del = x(5,n);
        
        if length(eta) == 9
            p_phi = VonMises(phi,mu_phi,sig_phi);
            p_lam = PiecewiseUniform(lam,q_lam);
            p_u   = GammaDist(u,mu_u,sig_u);
            p_v   = GammaDist(v,mu_v,sig_v);
        end
        
        p(n)  = p_dp(phi,del,sig_S,sig_R)*...
                p_phi*...
                p_lam*...
                p_u*...
                p_v;
    end
end

end

function p = VonMises(x,mu,sig)

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end

function p = PiecewiseUniform(x,eta)

[Nx,Ny] = size(x);

q = eta(1);

p = zeros(Nx,Ny);

p( ((x > -pi) & (x < -pi/2)) | ((x > pi/2) & (x < pi)) ) = (1-q)/pi;
p( ((x >= -pi/2) & (x <= pi/2)) )                          = q/pi;

end

function p = GammaDist(x,mu,sig)

p = x.^(mu^2/sig^2-1).*...
    exp(-x*sig^2/mu)./...
  ((mu/sig^2)^(mu^2/sig^2)*...
    gamma(mu^2/sig^2));

end


function p = p_dp(phi,del,sig_S,sig_R)

p = 1/(2*pi*besseli(0,1/sig_R^2))*exp(cos(del+pi/2)/sig_R^2)*xi(phi,sig_S)+...
    1/(2*pi*besseli(0,1/sig_R^2))*exp(cos(del-pi/2)/sig_R^2)*(1-xi(phi,sig_S));
end

function out = xi(phi,sig_S)

out = integral(@(phih)integrand(phih,phi,sig_S),0,pi);

end

function out = integrand(phih,phi,sig_S)

out = 1/(2*pi*besseli(0,1/sig_S^2))*exp(cos(phih-phi)/sig_S^2);

end

