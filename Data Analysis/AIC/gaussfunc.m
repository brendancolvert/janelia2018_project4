function y = gaussfunc(x,sig)
y = 1/sqrt(2*pi*sig^2)*exp(-x.^2/(2*sig^2));
