clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%data = [ d phi V thp dtheta dispangle];

phi = data(:,2);
u   = data(:,9);
v   = data(:,3)*0.01;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);
lam = wrapToPi(tht-phi+pi);
chi = real(acos(u./v));

sinistral = lam >= 0;
dextral   = lam <  0;

chih(sinistral) = wrapToPi(tht(sinistral)-dlh(sinistral));
chih(dextral)   = wrapToPi(dlh(dextral)  -tht(dextral));

chid(sinistral) = wrapToPi(tht(sinistral)-dld(sinistral));
chid(dextral)   = wrapToPi(dld(dextral)  -tht(dextral));

Nbin = 20;
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [1 1 5 6];


subplot(1,3,1)
histogram(chi,linspace(-pi,pi,Nbin+1),'Normalization','pdf')
ylim([0 1.5])

xl = xlabel('$\chi$','Interpreter','latex');
yl = ylabel('$p_\chi$','Interpreter','latex');
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTick = (0:0.2:2.0);
ax.TickLabelInterpreter = 'latex';

subplot(1,3,2)
histogram(chid,linspace(-pi,pi,Nbin+1),'Normalization','pdf')
ylim([0 1.5])

xl = xlabel('$\chi_d$','Interpreter','latex');
yl = ylabel('$p_{\chi_d}$','Interpreter','latex');
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTick = (0:0.2:2.0);
ax.TickLabelInterpreter = 'latex';

subplot(1,3,3)
histogram(chih,linspace(-pi,pi,Nbin+1),'Normalization','pdf')
ylim([0 1.5])

xl = xlabel('$\chi_h$','Interpreter','latex');
yl = ylabel('$p_{\chi_h}$','Interpreter','latex');
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTick = (0:0.2:2.0);
ax.TickLabelInterpreter = 'latex';

print(f1,'s4_issues.eps','-depsc')

