clear all
close all
clc

u     = 0.1;
v     = 0.05;
sig_V = 0.03;

chihat = linspace(-pi,pi,801);

p = pchi(chihat,u,v,sig_V);

figure(1)
hold on
plot(chihat,real(p),'b-')
plot(chihat,imag(p),'r-')
ylim([-1 1]*4)

function out = pv(vhat,v,sig_V)

out = vhat.^(v^2/sig_V^2-1).*exp(-vhat*sig_V^2/v)/...
      (v/sig_V)^(v^2/sig_V^2)/...
      gamma(v^2/sig_V^2);

end

function out = gp(vhat,u)

out = u./(vhat.^2.*sqrt(1-u^2./vhat.^2));

end

function out = pchi(chihat,u,v,sig_V)

in = chihat > -pi/2 & chihat < pi/2;

out      =  real(pv(u./cos(chihat),v,sig_V)./abs(gp(u./cos(chihat),v)));
out(~in) = -imag(pv(u./cos(chihat(~in)),v,sig_V)./abs(gp(u./cos(chihat(~in)),v)));
    
end