function p = pX_S4(x,eta,varargin)

sig_S = eta(1);
sig_W = eta(2);
sig_V = eta(3);
sig_R = eta(4);

[D,N] = size(x);

if D~=5
    error('Invalid data size');
end

p = zeros(1,N);

if isempty(varargin)
    if length(eta) == 4
        p_phi   = 1/(2*pi);
        p_lam   = 1/(2*pi);
        p_u     = 1/(0.20);
        p_v     = 1/(0.20);
    elseif length(eta) == 11
        mu_phi  = eta(5);
        sig_phi = eta(6);
        q_lam   = eta(7);
        mu_u    = eta(8);
        sig_u   = eta(9);
        mu_v    = eta(10);
        sig_v   = eta(11);
    end
    for n = 1:N
        phi = x(1,n);
        lam = x(2,n);
        u   = x(3,n);
        v   = x(4,n);
        del = x(5,n);
        
        if length(eta) == 10
            p_phi = VonMises(phi,mu_phi,sig_phi);
            p_lam = PiecewiseUniform(lam,q_lam);
            p_u   = GammaDist(u,mu_u,sig_u);
            p_v   = GammaDist(v,mu_v,sig_v);
        end
        
        p(n)  = p_dp(phi,del,lam,u,v,sig_S,sig_W,sig_V,sig_R)*...
                p_phi*...
                p_lam*...
                p_u*...
                p_v;
    end
end

end

function p = VonMises(x,mu,sig)

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end


function p = p_dp(del,phi,lam,u,v,sig_S,sig_W,sig_V,sig_R)

p = integral(@(delhat)intfunc4(delhat,del,phi,lam,u,v,sig_S,sig_W,sig_V,sig_R),-pi,pi);
    
end

function out = intfunc4(delhat,del,phi,lam,u,v,sig_S,sig_W,sig_V,sig_R)

a1 = integral(@(ththat)intfunc2(ththat,delhat,phi,lam,u,v,sig_S,sig_W,sig_V),0,2*pi);
a2 = integral(@(ththat)intfunc3(ththat,delhat,phi,lam,u,v,sig_S,sig_W,sig_V),0,2*pi);
b  = blam(lam,sig_W);

out = (b*a1+(1-b)*a2)*VM(del,delhat,sig_R^2);

end

function out = pv(vhat,v,sig_V)

%sig_V = sig_V

out = vhat.^(v^2/sig_V^2-1).*exp(-vhat*sig_V^2/v)/...
      (v/sig_V)^(v^2/sig_V^2)/...
      gamma(v^2/sig_V^2);

end

function out = gp(vhat,u)

out = u./(vhat.^2.*sqrt(1-u^2./vhat.^2));

end

function out = pchi(chihat,u,v,sig_V)

in = chihat > -pi/2 & chihat < pi/2;

out      =  real(pv(u./cos(chihat),v,sig_V)./abs(gp(u./cos(chihat),v)));
out(~in) = -imag(pv(u./cos(chihat(~in)),v,sig_V)./abs(gp(u./cos(chihat(~in)),v)));
    
end

function out = ptht(ththat,phi,lam,sig_S,sig_W)
out = integral(@(phihat)intfunc1(phihat,ththat,phi,lam,sig_S,sig_W),0,2*pi);
end

function out = intfunc1(phihat,ththat,phi,lam,sig_S,sig_W)
out = VM(phihat,phi,sig_S^2).*VM(ththat,wrapToPi(phihat+lam+pi),sig_W^2);
end

function out = intfunc2(ththat,delhat,phi,lam,u,v,sig_S,sig_W,sig_V)
out = ptht(ththat,phi,lam,sig_S,sig_W).*pchi(wrapToPi(ththat-delhat),u,v,sig_V);
end

function out = intfunc3(ththat,delhat,phi,lam,u,v,sig_S,sig_W,sig_V)
out = ptht(ththat,phi,lam,sig_S,sig_W).*pchi(wrapToPi(delhat-ththat),u,v,sig_V);
end

function out = blam(lam,sig_W)
out = integral(@(lamhat)VM(lamhat,lam,sig_W^2),0,pi);
end

function out = VM(x,mu,sig2)

[Nx,Ny] = size(x);

if isinf(sig2)
    out = ones(Nx,Ny)*1/(2*pi);
elseif sig2 < 0.1^2
    sig = sqrt(sig2);
    out = gaussfunc(x-mu,sig);
else
    out = 1/(2*pi*besseli(0,1/sig2))*exp(cos(x-mu)/sig2);
end

end
