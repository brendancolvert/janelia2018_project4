function p = pX_S3(x,eta,varargin)

sig_S = eta(1);
sig_W = eta(2);
sig_R = eta(3);

[D,N] = size(x);

if D~=5
    error('Invalid data size');
end

p = zeros(1,N);

if isempty(varargin)
    if length(eta) == 3
        p_phi   = 1/(2*pi);
        p_lam   = 1/(2*pi);
        p_u     = 1/(0.20);
        p_v     = 1/(0.20);
    elseif length(eta) == 10
        mu_phi  = eta(4);
        sig_phi = eta(5);
        q_lam   = eta(6);
        mu_u    = eta(7);
        sig_u   = eta(8);
        mu_v    = eta(9);
        sig_v   = eta(10);
    end
    for n = 1:N
        phi = x(1,n);
        lam = x(2,n);
        u   = x(3,n);
        v   = x(4,n);
        del = x(5,n);
        
        if length(eta) == 10
            p_phi = VonMises(phi,mu_phi,sig_phi);
            p_lam = PiecewiseUniform(lam,q_lam);
            p_u   = GammaDist(u,mu_u,sig_u);
            p_v   = GammaDist(v,mu_v,sig_v);
        end
        
        p(n)  = p_dp(phi,del,lam,sig_S,sig_W,sig_R)*...
                p_phi*...
                p_lam*...
                p_u*...
                p_v;
    end
end

end

function p = VonMises(x,mu,sig)

if isinf(sig)
    p = 1/(2*pi);
else
    p = 1/(2*pi*besseli(0,1/sig^2))*exp(cos(x-mu)/sig^2);
end

end


function p = p_dp(del,phi,lam,sig_S,sig_W,sig_R)

b  = integral(@(lamh)intfunc2(lamh,lam,sig_W),0,pi);
a1 = integral2(@(phih,delh)intfunc1(phih,delh,phi,del,lam, pi/2,sig_S,sig_W,sig_R),0,2*pi,-pi,pi);
a2 = integral2(@(phih,delh)intfunc1(phih,delh,phi,del,lam,-pi/2,sig_S,sig_W,sig_R),0,2*pi,-pi,pi);

p = a1*b+a2*(1-b); 

end

function out = intfunc1(phih,delh,phi,del,lam,offset,sig_S,sig_W,sig_R)

out = VM(del,delh,sig_R^2).*...
      VM(phih,phi,sig_S^2).*...
      VM(delh+offset,phih+lam+pi,sig_W^2);

end

function out = intfunc2(lamh,lam,sig_W)

out = VM(lamh,lam,sig_W^2);

end

function out = VM(x,mu,sig2)

[Nx,Ny] = size(x);

if isinf(sig2)
    out = ones(Nx,Ny)*1/(2*pi);
elseif sig2 < 0.1^2
    sig = sqrt(sig2);
    out = gaussfunc(x-mu,sig);
else
    out = 1/(2*pi*besseli(0,1/sig2))*exp(cos(x-mu)/sig2);
end

end
