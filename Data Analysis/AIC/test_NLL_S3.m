clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s


d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

Nsig = 11;
sig_S = linspace(0.1,0.5,Nsig)*pi;
sig_Q = linspace(0.1,0.5,Nsig)*pi;
sig_R = linspace(0.1,0.5,Nsig)*pi;

if ~exist('NLL_data_S3.mat','file')

    for sind = 1:length(sig_S)
        for qind = 1:length(sig_Q)
            for rind = 1:length(sig_R)
                fprintf('%i,%i,%i\n',sind,qind,rind);
                nll(sind,qind, rind) = NLL_S3([sig_S(sind);...
                                               sig_Q(qind);...
                                               sig_R(rind)],...
                                              [phi';...
                                               lam';...
                                               dlh']);
            end
        end
    end
    save('NLL_data_S3.mat','nll');
else
    load('NLL_data_S3.mat');
end


pause

%% Plot


min(nll(:))
numc = 40;
clim = [3.38 3.50];

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 3.2 2.7 ];


contourf(sig_S,sig_R,nll',[-realmax linspace(clim(1),clim(2),numc) realmax],'LineStyle','none')
colormap(cmap(1:end/2,:))
cb = colorbar;
caxis(clim)

hold on
box on

plot(0.68252,0.68252,'r.','MarkerSize',12);

xlabel('$\sigma_S$','Interpreter','latex');
ylabel('$\sigma_R$','Interpreter','latex');

ylabel(cb,'$-\log \mathcal{L}$','Interpreter','latex');

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (0.1:0.2:0.5)*pi;
ax1.YTick = (0.1:0.2:0.5)*pi;
ax1.TickLabelInterpreter = 'latex';
ax1.XTickLabels = {'$0.1\pi$','$0.3\pi$','$0.5\pi$'};
ax1.YTickLabels = {'$0.1\pi$','$0.3\pi$','$0.5\pi$'};

print(f1,'NLL_ls_02.eps','-depsc')


