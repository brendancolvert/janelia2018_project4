clear all
close all
clc

Nx = 101;
phi = linspace(0,2,Nx)*pi;
del = linspace(-1,1,Nx)*pi;

[del,phi] = meshgrid(del,phi);

sig_S   = 0.35*pi;
sig_R   = 0.20*pi;

mu_phi  = pi;
sig_phi = pi;

p = pX_S1([phi(:)';...
           del(:)'],...
          [sig_S;...
           sig_R;...
           mu_phi;...
           sig_phi]);
p = reshape(p,Nx,Nx);


%% Plot

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 2 2 ];
       
numc = 40;
contourf(phi,del,p,numc,'LineStyle','none')
colormap(flipud(gray))

ax1 = gca;
ax1.FontName = 'Times New Roman';
ax1.FontSize = 8;
ax1.XTick = (-2:1:2)*pi;
ax1.YTick = (-2:1:2)*pi;
ax1.TickLabelInterpreter = 'latex';
ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');

print(f1,'testDist_S2.eps','-depsc');