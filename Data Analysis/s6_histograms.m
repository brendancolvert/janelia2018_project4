clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

sin = lam < 0;
dex = ~sin;

dfh(sin) = wrapToPi(dlh(sin)-wrapToPi(lam(sin)+phi(sin)+pi+chi(sin)));
dfh(dex) = wrapToPi(dlh(dex)-wrapToPi(lam(dex)+phi(dex)+pi-chi(dex)));

dfd(sin) = wrapToPi(dld(sin)-wrapToPi(lam(sin)+phi(sin)+pi+chi(sin)));
dfd(dex) = wrapToPi(dld(dex)-wrapToPi(lam(dex)+phi(dex)+pi-chi(dex)));

Nbin = 16;

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [1 1 14 8];
f1.Color = 'k';

subplot(2,4,1)
plot(phi,dlh,'.','MarkerSize',6,'Color','w')
xlim([0 2*pi])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-2:1:2);
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

%xlabel('$\Phi$','Interpreter','latex')
ylabel('$\Delta_h$','Interpreter','latex')

subplot(2,4,5)
plot(phi,dld,'.','MarkerSize',6,'Color','w')
xlim([0 2*pi])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-2:1:2);
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$\Phi$','Interpreter','latex')
ylabel('$\Delta_d$','Interpreter','latex')


subplot(2,4,2)
plot(lam,dlh,'.','MarkerSize',6,'Color','w')
xlim([-pi pi])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-2:1:2);
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

%xlabel('$\Lambda$','Interpreter','latex')
%ylabel('$\Delta_h$','Interpreter','latex')

subplot(2,4,6)
plot(lam,dld,'.','MarkerSize',6,'Color','w')
xlim([-pi pi])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-2:1:2);
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$\Lambda$','Interpreter','latex')
%ylabel('$\Delta_d$','Interpreter','latex')

subplot(2,4,3)
plot(U,dlh,'.','MarkerSize',6,'Color','w')
xlim([0 0.2])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = 0:0.1:0.2;
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

%xlabel('$U$','Interpreter','latex')
%ylabel('$\Delta_h$','Interpreter','latex')

subplot(2,4,7)
plot(U,dld,'.','MarkerSize',6,'Color','w')
xlim([0 0.2])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = 0:0.1:0.2;
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$U$','Interpreter','latex')
%ylabel('$\Delta_d$','Interpreter','latex')



subplot(2,4,4)
plot(V,dlh,'.','MarkerSize',6,'Color','w')
xlim([0 0.2])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = 0:0.1:0.2;
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

%xlabel('$V$','Interpreter','latex')
%ylabel('$\Delta_h$','Interpreter','latex')

subplot(2,4,8)
plot(V,dld,'.','MarkerSize',6,'Color','w')
xlim([0 0.2])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = 0:0.1:0.2;
ax.YTick = pi*(-2:1:2);
ax.TickLabelInterpreter = 'latex';
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$V$','Interpreter','latex')
%ylabel('$\Delta_d$','Interpreter','latex')




print(f1,'s6_histograms.eps','-depsc','-painters')


















