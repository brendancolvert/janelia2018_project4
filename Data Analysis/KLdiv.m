function kldiv = KLdiv(X,Y,k)

[D,N] = size(X);
[~,M] = size(Y);

kldiv = log(M/(N-1));

for n = 1:N
    Xn   = X(:,n);
    dX   = sort(sqrt(sum((Xn-X).^2,1)),'ascend');
    dY   = sort(sqrt(sum((Xn-Y).^2,1)),'ascend');
    rhoX = dX(k+1);
    rhoY = dY(k);
    kldiv = kldiv + D/N*log(rhoY/rhoX);
end


end
