clear all
close all
clc

dataset = [];

for vnum = 1:3
    load(['responseData_' num2str(vnum) '.mat']);
    dataset = [dataset;data];
end

dataset = [dataset(1:436,:);...
           dataset(438:end,:)];

%data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle'];
d   = dataset(:,1);
phi = wrapTo2Pi(dataset(:,2));
V   = dataset(:,3);
thp = dataset(:,4);
dth = dataset(:,5);
del = dataset(:,6);

Nprey = length(d);


% % % COMPUTE MARGINALS % % % 

% Phi Marginal
A = [-1  0;...
      1  0;...
      0 -1];
  
eps = 10^(-10);

b = [0;...
     2*pi;...
     -eps];
 
x0 = [pi;...
      0.1*pi];
  
x = fmincon(@(x)negLLVM(x,phi),x0,A,b);

mu_phi = x(1);
kp_phi = x(2);

% Theta_p Marginal
A = [-1  0;...
      1  0;...
      0 -1];
  
eps = 10^(-10);

b = [0;...
     2*pi;...
     -eps];
 
x0 = [pi;...
      0.1*pi];
  
x = fmincon(@(x)negLLVM(x,thp),x0,A,b);

mu_thp = x(1);
kp_thp = x(2);

% Delta theta Marginal
A = [-1  0;...
      1  0;...
      0 -1];
  
eps = 10^(-10);

b = [0;...
     2*pi;...
     -eps];
 
x0 = [pi;...
      0.1*pi];
  
x = fmincon(@(x)negLLVM(x,dth),x0,A,b);

mu_dth = x(1);
kp_dth = x(2);


% V Marginal
A = [0 -1];
  
eps = 10^(-10);
b = [eps];
 
x0 = [1;...
      0.1];
  
x = fmincon(@(x)negLLGS(x,V),x0);

mu_V = x(1);
sg_V = x(2);

Nbin = 41;



% % Delta Theta -  vs. Thp % %
f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 1 1 5 5 ];
f11.PaperPositionMode = 'auto';


% Marginal
subplot(6,6,2:6:20)
box on
hold on
histogram(dth,Nbin,'Normalization','pdf',...
    'FaceColor',[ 1 1 1 ]*0.8,...
    'EdgeColor',[ 0 0 0 ])

xlabel('$\Delta \theta$','Interpreter','latex');
xlim([-1 1]*pi);

%thpdist = linspace(-1,1,101)*pi;
%pdfthp  = vonMisesPDF(dthdist,mu_thp,kp_thp);

% plot(thpdist,pdfthp,'-',...
%      'Color',[0.8 0 0],...
%      'LineWidth',2);

view([90 -90])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.XTick = (-1:0.5:1)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.YTickLabels = [];

% Marginal
subplot(6,6,27:30)
box on
hold on
histogram(wrapTo2Pi(thp),Nbin,'Normalization','pdf',...
    'FaceColor',[ 1 1 1 ]*0.8,...
    'EdgeColor',[ 0 0 0 ])

xlabel('$\theta_p$','Interpreter','latex');
xlim([0 2]*pi);

% phidist = linspace(0,2,101)*pi;
% pdfphi  = vonMisesPDF(phidist,mu_phi,kp_phi);
% 
% plot(phidist,pdfphi,'-',...
%      'Color',[0.8 0 0],...
%      'LineWidth',2);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.XTick = (0:0.5:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
ax.YTickLabels = [];

% Joint
subplot(6,6,[3:6 9:12 15:18 21:24])
hold on

plot(wrapTo2Pi(thp),dth,'k.')
xlim([0 2]*pi)
ylim([-1 1]*pi)
plot([0 2 2 0 0]*pi,[-1 -1 1 1 -1]*pi,'k-','LineWidth',0.5)

plot([0 0.5]*pi,[0.5 1]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([0 1.5]*pi,[-0.5 1]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([0.5 2]*pi,[-1 0.5]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([1.5 2]*pi,[-1 -0.5]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

ax = gca;
ax.XTick = (0:0.5:2)*pi;
ax.YTick = (-1:0.5:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


%print(f11,'dth_thp.eps','-depsc','-painters')
print(f11,'dth_thp_st3.jpg','-djpeg','-r300')





% % Delta Theta -  vs. Thp % %
f12 = figure(12);
f12.Units = 'inches';
f12.Position = [ 1 1 5 5 ];
f12.PaperPositionMode = 'auto';


% % Marginal
% subplot(6,6,2:6:20)
% box on
% hold on
% histogram(dth,Nbin,'Normalization','pdf',...
%     'FaceColor',[ 1 1 1 ]*0.8,...
%     'EdgeColor',[ 0 0 0 ])
% 
% xlabel('$\Delta \theta$','Interpreter','latex');
% xlim([-1 1]*pi);
% 
% %thpdist = linspace(-1,1,101)*pi;
% %pdfthp  = vonMisesPDF(dthdist,mu_thp,kp_thp);
% 
% % plot(thpdist,pdfthp,'-',...
% %      'Color',[0.8 0 0],...
% %      'LineWidth',2);
% 
% view([90 -90])
% ax = gca;
% ax.FontName = 'Times New Roman';
% ax.FontSize = 10;
% ax.XTick = (-1:0.5:1)*pi;
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
% ax.YTickLabels = [];

% % Marginal
% subplot(6,6,27:30)
% box on
% hold on
% histogram(wrapTo2Pi(thp),Nbin,'Normalization','pdf',...
%     'FaceColor',[ 1 1 1 ]*0.8,...
%     'EdgeColor',[ 0 0 0 ])
% 
% xlabel('$\theta_p$','Interpreter','latex');
% xlim([0 2]*pi);
% 
% % phidist = linspace(0,2,101)*pi;
% % pdfphi  = vonMisesPDF(phidist,mu_phi,kp_phi);
% % 
% % plot(phidist,pdfphi,'-',...
% %      'Color',[0.8 0 0],...
% %      'LineWidth',2);
% 
% ax = gca;
% ax.FontName = 'Times New Roman';
% ax.FontSize = 10;
% ax.XTick = (0:0.5:2)*pi;
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
% ax.YTickLabels = [];

% Joint
subplot(6,6,[3:6 9:12 15:18 21:24])
hold on

%plot(wrapTo2Pi(thp),dth,'k.')
xlim([0 2]*pi)
ylim([-1 1]*pi)
plot([0 2 2 0 0]*pi,[-1 -1 1 1 -1]*pi,'k-','LineWidth',0.5)

plot([0 0.5]*pi,[0.5 1]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([0 1.5]*pi,[-0.5 1]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([0.5 2]*pi,[-1 0.5]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

plot([1.5 2]*pi,[-1 -0.5]*pi,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

ax = gca;
ax.XTick = (0:0.5:2)*pi;
ax.YTick = (-1:0.5:1)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
ax.YTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};

xlabel('$\theta_p$','Interpreter','latex');
ylabel('$\Delta \theta$','Interpreter','latex');


%print(f12,'dth_thp_blank.eps','-depsc','-painters')
print(f12,'dth_thp_st3_blank.jpg','-djpeg','-r300')























% % 
% % phidist = linspace(0,2*pi,201);
% % pdfphi  = vonMisesPDF(phidist,x(1),x(2));
% % for phinum = 1:length(phidist)
% %     cdfphi(phinum)  = integral(@(phi)vonMisesPDF(phi,x(1),x(2)),0,phidist(phinum));
% % end
% % 
% % 
% % f11 = figure(11);
% % f11.Units = 'inches';
% % f11.Position = [1 1 4 3];
% % Nbin = 41;
% % hold on
% % histogram(phi,Nbin,'Normalization','pdf',...
% %     'FaceAlpha',0.5,...
% %     'FaceColor',[ 0 0 0 ],...
% %     'EdgeColor',[ 0 0 0 ])
% % 
% % 
% % plot(phidist,pdfphi,...
% %      '-',...
% %      'LineWidth',2,...
% %      'Color',[ 0.7 0 0 ])
% % xlim([0 2*pi])
% % ylim([0 0.4])
% % 
% % % subplot(1,2,2)
% % % hold on
% % % histogram(phi,Nbin,'Normalization','cdf')
% % % plot(phidist,cdfphi,'r-')
% % % xlim([0 2*pi])
% % % ylim([0 1])
% % 
% % ax1 = gca;
% % ax1.FontName = 'Times New Roman';
% % ax1.FontSize = 16;
% % ax1.TickLabelInterpreter = 'latex';
% % ax1.XTick = (0:0.5:2)*pi;
% % ax1.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
% % ax1.YTick = 0:0.2:0.4;
% % 
% % xl = xlabel('$\phi$');
% % xl.FontName = 'Times New Roman';
% % xl.FontSize = 16;
% % xl.Interpreter = 'latex';
% % 
% % yl = ylabel('$p(\phi)$');
% % yl.FontName = 'Times New Roman';
% % yl.FontSize = 16;
% % yl.Interpreter = 'latex';
% % 
% % 
% % print(f11,'phi_dist.eps','-depsc','-painters')
% % 
% % f12 = figure(12);
% % 
% % plot(phi,dth_unrwap,...
% %      '.',...
% %      'Color',[ 0 0 0 ])
% % xlim([0 2*pi])
% % ylim([0 0.4])
% % 
% % 
% % 
% % 
% % dth_unwrap = zeros(Nprey,1);
% % for nprey = 1:Nprey
% %     if dth(nprey)-phi(nprey) > 0
% %         dth_unwrap(nprey) = dth(nprey)-2*pi;
% %     elseif dth(nprey)-phi(nprey)+2*pi < 0
% %         dth_unwrap(nprey) = dth(nprey)+2*pi;
% %     else
% %         dth_unwrap(nprey) = dth(nprey);
% %     end
% % end






function pdf = vonMisesPDF(var,mu,kp)
pdf = 1./(2*pi*besseli(0,kp)).*exp(kp.*cos(var-mu));
end

function pdf = gaussianPDF(var,mu,sg)
eps = 10^(-10);
pdf = max([eps 1./sqrt(2*pi*sg.^2).*exp(-(var-mu).^2./(2*sg.^2))]);
end

function nll = negLLVM(x,phi_obs)

mu = x(1);
kp = x(2);

Nobs = length(phi_obs);
nll = 0;
for nobs = 1:Nobs
    nll = nll - log(vonMisesPDF(phi_obs(nobs),mu,kp));
end

end

function nll = negLLGS(x,phi_obs)

mu = x(1);
sg = x(2);

Nobs = length(phi_obs);
nll = 0;
for nobs = 1:Nobs
    nll = nll - log(gaussianPDF(phi_obs(nobs),mu,sg));
end

end