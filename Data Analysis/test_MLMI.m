clear all
close all
clc

Nx = 1001;
x = randn(Nx,1);
y = x + 0.1*randn(Nx,1);

out = MLMI(x',y',0);