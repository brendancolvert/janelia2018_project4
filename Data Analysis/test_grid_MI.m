clear all
close all
clc

N = 1000;

mu = [1 -1]; 
SIGMA = [.9 .4; .4 .3]; 
X = mvnrnd(mu,SIGMA,10); 
p = mvnpdf(X,mu,SIGMA); 


grids = [2:200];

for ng = 1:length(grids)
    I(ng) = grid_MI(X(:,1)',X(:,2)',grids(ng),grids(ng));
end
plot(grids,I,'ko-')