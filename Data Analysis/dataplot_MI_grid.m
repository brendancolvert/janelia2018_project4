clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);

U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
thp = data(:,4);
dth = data(:,5);
del = data(:,6);

data_in  = data(:,[2 4 1 3]);
data_out = data(:,5:6);

names_in  = {'d','phi','V','thp'};
names_out = {'dtheta','delta'};


Nc = 200;
Ng = 11;

for jv = 1:4
    for kv = 1:2
        fprintf([names_in{jv} ' vs. ' names_out{kv} ': \n']);
        I_g(jv,kv) = grid_MI(data_in(:,jv)',data_out(:,kv)',Ng,Ng);
% %         for nt = 1:Nt
% %             out(nt) = MLMI(data_in(:,jv)',data_out(:,kv)',0,[],Nc);
% %             fprintf(['\t\t ' num2str(nt) ' - ' num2str(out(nt)) '\n']);
% %         end
% %         I_mu(jv,kv) = mean(out);
% %         I_sg(jv,kv) = std(out);
        %fprintf('I = %6.3f�%6.3f\n',mean(out),std(out));
        fprintf('I = %6.3f\n',I_g(jv,kv));
    end
end

%%


LineSpec   = 'k.';
MarkerSize = 3;
FaceColor  = [1 1 1]*0.7;
EdgeColor  = [0 0 0];

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [2 2 7.0 3.5];

Nbin = 41;

% dth
subplot(7,13,[1 14 27])
histogram(dth,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\Delta \theta$','Interpreter','latex')

view([90 -90])

% dth
subplot(7,13,[40 53 66])
histogram(del,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

xlabel('$\delta$','Interpreter','latex')

view([90 -90])

% phi
subplot(7,13,[80 81 82])
histogram(phi,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([0 2]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
%ax.XTickLabelRotation = 90;

xlabel('$\phi$','Interpreter','latex')

% thp
subplot(7,13,[83 84 85])
histogram(thp,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
%ax.XTickLabelRotation = 90;

xlabel('$\theta_p$','Interpreter','latex')

% d
subplot(7,13,[86 87 88])
histogram(d,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([0 0.06]);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.03:0.06;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0.00$','$0.03$','$0.06$'};
%ax.XTickLabelRotation = 90;

xlabel('$d$','Interpreter','latex')

% V
subplot(7,13,[89 90 91])
histogram(V,Nbin,...
    'FaceColor',FaceColor,...
    'EdgeColor',EdgeColor)

xlim([0 0.2]);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.1:0.2;
ax.YTick = [];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0.0$','$0.1$','$0.2$'};
%ax.XTickLabelRotation = 90;

xlabel('$V$','Interpreter','latex')



% dth vs phi
subplot(7,13,[2 3 4 15 16 17 28 29 30])
hold on
box on
plot(phi,dth,LineSpec,'MarkerSize',MarkerSize)

xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.4*pi*[-1 1 1 -1 -1]+1*pi,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(1*pi,0.8*pi,...
          sprintf('I = %6.3f',I_g(1,1)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);


% % dth vs thp
subplot(7,13,[5 6 7 18 19 20 31 32 33])
hold on
box on
plot(thp,dth,LineSpec,'MarkerSize',MarkerSize)

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.4*pi*[-1 1 1 -1 -1],0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0*pi,0.8*pi,...
          sprintf('I = %6.3f',I_g(2,1)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);


% % dth vs d
subplot(7,13,[8 9 10 21 22 23 34 35 36])
hold on
box on
plot(d,dth,LineSpec,'MarkerSize',MarkerSize)

xlim([0 0.06]);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.03:0.06;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.012*[-1 1 1 -1 -1]+0.03,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0.03,0.8*pi,...
          sprintf('I = %6.3f',I_g(3,1)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);

% % dth vs V
subplot(7,13,[11 12 13 24 25 26 37 38 39])
hold on
box on
plot(V,dth,LineSpec,'MarkerSize',MarkerSize)

xlim([0 0.2]);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.1:0.2;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.04*[-1 1 1 -1 -1]+0.1,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0.1,0.8*pi,...
          sprintf('I = %6.3f',I_g(4,1)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);


% del vs phi
subplot(7,13,[41 42 43 54 55 56 67 68 69])
hold on
box on
plot(phi,del,LineSpec,'MarkerSize',MarkerSize)

xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.4*pi*[-1 1 1 -1 -1]+1*pi,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(1*pi,0.8*pi,...
          sprintf('I = %6.3f',I_g(1,2)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);

% del vs thp
subplot(7,13,[44 45 46 57 58 59 70 71 72])
hold on
box on
plot(thp,del,LineSpec,'MarkerSize',MarkerSize)

xlim([-1 1]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = [-2:1:2]*pi;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.4*pi*[-1 1 1 -1 -1],0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0*pi,0.8*pi,...
          sprintf('I = %6.3f',I_g(2,2)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);

% % del vs d
subplot(7,13,[47 48 49 60 61 62 73 74 75])
hold on
box on
plot(d,del,LineSpec,'MarkerSize',MarkerSize)

xlim([0 0.06]);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.03:0.06;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};

fill(0.012*[-1 1 1 -1 -1]+0.03,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0.03,0.8*pi,...
          sprintf('I = %6.3f',I_g(3,2)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);

% % dth vs V
subplot(7,13,[50 51 52 63 64 65 76 77 78])
hold on
box on
plot(V,del,LineSpec,'MarkerSize',MarkerSize)

xlim([0 0.2]);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.1:0.2;
ax.YTick = [-2:1:2]*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = [];
ax.YTickLabels = [];%{'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};


fill(0.04*[-1 1 1 -1 -1]+0.1,0.12*pi*[-1 -1 1 1 -1]+0.8*pi,...
    'w',...
    'EdgeColor',[0 0 0])

tx = text(0.1,0.8*pi,...
          sprintf('I = %6.3f',I_g(4,2)),...
          'HorizontalAlign','center',...
          'FontName','Times New Roman',...
          'FontSize',8);



print(f1,'dataplot_MI_grid.eps','-depsc','-painters')








    











