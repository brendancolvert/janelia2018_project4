clear all
close all
clc

Vnum = [1 2 3];


% Color Definitions
% Left vs. Right Phi
clr_left = [117 190 228]/255;
flr_rght = [231 136 102]/255;

% Left vs. Right Lambda
clr_left = [088 078 137]/255;
flr_rght = [097 133 072]/255;

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s

i_20 = 1:215;
i_11 = 216:448;
i_02 = 449:699;

V_20 = data(i_20,3)*0.01;
V_11 = data(i_11,3)*0.01;
V_02 = data(i_02,3)*0.01;

d_20 = data(i_20,1);
d_11 = data(i_11,1);
d_02 = data(i_02,1);

U_20 = data(i_20,9);
U_11 = data(i_11,9);
U_02 = data(i_02,9);

tht_20 = data(i_20,4);
tht_11 = data(i_11,4);
tht_02 = data(i_02,4);

phi_20 = data(i_20,2);
phi_11 = data(i_11,2);
phi_02 = data(i_02,2);

lam_20 = wrapToPi(tht_20-phi_20+pi);
lam_11 = wrapToPi(tht_11-phi_11+pi);
lam_02 = wrapToPi(tht_02-phi_02+pi);

chi_20 = real(acos(U_20./V_20));
chi_11 = real(acos(U_11./V_11));
chi_02 = real(acos(U_02./V_02));


dlh_20 = data(i_20,5);
dlh_11 = data(i_11,5);
dlh_02 = data(i_02,5);

dld_20 = data(i_20,6);
dld_11 = data(i_11,6);
dld_02 = data(i_02,6);

Nbin = 40;

figure(1)
hold on
histogram(chi_20,linspace(0,pi/2,Nbin))
histogram(chi_11,linspace(0,pi/2,Nbin))
histogram(chi_02,linspace(0,pi/2,Nbin))
xlim([0 pi/2])
legend('V=20','V=11','V=2')
ax1 = gca;
ax1.XTick = (0:0.25:0.5)*pi;
ax1.XTickLabels = {'0','0.25\pi','0.5\pi'};


figure(2)
hold on
subplot(3,1,1)
histogram(U_20,linspace(0,0.2,Nbin))
title('V=20')
xlim([0 1]*0.20)

subplot(3,1,2)
histogram(U_11,linspace(0,0.2,Nbin))
title('V=11')
xlim([0 1]*0.20)

subplot(3,1,3)
histogram(U_02,linspace(0,0.2,Nbin))
xlim([0 1]*0.20)
title('V=2')



figure(3)
hold on
subplot(3,1,1)
histogram(d_20,linspace(0,0.06,Nbin))
title('V=20')
xlim([0 1]*0.06)
ylim([0 30])

subplot(3,1,2)
histogram(d_11,linspace(0,0.06,Nbin))
title('V=11')
xlim([0 1]*0.06)
ylim([0 30])

subplot(3,1,3)
histogram(d_02,linspace(0,0.06,Nbin))
xlim([0 1]*0.06)
ylim([0 30])
title('V=2')


figure(4)
hold on
subplot(3,1,1)
histogram(phi_20,linspace(0,2*pi,Nbin))
title('V=20')
xlim([0 2]*pi)
ylim([0 30])

subplot(3,1,2)
histogram(phi_11,linspace(0,2*pi,Nbin))
title('V=11')
xlim([0 2]*pi)
ylim([0 30])

subplot(3,1,3)
histogram(phi_02,linspace(0,2*pi,Nbin))
xlim([0 1]*0.06)
xlim([0 2]*pi)
title('V=2')

figure(5)
hold on
subplot(3,1,1)
histogram(lam_20,linspace(-pi,pi,Nbin))
title('V=20')
xlim([-1 1]*pi)
ylim([0 30])

subplot(3,1,2)
histogram(lam_11,linspace(-pi,pi,Nbin))
title('V=11')
xlim([-1 1]*pi)
ylim([0 30])

subplot(3,1,3)
histogram(lam_02,linspace(-pi,pi,Nbin))
xlim([-1 1]*pi)
ylim([0 30])
title('V=2')

figure(6)
hold on
subplot(3,1,1)
histogram(dlh_20,linspace(-pi,pi,Nbin))
title('V=20 (Heading)')
xlim([-1 1]*pi)
ylim([0 20])

subplot(3,1,2)
histogram(dlh_11,linspace(-pi,pi,Nbin))
title('V=11')
xlim([-1 1]*pi)
ylim([0 20])

subplot(3,1,3)
histogram(dlh_02,linspace(-pi,pi,Nbin))
xlim([-1 1]*pi)
ylim([0 20])
title('V=2')

figure(7)
hold on
subplot(3,1,1)
histogram(dld_20,linspace(-pi,pi,Nbin))
title('V=20 (Displacement)')
xlim([-1 1]*pi)
ylim([0 20])

subplot(3,1,2)
histogram(dld_11,linspace(-pi,pi,Nbin))
title('V=11')
xlim([-1 1]*pi)
ylim([0 20])

subplot(3,1,3)
histogram(dld_02,linspace(-pi,pi,Nbin))
xlim([-1 1]*pi)
ylim([0 20])
title('V=2')


f8 = figure(8);
f8.Units = 'inches';
f8.Position = [ 1 1 12 3];
subplot(1,3,1)
plot(dlh_20,dld_20,'k.','MarkerSize',12);
axis(pi*[-1 1 -1 1])
title('V=20')
xlabel('\delta_h')
ylabel('\delta_d')

subplot(1,3,2)
plot(dlh_11,dld_11,'k.','MarkerSize',12);
axis(pi*[-1 1 -1 1])
title('V=11')
xlabel('\delta_h')
ylabel('\delta_d')

subplot(1,3,3)
plot(dlh_02,dld_02,'k.','MarkerSize',12);
axis(pi*[-1 1 -1 1])
title('V=2')
xlabel('\delta_h')
ylabel('\delta_d')

print(f8,'output_corr.eps','-depsc')



f9 = figure(9);
f9.Units = 'inches';
f9.Position = [ 1 1 12 3];
subplot(1,3,1)
plot(phi_20,lam_20,'k.','MarkerSize',12);
axis(pi*[0 2 -1 1])
title('V=20')
xlabel('\phi')
ylabel('\lambda')

subplot(1,3,2)
plot(phi_11,lam_11,'k.','MarkerSize',12);
axis(pi*[0 2 -1 1])
title('V=11')
xlabel('\phi')
ylabel('\lambda')

subplot(1,3,3)
plot(phi_02,lam_02,'k.','MarkerSize',12);
axis(pi*[0 2 -1 1])
title('V=2')
xlabel('\phi')
ylabel('\lambda')

print(f9,'phi_lam_corr.eps','-depsc')


%% 

orn = [231 136 102]/255;
blu = [117 190 228]/255;

f10 = figure(10);
f10.Units = 'inches';
f10.Position = [ 1 1 12 3];
subplot(1,3,1)
plot(phi_20(con_dld_20),dld_20(con_dld_20),'.','MarkerSize',12,'Color',blu);
hold on
box on
plot(phi_20(ips_dld_20),dld_20(ips_dld_20),'.','MarkerSize',12,'Color',orn);
axis(pi*[0 2 -1 1])
title('V=20')
xlabel('\phi')
ylabel('\delta_d')

subplot(1,3,2)
plot(phi_11(con_dld_11),dld_11(con_dld_11),'.','MarkerSize',12,'Color',blu);
hold on
box on
plot(phi_11(ips_dld_11),dld_11(ips_dld_11),'.','MarkerSize',12,'Color',orn);
axis(pi*[0 2 -1 1])
title('V=11')
xlabel('\phi')
ylabel('\delta_d')

subplot(1,3,3)
plot(phi_02(con_dld_02),dld_02(con_dld_02),'.','MarkerSize',12,'Color',blu);
hold on
box on
plot(phi_02(ips_dld_02),dld_02(ips_dld_02),'.','MarkerSize',12,'Color',orn);
axis(pi*[0 2 -1 1])
title('V=2')
xlabel('\phi')
ylabel('\delta_d')

print(f10,'con_ips.eps','-depsc')











