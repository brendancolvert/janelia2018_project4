clear all
close all
clc
       
sig_p = 1;
sig_q = logspace(-2,2,101);

KLD = 0.5*((sig_p./sig_q).^2-1)-log(sig_p./sig_q);

rng(1)

N = 500;
M = 500;

X = sig_p*randn(1,N);

ks = [1 10 50 100];
for sind = 1:length(sig_q)
    Y = sig_q(sind)*randn(1,M);
    for kind = 1:length(ks)
        k = ks(kind);
        kld(sind,kind) = max([KLdiv(X,Y,k),0]);
    end
end
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 3 3 ];

loglog(sig_q,KLD,...
       'k-',...
       'LineWidth',2)
hold on
for kind = 1:length(ks)
    plot(sig_q,kld(:,kind),'-')
end
axis equal
xlim(10.^([-2 2]))
ylim(10.^([-2 2]))


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 10.^(-2:1:2);
ax.YTick = 10.^(-2:1:2);

xlabel('$\sigma_q/\sigma_p$',...
       'Interpreter','latex');
ylabel('$\mathcal{D}(p \| q )$',...
       'Interpreter','latex');
   
lns = lines;
for kind = 1:length(ks)
    text(10^(-1.8),kld(1,kind)*1.2,['$k=' num2str(ks(kind)) '$'],...
        'FontName','Times New Roman',...
        'FontSize',8,...
        'Interpreter','latex',...
        'Color',lns(kind,:));
end

text(10^(-0.6),10^(1),'Analytical',...
    'FontName','Times New Roman',...
    'FontSize',8,...
    'Interpreter','latex',...
    'Color',[ 0 0 0 ]);


print(sprintf('kldiv_test_%i_%i.eps',N,M),'-depsc','-painters')



























