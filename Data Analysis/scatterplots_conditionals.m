clear all
close all
clc

Vnum = [1 2 3];


% Color Definitions
% Left vs. Right Phi
colors.left  = [190 030 045]/255;
colors.right = [046 049 146]/255;

% Left vs. Right Lambda
colors.sinistral = [102 045 145]/255;
colors.dextral  = [000 104 056]/255;

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s

%% Load Data
V   = data(:,3)*0.01;
d   = data(:,1);
U   = data(:,9);
tht = data(:,4);
phi = data(:,2);
lam = wrapToPi(tht-phi+pi);
chi = real(acos(U./V));
dlh = data(:,5);
dld = data(:,6);

%% Conditions
% Predator Speed
sp_20 = logical([ones(1,215)  zeros(1,233) zeros(1,251)])';
sp_11 = logical([zeros(1,215) ones(1,233)  zeros(1,251)])';
sp_02 = logical([zeros(1,215) zeros(1,233) ones(1,251) ])';

% Predator Position
phi_lf = ( phi >= 0  & phi < pi   );
phi_rt = ( phi >= pi & phi < 2*pi );

% Predator Direction
lam_lf  = ( lam >= 0   & lam < pi );
lam_rt  = ( lam >= -pi & lam < 0  );


%% Plots
Ns_20 = 215;
Ns_11 = 233;
Ns_02 = 251;


%% % Distance vs. Phi % % % 
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(phi(phi_lf & sp_02),d(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_02),d(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(phi(lam_lf & sp_02),d(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_02),d(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(phi(phi_lf & sp_11),d(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_11),d(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(phi(lam_lf & sp_11),d(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_11),d(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(phi(phi_lf & sp_20),d(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_20),d(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(phi(lam_lf & sp_20),d(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_20),d(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.06])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');



print(f1,'scatter_cond_d_phi.eps','-depsc','-painters')




%% % Lambda vs. Phi % % % 
f2 = figure(2);
f2.Units = 'inches';
f2.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(phi(phi_lf & sp_02),lam(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_02),lam(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(phi(lam_lf & sp_02),lam(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_02),lam(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(phi(phi_lf & sp_11),lam(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_11),lam(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(phi(lam_lf & sp_11),lam(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_11),lam(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(phi(phi_lf & sp_20),lam(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_20),lam(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(phi(lam_lf & sp_20),lam(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_20),lam(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\lambda$','Interpreter','latex');



print(f2,'scatter_cond_lam_phi.eps','-depsc','-painters')



%% % U vs. Phi % % % 
f3 = figure(3);
f3.Units = 'inches';
f3.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(phi(phi_lf & sp_02),U(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_02),U(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(phi(lam_lf & sp_02),U(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_02),U(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(phi(phi_lf & sp_11),U(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_11),U(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(phi(lam_lf & sp_11),U(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_11),U(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(phi(phi_lf & sp_20),U(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_20),U(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(phi(lam_lf & sp_20),U(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_20),U(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi 0 0.15])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$u$','Interpreter','latex');



print(f3,'scatter_cond_u_phi.eps','-depsc','-painters')



%% % Delta d vs. Phi % % % 
f4 = figure(4);
f4.Units = 'inches';
f4.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(phi(phi_lf & sp_02),dld(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_02),dld(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(phi(lam_lf & sp_02),dld(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_02),dld(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(phi(phi_lf & sp_11),dld(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_11),dld(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(phi(lam_lf & sp_11),dld(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_11),dld(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(phi(phi_lf & sp_20),dld(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_20),dld(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(phi(lam_lf & sp_20),dld(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_20),dld(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');



print(f4,'scatter_cond_dld_phi.eps','-depsc','-painters')



%% % Delta d vs. Theta % % % 
f5 = figure(5);
f5.Units = 'inches';
f5.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(tht(phi_lf & sp_02),dld(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_02),dld(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(tht(lam_lf & sp_02),dld(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_02),dld(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(tht(phi_lf & sp_11),dld(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_11),dld(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(tht(lam_lf & sp_11),dld(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_11),dld(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(tht(phi_lf & sp_20),dld(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_20),dld(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(tht(lam_lf & sp_20),dld(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_20),dld(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_d$','Interpreter','latex');



print(f5,'scatter_cond_dld_tht.eps','-depsc','-painters')



%% % Delta h vs. Phi % % % 
f6 = figure(6);
f6.Units = 'inches';
f6.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(phi(phi_lf & sp_02),dlh(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_02),dlh(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(phi(lam_lf & sp_02),dlh(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_02),dlh(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(phi(phi_lf & sp_11),dlh(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_11),dlh(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(phi(lam_lf & sp_11),dlh(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_11),dlh(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(phi(phi_lf & sp_20),dlh(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(phi(phi_rt & sp_20),dlh(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(phi(lam_lf & sp_20),dlh(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(phi(lam_rt & sp_20),dlh(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([0 2*pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');



print(f6,'scatter_cond_dlh_phi.eps','-depsc','-painters')



%% % Delta d vs. Theta % % % 
f7 = figure(7);
f7.Units = 'inches';
f7.Position = [ 1 1 6 3 ];

% V = 2, Phi
subplot(2,3,1)
hold on
box on
plot(tht(phi_lf & sp_02),dlh(phi_lf & sp_02),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_02),dlh(phi_rt & sp_02),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
plot(tht(lam_lf & sp_02),dlh(lam_lf & sp_02),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_02),dlh(lam_rt & sp_02),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');


% V = 11, Phi
subplot(2,3,2)
hold on
box on
plot(tht(phi_lf & sp_11),dlh(phi_lf & sp_11),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_11),dlh(phi_rt & sp_11),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
plot(tht(lam_lf & sp_11),dlh(lam_lf & sp_11),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_11),dlh(lam_rt & sp_11),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');

% V = 20, Phi
subplot(2,3,3)
hold on
box on
plot(tht(phi_lf & sp_20),dlh(phi_lf & sp_20),'.','MarkerSize',5,'Color',colors.left);
plot(tht(phi_rt & sp_20),dlh(phi_rt & sp_20),'.','MarkerSize',5,'Color',colors.right);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

% V = 20, Lam
subplot(2,3,6)
hold on
box on
plot(tht(lam_lf & sp_20),dlh(lam_lf & sp_20),'.','MarkerSize',5,'Color',colors.sinistral);
plot(tht(lam_rt & sp_20),dlh(lam_rt & sp_20),'.','MarkerSize',5,'Color',colors.dextral);
axis([-pi pi -pi pi])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.YTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
ax.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\theta$','Interpreter','latex');
ylabel('$\delta_h$','Interpreter','latex');



print(f7,'scatter_cond_dlh_tht.eps','-depsc','-painters')
















