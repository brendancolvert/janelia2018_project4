clear all
close all
clc

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];
pred_spd = [];
frame1 = [];
frame2 = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
    pred_spd = vertcat(repmat(L(vnum).spd,N_prey,1),pred_spd);
    
    frame1 = vertcat(L(vnum).frames1,frame1);
    frame2 = vertcat(L(vnum).frames2,frame2);
    
end

% Figure out time for speed
fps = 250;
dur_resp = (frame2 - frame1) ./ fps;

Nprey = size(com_before,1);

for n = 1:Nprey
    dist3d(1,n) = norm(com_before(n,:));
end

% Heading of each prey
th = atan2(head_before(:,2)-com_before(:,2),head_before(:,1)-com_before(:,1));


dth_left = zeros(0,1);
dth_rght = zeros(0,1);
dsp_left = zeros(0,1);
dsp_rght = zeros(0,1);
phi_dthr = [];
phi_dthl = [];
phi_dspr = [];
phi_dspl = [];

% Center around initial COM
for nprey = 1:Nprey
    
    R = [  cos(th(nprey))  sin(th(nprey));...
          -sin(th(nprey))  cos(th(nprey))];
      
    p0(1:2,nprey)    = [cos(th(nprey));sin(th(nprey))];
    ppred(1:2,nprey) = R*[1;0];
    
    com0(1:2,nprey)  = [0;0];
    pred0(1:2,nprey) = R*(-com_before(nprey,1:2))';
    head0(1:2,nprey) = R*(head_before(nprey,1:2)-com_before(nprey,1:2))';
    tail0(1:2,nprey) = R*(tail_before(nprey,1:2)-com_before(nprey,1:2))';
    com1(1:2,nprey)  = R*(com_after(nprey,1:2)-com_before(nprey,1:2))';
    head1(1:2,nprey) = R*(head_after(nprey,1:2)-com_before(nprey,1:2))';
    p1(1:2,nprey)    = nrmz(head1(1:2,nprey)-com1(1:2,nprey));
    
    data_predatorDistance(1,nprey)          = norm(pred0(1:2,nprey));
    data_predatorAngularPosition(1,nprey)   = wrapToPi(atan2(pred0(2,nprey),pred0(1,nprey)));
    data_predatorHeading(1,nprey)           = wrapToPi(atan2(ppred(2,nprey),ppred(1,nprey)));
    data_predatorSpeed(1,nprey)             = pred_spd(nprey);
    data_preyDisplacementMagnitude(1,nprey) = sqrt(com1(2,nprey)^2+com1(1,nprey)^2);
    data_preyDisplacementDirection(1,nprey) = wrapToPi(atan2(com1(2,nprey),com1(1,nprey)));
    data_preyHeadingChange(1,nprey)         = wrapToPi(atan2(p1(2,nprey),p1(1,nprey)));   
    data_preySpeed(1,nprey)                 = data_preyDisplacementMagnitude(1,nprey)/dur_resp(nprey);
    data_preyBodylength(1,nprey)            = norm(head_before(nprey,1:2)-tail_before(nprey,1:2));
    
end


%% Clean data to get rid of nans

nanInds =   isnan(data_predatorDistance)            | ...
            isnan(data_predatorAngularPosition)     | ...
            isnan(data_predatorHeading)             | ...
            isnan(data_predatorSpeed)               | ...
            isnan(data_preyDisplacementMagnitude)   | ...
            isnan(data_preyDisplacementDirection)   | ...
            isnan(data_preyHeadingChange)           | ...
            isnan(data_preySpeed)                   | ...
            isnan(data_preyBodylength)              ;
 
        
data.D      = data_predatorDistance(~nanInds);
data.Phi    = wrapTo2Pi(data_predatorAngularPosition(~nanInds));
data.Lambda = wrapToPi(data_predatorHeading(~nanInds)-data_predatorAngularPosition(~nanInds)+pi);
data.V      = data_predatorSpeed(~nanInds);
data.R      = data_preyDisplacementMagnitude(~nanInds);
data.Gamma  = wrapToPi(data_preyDisplacementDirection(~nanInds));
data.Delta  = data_preyHeadingChange(~nanInds);
data.U      = data_preySpeed(~nanInds);
data.L      = data_preyBodylength(~nanInds);

%save(['evasionData.mat'],'data');
