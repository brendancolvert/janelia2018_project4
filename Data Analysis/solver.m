function [Xout,Yout] = solver(com1,com2,K,options)

%com1 = abs(com1);
%com2 = abs(com2);

% Sign of x-axis
x_sign = com1(2)./abs(com1(2));

% Displacement
disp = sqrt((com1(1)-com2(1)).^2 + (com1(2)-com2(2)).^2);

% Define input parameters
%y0 = com1(1);

% Initial position, gradient
p.y0 = com1(1);
p.x0 = abs(com1(2));
p.dydx0 = p.y0/p.x0;

% Set K
p.K  = K;
 
% Set span of trajectory
x_span = [p.x0 p.x0+disp*2];

% Numerically solve the equation
[X,Y] = ode45(@gov_eqn,x_span,[p.y0; p.dydx0],options);

% Position data
Y = Y(:,1);
X = X .* x_sign;

% Find position closest to measured displacement
s_rel = abs(cumsum(sqrt(diff(X).^2 + diff(Y).^2))-disp);
iS = find(s_rel==min(s_rel));

% Reverse axes
Yout = X(1:iS);
Xout = Y(1:iS);

    function dY = gov_eqn(x,Y)
        % ODE of the dynamics of the system
        
        % Body position along y-axis
        y  = Y(1);
        
        % Spatial gradient in y wrt x
        dydx  = Y(2);
    
        % Define output: 
        dY(1,1) = dydx;
        
        % Define output: 
        dY(2,1) = -(p.K .* sqrt(1 + dydx.^2))./x;       
    end
end






% function [X,Y] = solver(com,com2,K)
% % Numerical ODE solver for model that predicts the trajectory for a prey
% % that attempts to maximize its instantaneous direction away from a
% % predator
% 
% % Solver options
% options    = odeset('RelTol',1e-9);
% 
% % Define input parameters
% y0 = com(1);
% 
% p.y0 = 1;
% p.x0 = com(2)./y0;
% 
% p.K  = K;
% 
% p.dydx0 = p.y0/p.x0;
% 
% x_span = [com(2)./y0 2*com2(2)./y0];
% 
% % Numerically solve the equation
% [X,Y] = ode45(@gov_eqn,x_span,[p.y0; p.dydx0],options);
% 
% % Extract just position
% Y = Y(:,1).*y0;
% X = X.*y0;
%     
%     function dY = gov_eqn(x,Y)
%         % ODE of the dynamics of the system
%         
%         % Body position along y-axis
%         y  = Y(1);
%         
%         % Spatial gradient in y wrt x
%         dydx  = Y(2);
%     
%         % Define output: 
%         dY(1,1) = dydx;
%         
%         % Define output: 
%         dY(2,1) = -(p.K .* sqrt(1 + dydx.^2))./x;       
%     end
% end