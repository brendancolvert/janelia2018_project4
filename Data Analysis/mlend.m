function pc = mlend(data,pdf,start)

pc = fmincon(@(params)NLL(params,data,pdf),start);

end

function out = NLL(params,data,pdf)

[Ns,~] = size(data);

out = 0;

for n = 1:Ns
    out = out - log(pdf(data(n,:),params));
end

end