clear all
close all
clc

dataset = [];

for vnum = 1:3
    load(['responseData_' num2str(vnum) '.mat']);
    dataset = [dataset;data];
end

dataset = [dataset(1:436,:);...
           dataset(438:end,:)];

%data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle'];
d   = dataset(:,1);
phi = wrapTo2Pi(dataset(:,2));
V   = dataset(:,3);
thp = dataset(:,4);
dth = dataset(:,5);
del = dataset(:,6);

Nprey = length(d);


% % % COMPUTE MARGINALS % % % 

% Phi Marginal
A = [-1  0;...
      1  0;...
      0 -1];
  
eps = 10^(-10);

b = [0;...
     2*pi;...
     -eps];
 
x0 = [pi;...
      0.1*pi];
  
x = fmincon(@(x)negLLVM(x,phi),x0,A,b);

mu_phi = x(1);
kp_phi = x(2);

% Delta theta Marginal
A = [-1  0;...
      1  0;...
      0 -1];
  
eps = 10^(-10);

b = [0;...
     2*pi;...
     -eps];
 
x0 = [pi;...
      0.1*pi];
  
x = fmincon(@(x)negLLVM(x,dth),x0,A,b);

mu_dth = x(1);
kp_dth = x(2);

Nbin = 41;


% % Dtheta vs. Phi % %
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 5 5 ];
f1.PaperPositionMode = 'auto';


% Marginal Dtheta
subplot(6,6,2:6:20)
box on
hold on
histogram(dth,Nbin,'Normalization','pdf',...
    'FaceColor',[ 1 1 1 ]*0.8,...
    'EdgeColor',[ 0 0 0 ])

xlabel('$\Delta \theta$','Interpreter','latex');
xlim([-1 1]*pi);

% dthdist = linspace(-1,1,101)*pi;
% pdfdth  = vonMisesPDF(dthdist,mu_dth,kp_dth);
% 
% plot(dthdist,pdfdth,'-',...
%      'Color',[0.8 0 0],...
%      'LineWidth',2);

view([90 -90])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.XTick = (-1:0.5:1)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.YTickLabels = [];

% Marginal Phi
subplot(6,6,27:30)
box on
hold on
histogram(phi,Nbin,'Normalization','pdf',...
    'FaceColor',[ 1 1 1 ]*0.8,...
    'EdgeColor',[ 0 0 0 ])

xlabel('$\phi$','Interpreter','latex');
xlim([0 2]*pi);

phidist = linspace(0,2,101)*pi;
pdfphi  = vonMisesPDF(phidist,mu_phi,kp_phi);

plot(phidist,pdfphi,'-',...
     'Color',[0.0 0 0],...
     'LineWidth',2);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 10;
ax.XTick = (0:0.5:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
ax.YTickLabels = [];

% Joint
subplot(6,6,[3:6 9:12 15:18 21:24])
hold on
box on
plot(phi,dth,'k.')

dthmean = phidist-pi;
plot(phidist,dthmean,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);

xlim([0 2]*pi)
ylim([-1 1]*pi)

ax = gca;
ax.XTick = (0:0.5:2)*pi;
ax.YTick = (-1:0.5:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


%print(f1,'dth_phi.eps','-depsc','-painters')
print(f1,'dth_phi.jpg','-djpeg','-r300')


% % Blank % %
f2 = figure(2);
f2.Units = 'inches';
f2.Position = [ 1 1 5 5 ];
f2.PaperPositionMode = 'auto';


% Marginal Dtheta
% subplot(6,6,2:6:20)
% box on
% hold on
% %histogram(dth,Nbin,'Normalization','pdf',...
% %    'FaceColor',[ 1 1 1 ]*0.8,...
% %    'EdgeColor',[ 0 0 0 ])
% 
% xlabel('$\Delta \theta$','Interpreter','latex');
% xlim([-1 1]*pi);
% 
% view([90 -90])
% ax = gca;
% ax.FontName = 'Times New Roman';
% ax.FontSize = 10;
% ax.XTick = (-1:0.5:1)*pi;
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
% ax.YTickLabels = [];

% Marginal Phi
% subplot(6,6,27:30)
% box on
% hold on
% %histogram(phi,Nbin,'Normalization','pdf',...
% %    'FaceColor',[ 1 1 1 ]*0.8,...
% %    'EdgeColor',[ 0 0 0 ])
% 
% xlabel('$\phi$','Interpreter','latex');
% xlim([0 2]*pi);
% 
% phidist = linspace(0,2,101)*pi;
% pdfphi  = vonMisesPDF(phidist,mu_phi,kp_phi);
% 
% %plot(phidist,pdfphi,'-',...
% %     'Color',[0 0 0],...
% %     'LineWidth',2);
% 
% % ax = gca;
% % ax.FontName = 'Times New Roman';
% % ax.FontSize = 10;
% % ax.XTick = (0:0.5:2)*pi;
% % ax.TickLabelInterpreter = 'latex';
% % ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
% ax.YTickLabels = [];

% Joint
subplot(6,6,[3:6 9:12 15:18 21:24])
hold on
box on
%plot(phi,dth,'k.')

dthmean = phidist-pi;
plot(phidist,dthmean,'-',...
    'Color',[0.8 0 0],...
    'LineWidth',1);
xlim([0 2]*pi)
ylim([-1 1]*pi)

ax = gca;
ax.XTick = (0:0.5:2)*pi;
ax.YTick = (-1:0.5:1)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$','$1.5\pi$','$2\pi$'};
ax.YTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\Delta \theta$','Interpreter','latex');


%print(f2,'dth_phi_blank.eps','-depsc','-painters')
print(f2,'dth_phi_blank.jpg','-djpeg','-r300')




function pdf = vonMisesPDF(var,mu,kp)
pdf = 1./(2*pi*besseli(0,kp)).*exp(kp.*cos(var-mu));
end

function pdf = gaussianPDF(var,mu,sg)
eps = 10^(-10);
pdf = max([eps 1./sqrt(2*pi*sg.^2).*exp(-(var-mu).^2./(2*sg.^2))]);
end

function nll = negLLVM(x,phi_obs)

mu = x(1);
kp = x(2);

Nobs = length(phi_obs);
nll = 0;
for nobs = 1:Nobs
    nll = nll - log(vonMisesPDF(phi_obs(nobs),mu,kp));
end

end

function nll = negLLGS(x,phi_obs)

mu = x(1);
sg = x(2);

Nobs = length(phi_obs);
nll = 0;
for nobs = 1:Nobs
    nll = nll - log(gaussianPDF(phi_obs(nobs),mu,sg));
end

end