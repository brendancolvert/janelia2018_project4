clear all
close all
clc

load rbcmap;

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
end


Nprey = length(com_before);

for nprey = 1:Nprey
    dx               = (com_after(nprey,1:2)-com_before(nprey,1:2))';
    prey0            = (head_before(nprey,1:2)-com_before(nprey,1:2))';
    prey             = (head_after(nprey,1:2)-com_after(nprey,1:2))';
    p0(1:2,nprey)    = prey0/norm(prey0);
    p(1:2,nprey)     = prey/norm(prey);
    th0              = atan2(p0(2,nprey),p0(1,nprey));
    th               = atan2(p(2,nprey),p(1,nprey));
    
    R0 = [  cos(th0)  sin(th0);...
          -sin(th0)  cos(th0) ];
    
    pred             = R0*(-com_before(nprey,1:2))';
    d(1,nprey)       = norm(pred);
    phi(1,nprey)     = wrapTo2Pi(atan2(pred(2),pred(1)));
    thp(1,nprey)     = -atan2(p0(2,nprey),p0(1,nprey));
    dth(1,nprey)     = wrapToPi(th-th0);
    delta(1,nprey)   = wrapToPi(atan2(dx(2),dx(1))-th0);
end

Nbin = 31;

f11 = figure(11);
f11.Units = 'inches';
f11.Position = [ 2 2 9 9 ];

sz = 3;
Nv = 5;

% Phi
subplot(1+sz*Nv,1+sz*Nv,1:(1+sz*Nv):(1+3*sz*Nv));
histogram(phi,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 2]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = [];

xlabel('$\phi$','Interpreter','latex');
view([90 -90])

% % Theta p
subplot(1+sz*Nv,1+sz*Nv,(1:(1+sz*Nv):(1+3*sz*Nv))+sz*(1+sz*Nv));
histogram(wrapToPi(thp),Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\theta_p$','Interpreter','latex');
view([90 -90])


% % d
subplot(1+sz*Nv,1+sz*Nv,(1:(1+sz*Nv):(1+3*sz*Nv))+2*sz*(1+sz*Nv));
histogram(d,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 0.06]);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = 0:0.03:0.06;
%ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.YTick = [];

xlabel('$d$','Interpreter','latex');
view([90 -90])

% % Delta theta
subplot(1+sz*Nv,1+sz*Nv,(1:(1+sz*Nv):(1+3*sz*Nv))+3*sz*(1+sz*Nv));
histogram(dth,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\Delta \theta$','Interpreter','latex');
view([90 -90])

% % delta
subplot(1+sz*Nv,1+sz*Nv,(1:(1+sz*Nv):(1+3*sz*Nv))+4*sz*(1+sz*Nv));
histogram(delta,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\delta$','Interpreter','latex');
view([90 -90])


% === % === % === % === % HORIZONTAL % === % === % === % === % 

% Phi
subplot(1+sz*Nv,1+sz*Nv,242+0+(0:2));
histogram(phi,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 2]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = [];

xlabel('$\phi$','Interpreter','latex');

% % % Theta p
subplot(1+sz*Nv,1+sz*Nv,242+3+(0:2));
histogram(wrapToPi(thp),Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\theta_p$','Interpreter','latex');

% 
% 
% % % d
subplot(1+sz*Nv,1+sz*Nv,242+6+(0:2));
histogram(d,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([0 0.06]);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = 0:0.03:0.06;
%ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.YTick = [];

xlabel('$d$','Interpreter','latex');

% 
% % % Delta theta
subplot(1+sz*Nv,1+sz*Nv,242+9+(0:2));
histogram(dth,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\Delta \theta$','Interpreter','latex');

% 
% % % delta
subplot(1+sz*Nv,1+sz*Nv,242+12+(0:2));
histogram(delta,Nbin,...
    'Normalization','pdf',...
    'FaceColor',[1 1 1]*0.7,...
    'EdgeColor',[1 1 1]*0.0);

xlim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = {'$-\pi$','$0$','$\pi$'};
ax.YTick = [];

xlabel('$\delta$','Interpreter','latex');

% thp vs Phi
subplot(1+sz*Nv,1+sz*Nv,48 + [2:4,18:20,34:36]);
plot(phi,wrapToPi(thp),'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (0:1:2)*pi;
ax.YTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];

xlim([0 2]*pi);
ylim([-1 1]*pi);

% d vs Phi
subplot(1+sz*Nv,1+sz*Nv,96 + [2:4,18:20,34:36]);
plot(phi,d,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-0:1:2)*pi;
ax.YTick = 0:0.03:0.06;
ax.XTickLabels = [];
ax.YTickLabels = [];

xlim([0 2]*pi);
ylim([0 0.06]);

% d vs thp
subplot(1+sz*Nv,1+sz*Nv,99 + [2:4,18:20,34:36]);
plot(wrapToPi(thp),d,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1:1:1)*pi;
ax.YTick = 0:0.03:0.06;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([-1 1]*pi);
ylim([0 0.06]);

% Delta theta vs Phi
subplot(1+sz*Nv,1+sz*Nv,144 + [2:4,18:20,34:36]);
plot(phi,dth,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-0:1:2)*pi;
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([0 2]*pi);
ylim([-1 1]*pi);

% Delta theta vs Theta p
subplot(1+sz*Nv,1+sz*Nv,147 + [2:4,18:20,34:36]);
plot(wrapToPi(thp),dth,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1:1:1)*pi;
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([-1 1]*pi);
ylim([-1 1]*pi);

% Delta theta vs d
subplot(1+sz*Nv,1+sz*Nv,150 + [2:4,18:20,34:36]);
plot(d,dth,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.03:0.06;
ax.YTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([0 0.06]);
ylim([-1 1]*pi);



% delta vs Phi
subplot(1+sz*Nv,1+sz*Nv,192 + [2:4,18:20,34:36]);
plot(phi,delta,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-0:1:2)*pi;
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([0 2]*pi);
ylim([-1 1]*pi);

% delta vs phi-theta p
subplot(1+sz*Nv,1+sz*Nv,195 + [2:4,18:20,34:36]);
plot(wrapToPi(thp),delta,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1:1:1)*pi;
ax.XTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([-1 1]*pi);
ylim([-1 1]*pi);

% delta vs d
subplot(1+sz*Nv,1+sz*Nv,198 + [2:4,18:20,34:36]);
plot(d,delta,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = 0:0.03:0.06;
ax.YTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([0 0.06]);
ylim([-1 1]*pi);

% delta vs Delta theta
subplot(1+sz*Nv,1+sz*Nv,201 + [2:4,18:20,34:36]);
plot(dth,delta,'k.',...
    'MarkerSize',3);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-1:1:1)*pi;
ax.YTick = (-1:1:1)*pi;
ax.XTickLabels = [];
ax.YTickLabels = [];


xlim([-1 1]*pi);
ylim([-1 1]*pi);










print(f11,'scatterplot_shift.eps','-depsc','-painters');



