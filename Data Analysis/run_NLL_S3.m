clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

U = 0.075; % m/s


d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
lam = wrapToPi(tht-phi+pi);
dlh = data(:,5);
dld = data(:,6);
chi = real(acos(1./K));

x = [phi';...
     lam';...
     dld'];
 
marg = true;
 
if marg

    eta0 = [0.014723*pi;...
            0.229331*pi;...
            0.016318*pi;...
            1.046803*pi;...
            6.657305*pi;...
            0.084386*pi;...
            3.728952*pi];
        
    A = [-1  0  0  0  0  0  0;...
          0 -1  0  0  0  0  0;...
          0  0 -1  0  0  0  0;...
          0  0  0 -1  0  0  0;...
          0  0  0  1  0  0  0;...
          0  0  0  0 -1  0  0;...
          0  0  0  0  0 -1  0;...
          0  0  0  0  0  1  0;...
          0  0  0  0  0  0 -1];

    b = [0;...
         0;...
         0;...
         0;...
         2*pi;...
         0;
         0;
         2*pi;...
         0];


else


    eta0 = [0.014723*pi;...
            0.229331*pi;...
            0.016318*pi];
    A = [-1  0  0;...
          0 -1  0;...
          0  0 -1];

    b = [0;...
         0;...
         0];

end

global indx;
indx = 1;


options = optimoptions('fmincon','Display','final-detailed');
[etaopt,nllopt] = fmincon(@(eta)NLL_S3(eta,x),eta0,A,b,[],[],[],[],[],options)

pause
    
%% AIC
Np = 2;
Ns = length(phi)
AICc = 2*Ns*nllopt +2*Np + (2*Np^2+2*Np)/(Np-Ns-1)

%pause


% %% Plot
% Nx = 101;
% phim = linspace(0,2,Nx)*pi;
% delm = linspace(-1,1,Nx)*pi;
% 
% [delm,phim] = meshgrid(delm,phim);
% 
% sig_S   = etaopt(1);
% sig_R   = etaopt(2);
% 
% 
% if length(etaopt) == 2
%     mu_phi  = pi;
%     sig_phi = inf;
% elseif length(etaopt) == 4
%     mu_phi  = etaopt(3);
%     sig_phi = etaopt(4);
% end
% 
% p = pX_S3([phim(:)';...
%            delm(:)'],...
%           [sig_S;...
%            sig_R;...
%            mu_phi;...
%            sig_phi]);
% p = reshape(p,Nx,Nx);
% 
% 
% f1 = figure(1);
% f1.Units = 'inches';
% f1.Position = [ 1 1 2 2 ];
%        
% numc = 40;
% contourf(phim,delm,p,numc,'LineStyle','none')
% colormap(flipud(gray))
% 
% ax1 = gca;
% ax1.FontName = 'Times New Roman';
% ax1.FontSize = 8;
% ax1.XTick = (-2:1:2)*pi;
% ax1.YTick = (-2:1:2)*pi;
% ax1.TickLabelInterpreter = 'latex';
% ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
% ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
% 
% xlabel('$\phi$','Interpreter','latex');
% ylabel('$\delta$','Interpreter','latex');
% 
% print(f1,'testDist_S3.eps','-depsc');
% 
% f2 = figure(2);
% f2.Units = 'inches';
% f2.Position = [ 3 1 2 2 ];
%        
% plot(phi,dlh,'k.','MarkerSize',3)
% 
% ax1 = gca;
% ax1.FontName = 'Times New Roman';
% ax1.FontSize = 8;
% ax1.XTick = (-2:1:2)*pi;
% ax1.YTick = (-2:1:2)*pi;
% ax1.TickLabelInterpreter = 'latex';
% ax1.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
% ax1.YTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
% 
% xlabel('$\phi$','Interpreter','latex');
% ylabel('$\delta$','Interpreter','latex');
% axis(pi*[0 2 -1 1])
% 
% print(f2,'testDist_S3_data.eps','-depsc');