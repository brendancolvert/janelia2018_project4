clear all
close all
clc


% Color Definitions
% Left vs. Right Phi
colors.left  = [242 218 221]/255;
colors.right = [208 208 229]/255;

% Left vs. Right Lambda
colors.sinistrous = [213 198 226]/255;
colors.dexterous  = [207 229 216]/255;

N = 100000;
X = randn(1,N)+1.0;
Y = randn(1,N)-1.0;

Nbin = 40;
edges = linspace(-3,3,Nbin);
bins  = 0.5*(edges(1:(end-1))+edges(2:end));
binwd = edges(2)-edges(1);

Cl = histcounts(X(X<0),edges);
Cr = histcounts(X(X>=0),edges);
b = bar(bins',[Cl' Cr']/(N*binwd),'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;