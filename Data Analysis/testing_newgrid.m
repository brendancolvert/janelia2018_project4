clear all
close all
clc

x = -5:1:5;

x = [ x+0.5;...
      x;...
      x+0.5;...
      x;...
      x+0.5;...
      x;...
      x+0.5;...
      x;...
      x+0.5;...
      x;...
      x+0.5];
y = repmat(unique([ -1*fliplr(0:sqrt(3)/2:5) 0:sqrt(3)/2:5 ])',1,11);

plot(x,y,'k.','MarkerSize',12)
hold on
plotCircle(x(:),y(:),0.5*ones(length(x(:)),1))
axis equal
axis(3*[-1 1 -1 1])



function [] = plotCircle(x,y,r)
alp = linspace(0,2*pi,401);
Nx = length(x);
for n = 1:Nx
    plot(x(n)+r(n)*cos(alp),y(n)+r(n)*sin(alp),'k-','LineWidth',1);
end

end


