clear all
close all
clc

load Ldata;

% Extract relevant
com_before = L.com;
com_after = L.com2;

head_before = L.head;
head_after = L.head2;

tail_before = L.tail;

Nprey = size(com_before,1);

figure(1)
hold on

for nprey = 1:Nprey
    plot(com_before(nprey,1),com_before(nprey,2),'k.','MarkerSize',6);
    plot(head_before(nprey,1),head_before(nprey,2),'k.','MarkerSize',9);
    plot([head_before(nprey,1) tail_before(nprey,1)],...
        [head_before(nprey,2) tail_before(nprey,2)],...
        'k-');
    
    plot(com_after(nprey,1),com_after(nprey,2),'r.','MarkerSize',6);
    plot(head_after(nprey,1),head_after(nprey,2),'r.','MarkerSize',9);
    plot([head_after(nprey,1) com_after(nprey,1)],...
        [head_after(nprey,2) com_after(nprey,2)],...
        'r-');
    
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
end

axis equal

figure(); hold on
plot3(com_before(:,1),com_before(:,2),com_before(:,3),'k.','MarkerSize',6);
plot3(head_before(:,1),head_before(:,2),head_before(:,3),'k.','MarkerSize',9);

for nprey = 1:Nprey
    plot3([head_before(nprey,1) tail_before(nprey,1)],...
        [head_before(nprey,2) tail_before(nprey,2)],...
        [head_before(nprey,3) tail_before(nprey,3)], ...
        'k-');
end
xlabel('X'); ylabel('Y'); zlabel('Z');

%% Calculate distance with unshifted coordinates
d = sqrt(com_before(:,1).^2 + com_before(:,2).^2 + com_before(:,3).^2)
figure()
histogram(d,20)
ylabel('N')
xlabel('d')

%% Collapse elevation

com_before = com_before(:,1:2);
com_after = com_after(:,1:2);

head_before = head_before(:,1:2);
head_after = head_after(:,1:2);

tail_before = tail_before(:,1:2);

%% Transform to prey frame of reference

% center location
pred = -com_before;
head_bef = head_before - com_before;
tail_bef = tail_before - com_before;
head_aft = head_after - com_before;
com_aft = com_after - com_before;

%% calculate angles
theta_b = atan2(head_bef(:,1),head_bef(:,2));
pred_head = [cos(theta_b) sin(theta_b)];
phi = atan2(-pred(:,2),-pred(:,1));

%% Plot distribution of \phi
figure()
polarhistogram(phi,20)
xlabel('\phi')

%% Find and plot change in displacement
dtheta = atan2(com_aft(:,2),com_aft(:,1));
figure()
polarhistogram(dtheta,20)
title('\Delta displacement')

%% Plot \theta as a function of \phi
figure()
subplot(1,2,1)
polarhistogram(dtheta(phi >= 0),20)
subplot(1,2,2)
polarhistogram(dtheta(phi < 0),20)

%% Find and plot change in heading
heading_aft = head_aft - com_aft;
dheading = atan2(heading_aft(:,2),heading_aft(:,1));
figure()
polarhistogram(dheading,20)
title('\Delta \theta')