clear all
close all
clc

Vnum = [1 2 3];


% Color Definitions
% Left vs. Right Phi
colors.left  = [190 030 045]/255;
colors.right = [046 049 146]/255;

% Left vs. Right Lambda
colors.Sinistral = [102 045 145]/255;
colors.Dextral  = [000 104 056]/255;

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%U = 0.075; % m/s

%% Load Data
V   = data(:,3)*0.01;
d   = data(:,1);
U   = data(:,9);
tht = data(:,4);
phi = data(:,2);
lam = wrapToPi(tht-phi+pi);
chi = real(acos(U./V));
dlh = data(:,5);
dld = data(:,6);

%% Conditions
% Predator Speed
sp_20 = logical([ones(1,215)  zeros(1,233) zeros(1,251)])';
sp_11 = logical([zeros(1,215) ones(1,233)  zeros(1,251)])';
sp_02 = logical([zeros(1,215) zeros(1,233) ones(1,251) ])';

% Predator Position
phi_lf = ( phi >= 0  & phi < pi   );
phi_rt = ( phi >= pi & phi < 2*pi );

% Predator Direction
lam_lf  = ( lam >= 0   & lam < pi );
lam_rt  = ( lam >= -pi & lam < 0  );


%% Plots
Nbin = 16;
Ns_20 = 215;
Ns_11 = 233;
Ns_02 = 251;


%% % Distance % % % 
f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 6 3 ];

edges = linspace(0,0.06,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(d(find(phi_lf & sp_02)),edges);
Nr = histcounts(d(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

[ph,pc] = mle(d(find(sp_02)),'distribution','gamma')
dt = linspace(0,0.06,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)


% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(d(find(lam_lf & sp_02)),edges);
Nr = histcounts(d(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(d(find(phi_lf & sp_11)),edges);
Nr = histcounts(d(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

[ph,pc] = mle(d(find(sp_11)),'distribution','gamma')
dt = linspace(0,0.06,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(d(find(lam_lf & sp_11)),edges);
Nr = histcounts(d(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(d(find(phi_lf & sp_20)),edges);
Nr = histcounts(d(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

[ph,pc] = mle(d(find(sp_20)),'distribution','gamma')
dt = linspace(0,0.06,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(d(find(lam_lf & sp_20)),edges);
Nr = histcounts(d(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.06 0 80])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$d$','Interpreter','latex');
ylabel('$p_D$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f1,'hist_cond_d.eps','-depsc','-painters')








%% % Phi % % % 
f2 = figure(2);
f2.Units = 'inches';
f2.Position = [ 1 1 6 3 ];

edges = linspace(0,2*pi,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(phi(find(phi_lf & sp_02)),edges);
Nr = histcounts(phi(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

[ph,pc] = mle(phi(find(sp_02)),'pdf',@vmpdf,'start',[pi,10/pi])
dt = linspace(0,2*pi,101);
pt = vmpdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(phi(find(lam_lf & sp_02)),edges);
Nr = histcounts(phi(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(phi(find(phi_lf & sp_11)),edges);
Nr = histcounts(phi(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

[ph,pc] = mle(phi(find(sp_11)),'pdf',@vmpdf,'start',[pi,10/pi])
dt = linspace(0,2*pi,101);
pt = vmpdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(phi(find(lam_lf & sp_11)),edges);
Nr = histcounts(phi(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(phi(find(phi_lf & sp_20)),edges);
Nr = histcounts(phi(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

[ph,pc] = mle(phi(find(sp_20)),'pdf',@vmpdf,'start',[pi,10/pi])
dt = linspace(0,2*pi,101);
pt = vmpdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(phi(find(lam_lf & sp_20)),edges);
Nr = histcounts(phi(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 2*pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\phi$','Interpreter','latex');
ylabel('$p_\Phi$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f2,'hist_cond_phi.eps','-depsc','-painters')








%% % Lambda % % % 
f3 = figure(3);
f3.Units = 'inches';
f3.Position = [ 1 1 6 3 ];

edges = linspace(-pi,pi,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(lam(find(phi_lf & sp_02)),edges);
Nr = histcounts(lam(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

[ph,pc] = mle(lam(find(sp_02)),'pdf',@pwupdf,'start',[0,0.5])
dt = linspace(-pi,pi,101);
pt = pwupdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(lam(find(lam_lf & sp_02)),edges);
Nr = histcounts(lam(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(lam(find(phi_lf & sp_11)),edges);
Nr = histcounts(lam(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

[ph,pc] = mle(lam(find(sp_11)),'pdf',@pwupdf,'start',[0,0.5])
dt = linspace(-pi,pi,101);
pt = pwupdf(dt,ph(1),ph(2));;
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(lam(find(lam_lf & sp_11)),edges);
Nr = histcounts(lam(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(lam(find(phi_lf & sp_20)),edges);
Nr = histcounts(lam(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

[ph,pc] = mle(lam(find(sp_20)),'pdf',@pwupdf,'start',[0,0.5])
dt = linspace(-pi,pi,101);
pt = pwupdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(lam(find(lam_lf & sp_20)),edges);
Nr = histcounts(lam(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\lambda$','Interpreter','latex');
ylabel('$p_\Lambda$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f3,'hist_cond_lam.eps','-depsc','-painters')








%% % U % % % 
f4 = figure(4);
f4.Units = 'inches';
f4.Position = [ 1 1 6 3 ];

edges = linspace(0,0.15,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(U(find(phi_lf & sp_02)),edges);
Nr = histcounts(U(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

[ph,pc] = mle(U(find(sp_02)),'distribution','gamma')
dt = linspace(0,0.15,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(U(find(lam_lf & sp_02)),edges);
Nr = histcounts(U(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(U(find(phi_lf & sp_11)),edges);
Nr = histcounts(U(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

[ph,pc] = mle(U(find(sp_11)),'distribution','gamma')
dt = linspace(0,0.15,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(U(find(lam_lf & sp_11)),edges);
Nr = histcounts(U(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(U(find(phi_lf & sp_20)),edges);
Nr = histcounts(U(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

[ph,pc] = mle(U(find(sp_20)),'distribution','gamma')
dt = linspace(0,0.15,101);
pt = gampdf(dt,ph(1),ph(2));
plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(U(find(lam_lf & sp_20)),edges);
Nr = histcounts(U(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([0 0.15 0 100])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
xlabel('$u$','Interpreter','latex');
ylabel('$p_U$','Interpreter','latex');

plot(dt,pt,'LineWidth',2,'Color',[1 1 1]*0.3)

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f4,'hist_cond_u.eps','-depsc','-painters')












%% % Delta d % % % 
f5 = figure(5);
f5.Units = 'inches';
f5.Position = [ 1 1 6 3 ];

edges = linspace(-pi,pi,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(dld(find(phi_lf & sp_02)),edges);
Nr = histcounts(dld(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')


% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(dld(find(lam_lf & sp_02)),edges);
Nr = histcounts(dld(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(dld(find(phi_lf & sp_11)),edges);
Nr = histcounts(dld(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(dld(find(lam_lf & sp_11)),edges);
Nr = histcounts(dld(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(dld(find(phi_lf & sp_20)),edges);
Nr = histcounts(dld(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(dld(find(lam_lf & sp_20)),edges);
Nr = histcounts(dld(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_d$','Interpreter','latex');
ylabel('$p_{\Delta_d}$','Interpreter','latex');

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f5,'hist_cond_dld.eps','-depsc','-painters')









%% % Delta h % % % 
f6 = figure(6);
f6.Units = 'inches';
f6.Position = [ 1 1 6 3 ];

edges = linspace(-pi,pi,Nbin+1);
bins  = 0.5*(edges(2:end) + edges(1:(end-1)));
binwd = edges(2)-edges(1);

% V = 2, Phi
subplot(2,3,1)
hold on
box on
Nl = histcounts(dlh(find(phi_lf & sp_02)),edges);
Nr = histcounts(dlh(find(phi_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');
title('$V = 2$','Interpreter','latex')

% V = 2, Lam
subplot(2,3,4)
hold on
box on
Nl = histcounts(dlh(find(lam_lf & sp_02)),edges);
Nr = histcounts(dlh(find(lam_rt & sp_02)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');

% V = 11, Phi
subplot(2,3,2)
hold on
box on
Nl = histcounts(dlh(find(phi_lf & sp_11)),edges);
Nr = histcounts(dlh(find(phi_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');
title('$V = 11$','Interpreter','latex')

% V = 11, Lam
subplot(2,3,5)
hold on
box on
Nl = histcounts(dlh(find(lam_lf & sp_11)),edges);
Nr = histcounts(dlh(find(lam_rt & sp_11)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');


% V = 20, Phi
subplot(2,3,3)
hold on
box on
Nl = histcounts(dlh(find(phi_lf & sp_20)),edges);
Nr = histcounts(dlh(find(phi_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.left;
b(2).FaceColor = colors.right;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');
title('$V = 20$','Interpreter','latex')

lg = legend('Left','Right');
lg.FontSize = 6;

% V = 20, Lam
subplot(2,3,6)
hold on
box on
Nl = histcounts(dlh(find(lam_lf & sp_20)),edges);
Nr = histcounts(dlh(find(lam_rt & sp_20)),edges);
b = bar(bins',[Nl' Nr']/(Ns_20*binwd),1.0,'Stacked');
b(1).FaceColor = colors.Sinistral;
b(2).FaceColor = colors.Dextral;
b(1).FaceAlpha = 0.5;
b(2).FaceAlpha = 0.5;
axis([-pi pi 0 0.6])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (-2:2)*pi;
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-2\pi$','$-\pi$','$0$','$\pi$','$2\pi$'};
xlabel('$\delta_h$','Interpreter','latex');
ylabel('$p_{\Delta_h}$','Interpreter','latex');

lg = legend('Sinistral','Dextral');
lg.FontSize = 6;

print(f6,'hist_cond_dlh.eps','-depsc','-painters')






























