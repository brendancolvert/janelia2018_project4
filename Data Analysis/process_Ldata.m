clear all
close all
clc

load Ldata;
spds = [2 11 20];
Vnum = [1 2 3];

com_before = [];
com_after = [];
head_before = [];
head_after = [];
tail_before = [];
pred_spd = [];
frame1 = [];
frame2 = [];

for vnum = Vnum
    N_prey = size(L(vnum).com, 1);
    
    com_before = vertcat(L(vnum).com,com_before);
    com_after = vertcat(L(vnum).com2,com_after);

    head_before = vertcat(L(vnum).head,head_before);
    head_after  = vertcat(L(vnum).head2,head_after);

    tail_before = vertcat(L(vnum).tail,tail_before);
    
    pred_spd = vertcat(repmat(L(vnum).spd,N_prey,1),pred_spd);
    
    frame1 = vertcat(L(vnum).frames1,frame1);
    frame2 = vertcat(L(vnum).frames2,frame2);
    
end

% Figure out time for speed
fps = 250;
dur_resp = (frame2 - frame1) ./ fps;

Nprey = size(com_before,1);

for n = 1:Nprey
    dist3d(1,n) = norm(com_before(n,:));
end

% Heading of each prey
th = atan2(head_before(:,2)-com_before(:,2),head_before(:,1)-com_before(:,1));


dth_left = zeros(0,1);
dth_rght = zeros(0,1);
dsp_left = zeros(0,1);
dsp_rght = zeros(0,1);
phi_dthr = [];
phi_dthl = [];
phi_dspr = [];
phi_dspl = [];

% Center around initial COM
for nprey = 1:Nprey
    
    R = [  cos(th(nprey))  sin(th(nprey));...
          -sin(th(nprey))  cos(th(nprey))];
      
    p0(1:2,nprey)    = [cos(th(nprey));sin(th(nprey))];
    ppred(1:2,nprey) = R*[1;0];
    
    com0(1:2,nprey)  = [0;0];
    pred0(1:2,nprey) = R*(-com_before(nprey,1:2))';
    head0(1:2,nprey) = R*(head_before(nprey,1:2)-com_before(nprey,1:2))';
    tail0(1:2,nprey) = R*(tail_before(nprey,1:2)-com_before(nprey,1:2))';
    com1(1:2,nprey)  = R*(com_after(nprey,1:2)-com_before(nprey,1:2))';
    head1(1:2,nprey) = R*(head_after(nprey,1:2)-com_before(nprey,1:2))';
    p1(1:2,nprey)    = nrmz(head1(1:2,nprey)-com1(1:2,nprey));
    
    bodylength(1,nprey) = norm(head_before(nprey,1:2)-tail_before(nprey,1:2));
    d(1,nprey)          = norm(pred0(1:2,nprey));
    phi(1,nprey)        = wrapToPi(atan2(pred0(2,nprey),pred0(1,nprey)));
    thp(1,nprey)        = wrapToPi(atan2(ppred(2,nprey),ppred(1,nprey)));
    dispangle(1,nprey)  = wrapToPi(atan2(com1(2,nprey),com1(1,nprey)));
    dtheta(1,nprey)     = wrapToPi(atan2(p1(2,nprey),p1(1,nprey)));   
    
    % Calculate displacement
    displ(1,nprey) = norm(com1(1:2,nprey));
    
    if (phi(nprey) < 0 )
        phi_dthr = [phi_dthr phi(nprey)];
        dth_rght = [dth_rght, dtheta(1,nprey)];
    else
        phi_dthl = [phi_dthl phi(nprey)];
        dth_left = [dth_left, dtheta(1,nprey)];
    end
    
    if (phi(nprey) < 0 )
        phi_dspr = [phi_dspr phi(nprey)];
        dsp_rght = [dsp_rght, dispangle(1,nprey)];
    else
        phi_dspl = [phi_dspl phi(nprey)];
        dsp_left = [dsp_left, dispangle(1,nprey)];
    end
    
    
    
    scale = 0.001;
    plotsize = 0.05;
    
%     figure(1);
%     hold on
%     axis equal
%     axis(plotsize*[-1 1 -1 1])
%     plot([head0(1,nprey) com0(1,nprey)],...
%          [head0(2,nprey) com0(2,nprey)],...
%          'k-',...
%          'LineWidth',1);
%     plot(head0(1,nprey),...
%          head0(2,nprey),...
%          'k.',...
%          'MarkerSize',12);
%     quiver(d(nprey)*cos(phi(nprey)),d(nprey)*sin(phi(nprey)),...
%            scale*cos(thp(nprey)),-scale*sin(thp(nprey)),'r-');
%     
%     
%     figure(2);
%     hold on
%     axis equal
%     axis(plotsize*[-1 1 -1 1])
%     plot([head1(1,nprey) com1(1,nprey)],...
%          [head1(2,nprey) com1(2,nprey)],...
%      'k-',...
%      'LineWidth',1);
%     plot(head1(1,nprey),...
%          head1(2,nprey),...
%      'k.',...
%      'MarkerSize',12);
%     quiver(d(nprey)*cos(phi(nprey)),d(nprey)*sin(phi(nprey)),...
%            scale*cos(thp(nprey)),scale*sin(thp(nprey)),'r-');
    
end

% figure(1)
% subplot(1,2,1)
% polarhistogram(dth_left,20);
% subplot(1,2,2)
% polarhistogram(dth_rght,20);
% 
% figure(2)
% subplot(1,2,1)
% polarhistogram(dsp_left,20);
% subplot(1,2,2)
% polarhistogram(dsp_rght,20);
% 
% figure(3)
% subplot(2,1,1)
% histogram(dist3d,20)
% subplot(2,1,2)
% histogram(d,20)


%<<<<<<< HEAD
% data = [ d' phi' repmat(spds(Vnum),Nprey,1) thp' dtheta' dispangle'];
% save(['responseData_' num2str(Vnum) '.mat'],'data');

% figure(4)
% subplot(1,2,1)
% plot(phi_dthl, dth_left, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dthr, dth_rght, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('right')  
% 
% figure(5)
% subplot(1,2,1)
% plot(phi_dspl, dsp_left, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dspr, dsp_rght, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('right')
%=======
% figure(4)
% subplot(1,2,1)
% plot(phi_dthl, dth_left, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dthr, dth_rght, 'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% title('right')  
% 
% figure(5)
% subplot(1,2,1)
% plot(phi_dspl, dsp_left, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('left')
% subplot(1,2,2)
% plot(phi_dspr, dsp_rght, 'k.')
% ylabel('Displacement')
% xlabel('\phi')
% title('right')
% >>>>>>> 128d88b7629d94dbffe938de78fc23c251a3c871

% phi_wrap = wrapTo2Pi(phi)/pi;
% dtheta_wrap = wrapTo2Pi(dtheta+pi)/pi-1;
% indices = ~isnan(dtheta_wrap);
% [R, p] = corr(phi_wrap(indices)', dtheta_wrap(indices)')
% <<<<<<< HEAD
% 
% 
% =======
% % corrcoef([wrapTo2Pi(phi)/pi', wrapTo2Pi(dtheta+pi)/pi-1'])
% % corr(phi',dtheta')
% >>>>>>> 128d88b7629d94dbffe938de78fc23c251a3c871
% figure(6)
% plot(wrapTo2Pi(phi)/pi,wrapTo2Pi(dtheta+pi)/pi-1,'k.')
% ylabel('\Delta \theta')
% xlabel('\phi')
% 
% <<<<<<< HEAD
% =======
% figure(7)
% plot(wrapToPi(thp)/pi,wrapToPi(dtheta)/pi,'k.')
% ylabel('\Delta \theta')
% xlabel('\theta_p')

%% Calculate prey velocity
% prey_anglespd = dispangle ./ dur_resp;
% figure(101); hold on
% histogram(prey_anglespd, 20)

prey_displspd = displ ./ dur_resp';
figure(102); hold on
histogram(prey_displspd, 20)

pause
%% Clean data to get rid of nans


dtheta_nnan = ~isnan(dtheta);
dispangle_nnan = ~isnan(dispangle);
resp_dur_nnan = ~(dur_resp == 0);
nnan = dtheta_nnan .* dispangle_nnan .* resp_dur_nnan';
d_clean = d(logical(nnan));
phi_clean = phi(logical(nnan));
pred_spd_clean = pred_spd(logical(nnan));
thp_clean = thp(logical(nnan));
dtheta_clean = dtheta(logical(nnan));
dispangle_clean = dispangle(logical(nnan));
dur_clean = dur_resp(logical(nnan));
prey_anglespd_clean = prey_anglespd(logical(nnan));
prey_displspd_clean = prey_displspd(logical(nnan));

%% Perform full correlation analysis
% Construct table of variables
if length(Vnum) == 1
    data = [ d_clean' ...
         wrapTo2Pi(phi_clean)' ...
         thp_clean' ...
         dtheta_clean' ...
         dispangle_clean' ...
         dur_clean ...
         prey_anglespd_clean'...
         prey_displspd_clean'];
     
    data_tbl = array2table(data, ...
        'VariableNames', {'d', 'phi', 'thp', 'dtheta', 'dispm', 'dur', 'aspd', 'dspd'});     
else
    data = [ d_clean' ...
             wrapTo2Pi(phi_clean)' ...
             pred_spd_clean ...
             thp_clean' ...
             dtheta_clean' ...
             dispangle_clean'...
             dur_clean...
             prey_anglespd_clean'...
             prey_displspd_clean'
            ];
     

    data_tbl = array2table(data, ...
        'VariableNames', {'d', 'phi', 'V', 'thp', 'dtheta', 'dispm', 'dur', 'aspd', 'dspd'});
end

save(['responseData_' num2str(Vnum) '.mat'],'data');

% [Rvalue,Pvalue] = corrplot(data_tbl);
% [R,P,RL,RU] = corrcoef(data);
% 
% corrfig = gcf;
% corraxes = findobj(corrfig, 'Type', 'Axes');
% 
% %% Look into dispm vs dthet
% figure(9); hold on
% x1 = dispangle_clean(dtheta_clean >= 0);
% y1 = dtheta_clean(dtheta_clean >= 0);
% x2 = dispangle_clean(dtheta_clean < 0);
% y2 = dtheta_clean(dtheta_clean < 0);
% plot(x1,y1, 'k.')
% plot(x2,y2, 'r.')
% xlabel('Displacement')
% ylabel('\Delta \theta');
% 
% % Linear regression
% X1 = [ones(length(x1),1) x1'];
% b1 = X1\y1';
% X2 = [ones(length(x2),1) x2'];
% b2 = X2\y2';
% 
% % Plot linear regression line
% Y1 = X1*b1;
% plot(x1, Y1, 'k--','DisplayName','Left turn')
% str1 = sprintf('dth = %.3f disp + %.3f', b1(2), b1(1));
% text(x1(1)-4, Y1(1), str1, 'FontSize', 12, 'Interpreter', 'latex');
% 
% Y2 = X2*b2;
% plot(x2, Y2, 'r--','DisplayName','Right turn')
% str2 = sprintf('dth = %.3f disp + %.3f', b2(2), b2(1));
% text(x2(1)+2, Y2(1), str2, 'FontSize', 12, 'Interpreter', 'latex');
% 
% legend({'Left turn', 'Right turn'},'Location','bestoutside')
% 
% %% Investigate differences between speeds
% vars = {'d', 'phi', 'V', 'thp', 'dtheta', 'dispm'};
% data_Tbl{1} = data_tbl(data_tbl.V == 2, vars);
% data_Tbl{2} = data_tbl(data_tbl.V == 11, vars);
% data_Tbl{3} = data_tbl(data_tbl.V == 20, vars);
% figure()
% subplot(1,2,1); hold on
% for vnum = Vnum
% %     his = hist(table2array(data_Tbl{vnum}(:,{'dtheta'})),20);
% %     plot(his)
%     histogram(table2array(data_Tbl{vnum}(:,{'dtheta'})),20);
% end
% legend({'2', '11', '20'})
% xlabel('\Delta \theta')
% ylabel('N')
% 
% subplot(1,2,2); hold on
% for vnum = Vnum
% %     his = hist(table2array(data_Tbl{vnum}(:,{'dispm'})),20);
% %     plot(his)
%     histogram(table2array(data_Tbl{vnum}(:,{'dispm'})),20);
% end
% 
% legend({'2', '11', '20'})
% xlabel('Displacement')
% ylabel('N')
% >>>>>>> 128d88b7629d94dbffe938de78fc23c251a3c871
% % f1 = figure(1);
% % f1.Units = 'inches';
% % f1.Position = [1 1 6 6];
% % hold on
% % axis equal
% % box on
% % 
% % for nprey = 1:Nprey
% %     plot(com_before(nprey,1),com_before(nprey,2),'k.','MarkerSize',4);
% %     plot(head_before(nprey,1),head_before(nprey,2),'k.','MarkerSize',8);
% %     plot([head_before(nprey,1) com_before(nprey,1)],[head_before(nprey,2) com_before(nprey,2)],...
% %         'k-');
% %     
% %     plot([com_before(nprey,1) com_after(nprey,1)],...
% %          [com_before(nprey,2) com_after(nprey,2)],...
% %          '-',...
% %          'Color',[ 1 1 1 ]*0.5);
% %     
% %     plot(com_after(nprey,1),com_after(nprey,2),'r.','MarkerSize',4);
% %     plot(head_after(nprey,1),head_after(nprey,2),'r.','MarkerSize',8);
% %     plot([head_after(nprey,1) com_after(nprey,1)],[head_after(nprey,2) com_after(nprey,2)],...
% %         'r-');
% % end
% % 
% % ax = gca;
% % ax.FontName = 'Times New Roman';
% % xl = xlabel('$x$');
% % xl.Interpreter = 'latex';
% % xl.FontName = 'Times New Roman';
% % yl = ylabel('$y$');
% % yl.Interpreter = 'latex';
% % yl.FontName = 'Times New Roman';
% % 
% % axis(0.01*[-2 6 -5 5])
% % 
% % print(f1,'2d_slow.eps','-depsc','-painters');
% 
% % figure(2)
% % hold on
% % axis equal
% % 
% % for nprey = 1:Nprey
% %     plot(com_before(nprey,1),com_before(nprey,3),'k.','MarkerSize',6);
% %     plot(head_before(nprey,1),head_before(nprey,3),'k.','MarkerSize',9);
% %     plot([head_before(nprey,1) tail_before(nprey,1)],[head_before(nprey,3) tail_before(nprey,3)],...
% %         'k-');
% %     
% %     plot(com_after(nprey,1),com_after(nprey,3),'r.','MarkerSize',6);
% %     plot(head_after(nprey,1),head_after(nprey,3),'r.','MarkerSize',9);
% %     plot([head_after(nprey,1) com_after(nprey,1)],[head_after(nprey,3) com_after(nprey,3)],...
% %         'r-');
% % end
% % 
% % figure(3)
% % hold on
% % axis equal
% % 
% % for nprey = 1:Nprey
% %     plot3(com_before(nprey,1),...
% %           com_before(nprey,2),...
% %           com_before(nprey,3),...
% %           'k.','MarkerSize',10);
% %     plot3(head_before(nprey,1),...
% %           head_before(nprey,2),...
% %           head_before(nprey,3),...
% %           'k.','MarkerSize',20);
% %     plot3([head_before(nprey,1) tail_before(nprey,1)],...
% %           [head_before(nprey,2) tail_before(nprey,2)],...
% %           [head_before(nprey,3) tail_before(nprey,3)],...
% %           'k-',...
% %           'LineWidth',2);
% %     
% %     plot3(com_after(nprey,1),...
% %           com_after(nprey,2),...
% %           com_after(nprey,3),...
% %           'r.','MarkerSize',10);
% %     plot3(head_after(nprey,1),...
% %           head_after(nprey,2),...
% %           head_after(nprey,3),...
% %           'r.','MarkerSize',20);
% %     plot3([head_after(nprey,1) com_after(nprey,1)],...
% %           [head_after(nprey,2) com_after(nprey,2)],...
% %           [head_after(nprey,3) com_after(nprey,3)],...
% %           'r-',...
% %           'LineWidth',2); 
% % end
