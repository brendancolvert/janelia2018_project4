function [rho_p,rho_q] = nn_dist_circ(X,Y,k,circFlag)
%NN_DIST Computes k-nearest-neigbor (KNN) distances from elements in X to
%elements in X and elements in Y

Nx = size(X,2);
Ny = size(Y,2);

rho_p = zeros(1,Nx);
rho_q = zeros(1,Nx);
for nx = 1:Nx
    if nx == 1
        rho_p(nx) = kth_dist_circ(X(:,nx),X(:,2:end),k,circFlag);
    elseif nx == Nx
        rho_p(nx) = kth_dist_circ(X(:,nx),X(:,1:(end-1)),k,circFlag);
    else
        rho_p(nx) = kth_dist_circ(X(:,nx),X(:,[1:(nx-1) (nx+1):end]),k,circFlag);
    end
    rho_q(nx) = kth_dist_circ(X(:,nx),Y,k,circFlag);
end


end

function rhok = kth_dist_circ(X,Y,k,circFlag)
%%KTH_DIST Computes kth smallest distances from X to elements in Y

rhos = vecnorm_circ(X-Y,circFlag);
rhom = mink(rhos,k);
rhok = rhom(k);


end

function nrm = vecnorm_circ(X,circFlag)
eps = 10^(-10);

[D,N] = size(X);
nrm = vecnorm(X,2,1);
for d = 1:D
    if circFlag(d)
        Xp = zeros(D,1);
        Xp(d) = 1;
        distP = vecnorm(X+2*pi*Xp,2,1);
        distM = vecnorm(X-2*pi*Xp,2,1);
    end
    nrm = min([nrm;
               distP;...
               distM;],...
               [],1);
               
end

nrm = nrm + eps;


end