function out = NLL_S2(eta,x,varargin)

[D,N] = size(x);

if D ~= 2
    error('Improper data matrix dimension');
end

out = 0;

if isempty(varargin)
    if length(eta) == 2 || length(eta) == 4
        out = 1/N*sum(-log(pX_S2(x,eta)));
    end
end

end

