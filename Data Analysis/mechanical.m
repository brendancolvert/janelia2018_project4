clear all
close all
clc

Vnum = [1 2 3];


load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

%% Load Data
V   = data(:,3)*0.01;
d   = data(:,1);
U   = data(:,9);
tht = data(:,4);
phi = data(:,2);
lam = wrapToPi(tht-phi+pi);
chi = real(acos(U./V));
dlh = data(:,5);
dld = data(:,6);


f1 = figure(1);
f1.Units = 'inches';
f1.Position = [1 1 6 6];
f1.Color = 'k';

plot(dlh,dld,'w.','MarkerSize',6)


xlim([-pi pi])
ylim([-pi pi])
ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.Color = [1 1 1]*0.001;
ax.XTick = pi*(-1:0.5:1);
ax.YTick = pi*(-1:0.5:1);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.YTickLabels = {'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$\pi$'};
ax.XAxis.Color = [1 1 1]*0.999;
ax.YAxis.Color = [1 1 1]*0.999;

xlabel('$\Delta_h$','Interpreter','latex')
ylabel('$\Delta_d$','Interpreter','latex')


print(f1,'mechanical.eps','-depsc','-painters')