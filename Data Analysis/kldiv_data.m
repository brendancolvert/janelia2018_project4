clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap

U = 0.075; % m/s

%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
K   = V/U;
thp = data(:,4);
dth = data(:,5);
del = data(:,6);

Nsig = 21;
Nrep = 3;

sig_phi = logspace(log10(0.05),log10(0.5),Nsig)*pi;
sig_Del = logspace(log10(0.05),log10(0.5),Nsig)*pi;
Ns_phi  = length(sig_phi);
Ns_Del  = length(sig_Del);



state.phi = repmat(phi,Nrep,1);
state.thp = repmat(thp,Nrep,1);
state.K   = repmat(K,Nrep,1);
state.dth = repmat(dth,Nrep,1);
state.del = repmat(del,Nrep,1);
state.N   = length(state.phi);
N = state.N;

Delta_1 = zeros(N,Ns_phi,Ns_Del);
Delta_2 = zeros(N,Ns_phi,Ns_Del);
% Delta_3 = zeros(N,Ns_phi,Ns_Del);
% Delta_4 = zeros(N,Ns_phi,Ns_Del);

for ns_phi = 1:Ns_phi
    for ns_Del = 1:Ns_Del
        fprintf('%i - %i:\t',ns_phi,ns_Del);
        noise.phi   = sig_phi(ns_phi);
        noise.thp   = sig_phi(ns_phi);
        noise.Delta = sig_Del(ns_Del);
        tic
        Delta_1(1:N,ns_phi,ns_Del) = strategy_1(state,noise);
        Delta_2(1:N,ns_phi,ns_Del) = strategy_2(state,noise);
        dkl_1_h(ns_phi,ns_Del) = KLdiv_circ([state.phi';state.dth'],...
                                            [state.phi';squeeze(Delta_1(:,ns_phi,ns_Del))'],...
                                            1,...
                                            [true;true]);
        dkl_1_d(ns_phi,ns_Del) = KLdiv_circ([state.phi';state.del'],...
                                            [state.phi';squeeze(Delta_1(:,ns_phi,ns_Del))'],...
                                            1,...
                                            [true;true]);
        dkl_2_h(ns_phi,ns_Del) = KLdiv_circ([state.phi';state.dth'],...
                                            [state.phi';squeeze(Delta_2(:,ns_phi,ns_Del))'],...
                                            1,...
                                            [true;true]);
        dkl_2_d(ns_phi,ns_Del) = KLdiv_circ([state.phi';state.del'],...
                                            [state.phi';squeeze(Delta_2(:,ns_phi,ns_Del))'],...
                                            1,...
                                            [true;true]);
                                        
       fprintf(sprintf('%f, \t%f, \t%f, \t%f -- %fs\n',dkl_1_h(ns_phi,ns_Del),...
                                                       dkl_1_d(ns_phi,ns_Del),...
                                                       dkl_2_h(ns_phi,ns_Del),...
                                                       dkl_2_d(ns_phi,ns_Del),...
                                                       toc));
    end
end


%% Save

save('dkl.mat','dkl_1_h','dkl_1_d','dkl_2_h','dkl_2_d')



%% Plot

cnum = 40;
clim = [39

f1 = figure(1);
f1.Units = 'pixels';
f1.Position = [ 100 100 750 650];

subplot(2,2,1)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),dkl_1_h,cnum,'LineStyle','none')
colormap(gray);%colormap(cmap(end/2:end,:));
colorbar
xlabel('$\log \sigma_\phi$','Interpreter','latex');
ylabel('$\log \sigma_\delta$','Interpreter','latex');
axis([-0.8 0.0 -0.8 0.0])
%title('Strategy 1 vs. Heading');
caxis([40 42])


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.XTick = (-0.8:0.4:0.0);
ax.YTick = (-0.8:0.4:0.0);
ax.TickLabelInterpreter = 'latex';
%ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
%ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};


subplot(2,2,2)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),dkl_1_d,cnum,'LineStyle','none')
colormap(gray);%colormap(cmap(end/2:end,:));
colorbar
xlabel('$\log \sigma_\phi$','Interpreter','latex');
ylabel('$\log \sigma_\delta$','Interpreter','latex');
axis([-0.8 0.0 -0.8 0.0])
%title('Strategy 1 vs. Displacement');
caxis([40 42])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.XTick = (-0.8:0.4:0.0);
ax.YTick = (-0.8:0.4:0.0);
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
% ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};







subplot(2,2,3)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),dkl_2_h,cnum,'LineStyle','none')
colormap(gray);%colormap(cmap(end/2:end,:));
colorbar
xlabel('$\log \sigma_\phi$','Interpreter','latex');
ylabel('$\log \sigma_\delta$','Interpreter','latex');
axis([-0.8 0.0 -0.8 0.0])
%title('Strategy 2 vs. Heading');
caxis([40 42])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.XTick = (-0.8:0.4:0.0);
ax.YTick = (-0.8:0.4:0.0);
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
% ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};


subplot(2,2,4)
hold on
box on
contourf(log10(sig_phi),log10(sig_Del),dkl_2_d,cnum,'LineStyle','none')
colormap(gray);%colormap(cmap(end/2:end,:));
colorbar
xlabel('$\log \sigma_\phi$','Interpreter','latex');
ylabel('$\log \sigma_\delta$','Interpreter','latex');
axis([-0.8 0.0 -0.8 0.0])
%title('Strategy 2 vs. Displacement');
caxis([40 42])

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 20;
ax.XTick = (-0.8:0.4:0.0);
ax.YTick = (-0.8:0.4:0.0);
% ax.TickLabelInterpreter = 'latex';
% ax.XTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};
% ax.YTickLabels = {'$10^{-1.5}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$','$10^{0.5}$'};


% lg.Interpreter = 'latex';
% 
print(f1,'paramstudy_dkl.eps','-depsc')


    
% == % == % == % == % == % FUNCTIONS % == % == % == % == % == % == %


% % Strategy 1 % % 
function Delta = strategy_1(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    if phi_hat < pi
        Delta(n) = wrapToPi(-pi/2+vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi( pi/2+vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 2 % % 
function Delta = strategy_2(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    Delta(n) = wrapToPi(phi_hat+pi+vmrand(0,1/noise.Delta^2));
end

end


% % Strategy 3 % % 
function Delta = strategy_3(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        Delta(n) = wrapToPi(thp_hat + pi/2 + vmrand(0,1/noise.Delta^2));
    else
        Delta(n) = wrapToPi(thp_hat - pi/2 + vmrand(0,1/noise.Delta^2));
    end
end

end

% % Strategy 4 % % 
function Delta = strategy_4(state,noise)
N = state.N;
Delta = zeros(N,1);

for n = 1:N
    phi_hat = wrapTo2Pi(state.phi(n) + vmrand(0,1/noise.phi^2));
    thp_hat = wrapTo2Pi(state.thp(n) + vmrand(0,1/noise.thp^2));
    K_hat   = max(1,state.K(n) + randn*noise.K);
    if wrapTo2Pi(thp_hat-phi_hat) < pi
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat + acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    else
        if K_hat > 1
            Delta(n) = wrapToPi(thp_hat - acos(1/K_hat) + vmrand(0,1/noise.Delta^2));
        else
            Delta(n) = wrapToPi(thp_hat + vmrand(0,1/noise.Delta^2));
        end
    end
end

end









