clear all
close all
clc

Vnum = [1 2 3];

load(['responseData_' num2str(Vnum) '.mat']);
load rbcmap



%data = [ d phi V thp dtheta dispangle];

d   = data(:,1);
phi = data(:,2);
V   = data(:,3)*0.01;
U   = data(:,9);
K   = V./U;
tht = data(:,4);
dlh = data(:,5);
dld = data(:,6);

N = length(phi);


sig_phi = 0.625;%1.112;%[ 0.4 ]*pi;
sig_del = 0.787;%0.625;%[ 0.2 ]*pi;

mult = 500;
Ntest = mult*N;
phitest = 2*pi*rand(Ntest,1);

Nbin = 20;
phiedge = linspace(-0.1,2.1,Nbin+3)*pi;
deledge = linspace(-1.1,1.1,Nbin+1)*pi;
phibin  = 0.5*(phiedge(2:end)+phiedge(1:(end-1)));
delbin  = 0.5*(deledge(2:end)+deledge(1:(end-1)));

deltest = strategy2(phitest,sig_phi,sig_del);
C_model = histcounts2(phitest,deltest,phiedge,deledge);
C_dlh   = histcounts2(phi,dlh,phiedge,deledge);
C_dld   = histcounts2(phi,dld,phiedge,deledge);


cnum = 40;
clim = [0 1]*0.01;

f11 = figure(11);
f11.Units = 'pixels';
f11.Position = [ 100 100 300 300 ];

hold on
box on
contourf(phibin,delbin,C_model'/Ntest,[-realmax linspace(clim(1),clim(2),cnum) realmax],'LineStyle','none');
colormap gray
caxis(clim)


xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');

print(f11,['dataFit_model.eps'],...
                       '-depsc','-painters');


f12 = figure(12);
f12.Units = 'pixels';
f12.Position = [ 100 100 300 300 ];

hold on
box on
contourf(phibin,delbin,C_dld'/N,[-realmax linspace(clim(1),clim(2),cnum) realmax],'LineStyle','none');
colormap gray
caxis(clim)


xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');

print(f12,['dataFit_dld.eps'],...
                       '-depsc','-painters');


f13 = figure(13);
f13.Units = 'pixels';
f13.Position = [ 100 100 300 300 ];

hold on
box on
contourf(phibin,delbin,C_dlh'/N,[-realmax linspace(clim(1),clim(2),cnum) realmax],'LineStyle','none');
colormap gray
caxis(clim)


xlim([0 2]*pi);
ylim([-1 1]*pi);

ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 16;
ax.TickLabelInterpreter = 'latex';
ax.XTick = (0:1:2)*pi;
ax.XTickLabels = {'$0$','$\pi$','$2\pi$'};
ax.YTick = (-1:1:1)*pi;
ax.YTickLabels = {'$-\pi$','$0$','$\pi$'};

xlabel('$\phi$','Interpreter','latex');
ylabel('$\delta$','Interpreter','latex');

print(f13,['dataFit_dlh.eps'],...
                       '-depsc','-painters');








function delmod = strategy1(phi,sig_phi,sig_del)
[Nx,Ny] = size(phi);

phi_hat = wrapTo2Pi(phi + vmrand(0,1/sig_phi^2,[Nx,Ny]));

test = phi_hat < pi;
delmod = test*(-pi/2) + (~test)*(pi/2) + vmrand(0,1/sig_del^2,[Nx,Ny]);

end


function delmod = strategy2(phi,sig_phi,sig_del)
[Nx,Ny] = size(phi);

phi_hat = wrapTo2Pi(phi + vmrand(0,1/sig_phi^2,[Nx,Ny]));
delmod = wrapToPi(phi+pi + vmrand(0,1/sig_del^2,[Nx,Ny]));

end



