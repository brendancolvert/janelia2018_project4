clear all
close all
clc

kappa = 4;
     
mu_p = 0;
mu_qs = linspace(0,2*pi,51);

rng(1)

N = 500;
M = 500;

X = vmrand(mu_p,kappa,N,1)';

ks = [1 10 50 100];
for qind = 1:length(mu_qs)
    fprintf('qind = %i/%i\n',qind,length(mu_qs))
    mu_q = mu_qs(qind);


    KLD(qind) = kappa*besseli(1,kappa)/besseli(0,kappa)*(1-cos(mu_p-mu_q));
    
    Y = vmrand(mu_q,kappa,M,1)';
    
    for kind = 1:length(ks)
        k = ks(kind);
        kld(qind,kind) = max([KLdiv_circ(X,Y,k,true),0]);
    end
end


%% Plots

f1 = figure(1);
f1.Units = 'inches';
f1.Position = [ 1 1 3 3 ];

semilogy(mu_qs,KLD,...
       'k-',...
       'LineWidth',2)
hold on
for kind = 1:length(ks)
    plot(mu_qs,kld(:,kind),'-')
end
xlim([0 2]*pi)
ylim(10.^[-0.2 1.0])


ax = gca;
ax.FontName = 'Times New Roman';
ax.FontSize = 8;
ax.XTick = (0:0.5:2)*pi;
ax.YTick = 10.^(-0.2:0.3:1.0);
ax.TickLabelInterpreter = 'latex';
ax.XTickLabels = {'$0$','$0.5\pi$','$\pi$',...
                    '$1.5\pi$','$2\pi$'};
ax.YTickLabels = {'$10^{-0.2}$','$10^{0.1}$','$10^{0.4}$',...
                    '$10^{0.7}$','$10^{1}$'};

xlabel('$\mu_q$',...
       'Interpreter','latex');
ylabel('$\mathcal{D}(p \| q )$',...
       'Interpreter','latex');
%    
lns = lines;
kshft = 1.1*[1 1 1 1];
for kind = 1:length(ks)
    text(pi,kld(round(end/2),kind)*kshft(kind),['$k=' num2str(ks(kind)) '$'],...
        'HorizontalAlign','center',...
        'FontName','Times New Roman',...
        'FontSize',8,...
        'Interpreter','latex',...
        'Color',lns(kind,:));
end

text(pi,10^(0.88),'Analytical',...
    'FontName','Times New Roman',...
    'FontSize',8,...
    'Interpreter','latex',...
    'HorizontalAlign','center',...
    'Color',[ 0 0 0 ]);

% 
print(sprintf('kldiv_circ_test_%i_%i.eps',N,M),'-depsc','-painters')
% 
% % 
% % 
% % 
% % 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 





