function play_weihs_numerical
% Tangent to ground-thruth the Evasion section of the Weihs & Webb
% theoretical paper



if 0
% Parameter values  -----------------

Kvals = 10.^linspace(1,2,9);

%Kvals = 2;

% Initial position of prey
p.X0 = 1;
p.Y0 = 0;

% Speed of predator
%p.U = 10e-2;


% Speed of prey (m/s)
p.V = 12e-2;

t = linspace(0,1.2,100)';

alfa = 20./180.*pi;

E = [p.V.*t.*cos(alfa)+p.X0 p.V.*t.*sin(alfa)+p.Y0];

for i = 1:length(Kvals)
    
    p.K = Kvals(i);
    
    % Speed of predator (m/s)
    p.U = p.V.*p.K;

    
    P = [p.U.*t t.*0];
    

    D = sqrt( (P(:,1)-E(:,1)).^2 + E(:,2).^2);
    
    % subplot(3,3,2)
    % plot(P(:,1),P(:,2),'b+-',E(:,1),E(:,2),'g+')
    % hold on
    % axis equal
   
    subplot(3,3,i)
    plot(t,D,'k')
    
    t_min = (p.X0./p.V).*(p.K-cos(alfa))./(1-2.*p.K.*cos(alfa)+p.K.^2);
    
    title([' K = ' num2str(p.K) ', tm_i_n = ' num2str(t_min)])

end

end


figure

% Parameter values  -----------------

Kvals = [2 10 50 100];


alfas = linspace(-pi,pi,1000);

%Kvals = 2;

% Initial position of prey
p.X0 = 1;
p.Y0 = 0;

% Speed of prey (m/s)
    p.V = 12e-2;

% Speed of predator
%p.U = 10e-2;
for i = 1:length(Kvals)
    
    p.K = Kvals(i);
    
    
    
    t = linspace(0,1.2,100)';
    
   
    
    for j = 1:length(alfas)
        
        alfa = alfas(j);
        
        % Speed of predator (m/s)
        p.U = p.V.*p.K;
        
        
        t_min(j,1) = (p.X0./p.V).*(p.K-cos(alfa))./(1-2.*p.K.*cos(alfa)+p.K.^2);
               
        D_min(j,1) = sin(alfa).^2 ./ (p.K.^2-2.*p.K.*cos(alfa)+1);
                  
                  
    end
    
    subplot(2,2,i)
    plot(alfas.*180/pi,D_min,'r')
    title(['K = ' num2str(p.K)])
    ylabel('t_m_i_n');xlabel('alpha')
    grid on
    
    %sqrt( (sin(alfa).^2) ./ 
end
