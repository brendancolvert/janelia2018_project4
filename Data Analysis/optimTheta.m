%% Function to compute optimal escpae angle at (x0,y0,K)
function [theta,Dmin] =  optimTheta(x,y,K)
% theta is the angle that achieves the minium distance (Dmin)

% Loop thru each coordinate
for i = 1:length(x)
    
    % Set current starting position 
    x0 = x(i);
    y0 = abs(y(i));
    
    % Set current K
    if length(K)>1
        k = K(i);
    else
        k = K;
    end
    
    % Use 'fminbnd' by calling fminbnd(@fun,a,b)
    [theta(i,1),Dmin(i,1)] = fminbnd(@mindist,0,pi);
    
    if y(i)<0
        theta(i,1) = -theta(i,1);
    end
end

% Nested function that computes the objective function     
    function d = mindist(theta)
        
        % minimum distance function
        d = -1 * ((k*y0 - y0*cos(theta) + x0*sin(theta))^2 / ...
            (1+k^2-2*k*cos(theta)));
        
    end
end